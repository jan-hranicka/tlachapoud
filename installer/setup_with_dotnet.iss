; ------------------------------------------------------------
; Script generating installer for Phrase Recorder application
; Author:           Jan Hranicka
; Script version:   1.2
; ------------------------------------------------------------

#define MyAppName "Phrase Recorder"
#define MyAppVersion "2.3"
#define MyAppPublisher "Západočeská univerzita v Plzni"
#define MyAppURL "http://zcu.cz/"
#define MyAppExeName "Phrase Recorder.exe"
#define CopyrightYear "2018"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{5576B2D1-169E-4813-8071-211C87423CAD}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppCopyright=Copyright © {#CopyrightYear} {#MyAppPublisher}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\{#MyAppName} 2
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
OutputDir=InstallerRelease
OutputBaseFilename=PhraseRecorder_Installer
SetupIconFile=..\Tlachapoud\Tlachapoud\prapp.ico
UninstallDisplayName={#MyAppName}
UninstallDisplayIcon={app}\Icons\prapp.ico
UninstallFilesDir={app}\Uninstall
Compression=lzma
SolidCompression=yes
VersionInfoVersion=1.1

;Digital signature of the setup
;SignTool=signtool
;SignedUninstaller=yes

[Languages]
Name: "czech"; MessagesFile: "compiler:Languages\Czech.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}";

[Dirs]
Name: {app}; Permissions: everyone-full

[Files]
Source: "..\Tlachapoud\Tlachapoud\bin\Release\Tlachapoud.exe"; DestName: "{#MyAppExeName}"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs; Permissions: everyone-full
Source: "..\Tlachapoud\Tlachapoud\bin\Release\*.dll"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs; Permissions: everyone-full
Source: "..\documentation\Documentation.pdf"; DestName: "Documentation.pdf"; DestDir: "{app}\Resources\"; Permissions: everyone-full

Source: "dotNetFramework\*"; DestDir: "{tmp}"; Flags: deleteafterinstall; AfterInstall: InstallFramework; Check: FrameworkIsNotInstalled
Source: "..\Tlachapoud\Tlachapoud\prapp.ico"; DestDir: {app}\Icons\; Permissions: everyone-full
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon; IconFilename: "{app}\Icons\prapp.ico"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"; IconFilename: "{app}\Icons\prapp.ico"

[Code]
var CancelWithoutPrompt: boolean;

function InitializeSetup(): Boolean;
begin
  CancelWithoutPrompt := false;
  result := true;
end;

procedure CancelButtonClick(CurPageID: Integer; var Cancel, Confirm: Boolean);
begin
  if CurPageID=wpInstalling then
    Confirm := not CancelWithoutPrompt;
end;

function FrameworkIsNotInstalled(): Boolean;
var
  bSuccess: Boolean;
  regVersion: Cardinal;
begin
  Result := True;

  bSuccess := RegQueryDWordValue(HKLM, 'Software\Microsoft\NET Framework Setup\NDP\v4\Full', 'Release', regVersion);
  if (True = bSuccess) and (regVersion >= 378389) then begin
    Result := False;
  end;
end;

procedure InstallFramework;
var
  StatusText: string;
  ResultCode: Integer;
begin
  StatusText := WizardForm.StatusLabel.Caption;
  WizardForm.StatusLabel.Caption := 'Instaluji .NET Framework na tento počítač...';
  WizardForm.ProgressGauge.Style := npbstMarquee;
  try
      if not Exec(ExpandConstant('{tmp}\dotNetFrameworkv452FullOffline.exe'), '/q /norestart', '', SW_SHOW, ewWaitUntilTerminated, ResultCode) then
  begin
    // you can interact with the user that the installation failed
    MsgBox('Instalace .NET Frameworku skončila chybou: ' + IntToStr(ResultCode) + '.',
      mbError, MB_OK);
    CancelWithoutPrompt := true;
    WizardForm.Close;       
  end;
  finally
    WizardForm.StatusLabel.Caption := StatusText;
    WizardForm.ProgressGauge.Style := npbstNormal;
  end;
end;

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

