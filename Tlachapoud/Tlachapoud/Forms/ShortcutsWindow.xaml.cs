﻿using MahApps.Metro.Controls;
using System.Windows.Input;

namespace Tlachapoud.Forms
{
    /// <summary>
    /// Interakční logika pro ShortcutsWindow.xaml
    /// </summary>
    public partial class ShortcutsWindow : MetroWindow
    {
        /// <summary>
        /// Create a new windows with shortcuts overview
        /// </summary>
        public ShortcutsWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Close the window when user press down the key for Shortcuts window
        /// </summary>
        private void OnGlobalKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Q)
            {
                Close();
                return;
            }
        }
    }
}
