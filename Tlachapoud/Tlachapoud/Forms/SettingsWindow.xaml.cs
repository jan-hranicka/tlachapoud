﻿using FluentFTP;
using Ionic.Zip;
using MahApps.Metro.Controls;
using MahApps.Metro.IconPacks;
using Microsoft.Win32;
using NAudio.CoreAudioApi;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml.Linq;
using Tlachapoud.Controls.Dialogs;
using Tlachapoud.Core;
using Tlachapoud.Core.Audio.CheckModules;
using Tlachapoud.Core.Audio.Wasapi;
using Tlachapoud.Core.Data;
using Tlachapoud.Core.IO;
using Tlachapoud.Core.Net;

namespace Tlachapoud.Forms
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : MetroWindow
    {
        // Private reference to DataIntegrator
        private DataIntegrator DataIntegrator;

        #region Static Color Brushes
        readonly SolidColorBrush ColorWarning = new SolidColorBrush(Colors.LightPink);   // Brush for Warnings
        readonly SolidColorBrush ColorDefault = new SolidColorBrush(Colors.White);       // Default brush
        #endregion Static Color Brushes

        /// <summary>
        /// Create a new Settings Window
        /// </summary>
        /// <param name="dataIntegrator">DataIntegrator object encapsulating all data needed</param>
        /// <param name="openingTab">Which tab should be the opening one</param>
        public SettingsWindow(DataIntegrator dataIntegrator, SettingsTabs openingTab=SettingsTabs.Application)
        {
            InitializeComponent();
            DataIntegrator = dataIntegrator;

            #region Numeric Updowns Calibration
            nudNavigationButtonSize.Minimum = Calibration.Buttons.NavigationButton.Min;
            nudNavigationButtonSize.Maximum = Calibration.Buttons.NavigationButton.Max;
            nudRecorderButtonSize.Minimum = Calibration.Buttons.RecorderButton.Min;
            nudRecorderButtonSize.Maximum = Calibration.Buttons.RecorderButton.Max;
            nudButtonsFontSize.Minimum = Calibration.Text.ButtonFontSize.Min;
            nudButtonsFontSize.Maximum = Calibration.Text.ButtonFontSize.Max;
            nudPhraseFontSize.Minimum = Calibration.Text.PhraseFontSize.Min;
            nudPhraseFontSize.Maximum = Calibration.Text.PhraseFontSize.Max;
            #endregion Numeric Updowns Calibration

            #region Initialize Application Tab
            nudPhraseFontSize.Value = DataIntegrator.Settings.AppPhraseFontSize;
            nudButtonsFontSize.Value = DataIntegrator.Settings.AppButtonFontSize;
            nudRecorderButtonSize.Value = DataIntegrator.Settings.AppRecorderButtonsSize;
            nudPlayerButtonSize.Value = DataIntegrator.Settings.AppPlayerButtonSize;
            nudNavigationButtonSize.Value = DataIntegrator.Settings.AppNavigationButtonSize;
            chbAutomaticUpload.IsChecked = DataIntegrator.Settings.AppAutomaticUploadToSftp;
            chbAutomaticMove.IsChecked = DataIntegrator.Settings.AppAutomaticMoveToFollowing;
            #endregion Initialize Application Tab

            #region Initialize User Tab
            tbxFirstName.Text = DataIntegrator.User.FirstName;
            tbxLastName.Text = DataIntegrator.User.LastName;
            tbxEmail.Text = DataIntegrator.User.Email;
            tbxNickname.Text = DataIntegrator.User.Nickname;

            List<int> years = new List<int>();
            // Initialize dates to the picker
            for (int year = DateTime.Now.Year; year >= 1900; year--)
            {
                years.Add(year);
            }
            cbxYearOfBirth.ItemsSource = years;
            cbxYearOfBirth.SelectedItem = years.Where(y => DataIntegrator.User.YearOfBirth == y).First();
            tbxRegistrationDate.Text = DataIntegrator.User.RegistrationDate?.ToShortDateString();

            tbxDegree.Text = DataIntegrator.User.Degree;
            rbnMale.IsChecked = DataIntegrator.User.Gender == Gender.Male;
            rbnFemale.IsChecked = !rbnMale.IsChecked;
            tbxAccentKind.Text = DataIntegrator.User.RegionalAccentKind;
            rbnUsedToSpeakCoherently.IsChecked = DataIntegrator.User.IsUsedToSpeakCoherently;
            rbnNotUsedToSpeakCoherently.IsChecked = !rbnUsedToSpeakCoherently.IsChecked;
            #endregion

            #region Initialize Server Tab
            tbxHostAddress.Text = DataIntegrator.Settings.SftpServer;
            tbxUserName.Text = DataIntegrator.Settings.SftpUserName;
            pwbPassword.Password = DataIntegrator.Settings.SftpPassword;
            tbxRootDir.Text = DataIntegrator.Settings.SftpRootDirectory;
            #endregion Initialize Server Tab

            // Initialize capture devices
            var captureDevices = DeviceProvider.GetCaptureDevices();
            cobCaptureDevice.ItemsSource = captureDevices;
            cobCaptureDevice.DisplayMemberPath = "FriendlyName";
            var captureDeviceFound = captureDevices?.Where(d => d.ID == DataIntegrator.Settings.CaptureDevice?.ID) ?? new List<MMDevice>();
            if (captureDeviceFound.Any())
                cobCaptureDevice.SelectedItem = captureDeviceFound.First();
            //else
            //    cobCaptureDevice.SelectedItem = captureDevices?.First();

            // Initialize render devices
            var renderDevices = DeviceProvider.GetRenderDevices();
            cobRenderDevices.ItemsSource = renderDevices;
            cobRenderDevices.DisplayMemberPath = "FriendlyName";
            var renderDeviceFound = renderDevices?.Where(d => d.ID == DataIntegrator.Settings.RenderDevice?.ID) ?? new List<MMDevice>();
            if (renderDeviceFound.Any())
                cobRenderDevices.SelectedItem = renderDeviceFound.First();
            else
                cobRenderDevices.SelectedItem = renderDevices?.First();

            // Initialize CheckModules
            tswIntensity.IsChecked = DataIntegrator.Settings.CheckModuleIntensityActive;
            tswPauses.IsChecked = DataIntegrator.Settings.CheckModulePausesActive;
            chbAutomaticSkip.IsChecked = DataIntegrator.Settings.CheckModuleAutomaticSkip;

            atcNavigation.SelectedIndex = (int)openingTab;
        }

        /// <summary>
        /// Check whether user info is set properly or not
        /// </summary>
        /// <returns>True if all info is set properly, otherwise false</returns>
        private bool CheckUserInfo()
        {
            // Reset color of all fields that can change background color
            foreach (var control in new Control[] { tbxFirstName, tbxLastName })
            {
                control.Background = ColorDefault;
            }

            // Check whether required fields are filled
            if (string.IsNullOrWhiteSpace(tbxFirstName.Text))
            {
                warningDlgBox.Message = "Položka pro Vaše jméno je prázdná nebo nevhodně vyplněná. Prosím, zadejte Vaše jméno!";
                tbxFirstName.Background = ColorWarning;
                return false;
            }

            if (string.IsNullOrWhiteSpace(tbxLastName.Text))
            {
                warningDlgBox.Message = "Položka pro Vaše příjmení je prázdná nebo nevhodně vyplněná. Prosím, zadejte Vaše příjmení!";
                tbxLastName.Background = ColorWarning;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Event when user click on Save & Close button. Save settings to the XML.
        /// </summary>
        /// <param name="sender">Save & Close button</param>
        /// <param name="e">Routed event arguments</param>
        private void OnBtnSaveAndCloseClick(object sender, RoutedEventArgs e)
        {           
            DataIntegrator.Settings.CaptureDevice = cobCaptureDevice.SelectedItem as MMDevice;
            DataIntegrator.Settings.RenderDevice = cobRenderDevices.SelectedItem as MMDevice;

            DataIntegrator.Settings.CheckModuleIntensityActive = tswIntensity.IsChecked == true;
            DataIntegrator.Settings.CheckModulePausesActive = tswPauses.IsChecked == true;
            DataIntegrator.Settings.CheckModuleAutomaticSkip = chbAutomaticSkip.IsChecked == true;

            DataIntegrator.Settings.AppPhraseFontSize = (double)nudPhraseFontSize.Value;
            DataIntegrator.Settings.AppButtonFontSize = (double)nudButtonsFontSize.Value;
            DataIntegrator.Settings.AppRecorderButtonsSize = (double)nudRecorderButtonSize.Value;
            DataIntegrator.Settings.AppPlayerButtonSize = (double)nudPlayerButtonSize.Value;
            DataIntegrator.Settings.AppNavigationButtonSize = (double)nudNavigationButtonSize.Value;
            DataIntegrator.Settings.AppAutomaticUploadToSftp = chbAutomaticUpload.IsChecked == true;
            DataIntegrator.Settings.AppAutomaticMoveToFollowing = chbAutomaticMove.IsChecked == true;
            DataIntegrator.SettingsFile.Update();

            if (!CheckUserInfo())
            {
                warningDlgBox.Visibility = Visibility.Visible;
                atcNavigation.SelectedIndex = (int)SettingsTabs.User;
                return;
            }
            else
            {
                warningDlgBox.Visibility = Visibility.Hidden;

                UsersFile usersFile = new UsersFile(ContentDataFile.UsersFilePath);

                DataIntegrator.User.FirstName = tbxFirstName.Text;
                usersFile.UpdateData(DataIntegrator.User, UserProfileData.FirstName);
                DataIntegrator.User.LastName = tbxLastName.Text;
                usersFile.UpdateData(DataIntegrator.User, UserProfileData.LastName);
                DataIntegrator.User.YearOfBirth = (int?)(cbxYearOfBirth.SelectedItem);
                usersFile.UpdateData(DataIntegrator.User, UserProfileData.YearOfBirth);
                DataIntegrator.User.RegionalAccentKind = tbxAccentKind.Text;
                usersFile.UpdateData(DataIntegrator.User, UserProfileData.RegionalAccentKind);
                DataIntegrator.User.Gender = rbnMale.IsChecked == true ? Gender.Male : Gender.Female;
                usersFile.UpdateData(DataIntegrator.User, UserProfileData.Gender);
                DataIntegrator.User.IsUsedToSpeakCoherently = rbnUsedToSpeakCoherently.IsChecked == true;
                usersFile.UpdateData(DataIntegrator.User, UserProfileData.IsUsedToSpeakCoherently);
                DataIntegrator.User.Degree = tbxDegree.Text;
                usersFile.UpdateData(DataIntegrator.User, UserProfileData.Degree);
                usersFile.Save();
            }

            DialogResult = true;
            Close();
        }

        /// <summary>
        /// Event when user click on Cancel button. Close settings window without saving.
        /// </summary>
        /// <param name="sender">Cancel button</param>
        /// <param name="e">Routed event arguments</param>
        private void OnBtnCancelClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        /// <summary>
        /// Event when user click on Reset to default button. Reset style-settings to its default.
        /// </summary>
        /// <param name="sender">Reset to default button</param>
        /// <param name="e">Routed event arguments</param>
        private void OnBtnResetToDefaultClick(object sender, RoutedEventArgs e)
        {
            nudPhraseFontSize.Value = DefaultSettings.Application.PhraseFontSize;
            nudButtonsFontSize.Value = DefaultSettings.Application.ButtonsFontSize;
            nudRecorderButtonSize.Value = DefaultSettings.Application.RecorderButtonsSize;
            nudPlayerButtonSize.Value = DefaultSettings.Application.PlayerButtonsSize;
            nudNavigationButtonSize.Value = DefaultSettings.Application.NavigationButtonsSize;
        }

        /// <summary>
        /// Event when user click on Load Config File button. Load a new configuration file for check modules.
        /// </summary>
        /// <param name="sender">Load Config File button</param>
        /// <param name="e">Routed event arguments</param>
        private void OnBtnLoadConfigClick(object sender, RoutedEventArgs e)
        {
            LoadNewConfig(DataIntegrator);
        }

        /// <summary>
        /// Event when user click on Load Phrases File button. Load a new phrases file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBtnLoadPhrasesClick(object sender, RoutedEventArgs e)
        {
            LoadNewPhrases(DataIntegrator);
        }

        /// <summary>
        /// Load a new configuration CFG file for check modules
        /// </summary>
        /// <param name="dataIntegrator"></param>
        public static void LoadNewConfig(DataIntegrator dataIntegrator)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Konfigurační soubor (*.cfg)|*.cfg";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            var dialogResult = openFileDialog.ShowDialog();
            if (dialogResult == true)
            {
                File.Copy(openFileDialog.FileName, dataIntegrator.Session.CheckModulesConfigFilePath.FullName, true);
                dataIntegrator.CheckModuleConfigs = CheckModulesConfigFile.Load(dataIntegrator.Session.CheckModulesConfigFilePath.FullName);
                // Initialize Check Modules
                if (!dataIntegrator.Settings.CheckModuleIntensityActive)
                    dataIntegrator.CheckModuleConfigs = dataIntegrator.CheckModuleConfigs.ForEach(m => m.IsActive = false, m => m.ModuleName == AvailableCheckModules.Intensity);
                if (!dataIntegrator.Settings.CheckModulePausesActive)
                    dataIntegrator.CheckModuleConfigs = dataIntegrator.CheckModuleConfigs.ForEach(m => m.IsActive = false, m => m.ModuleName == AvailableCheckModules.PauseLen);
                dataIntegrator.CheckModules = new CheckModules(dataIntegrator.CheckModuleConfigs);
            }
        }

        /// <summary>
        /// Load a new phrases XML file for application
        /// </summary>
        /// <param name="dataIntegrator"></param>
        public static void LoadNewPhrases(DataIntegrator dataIntegrator)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Soubor frází (*.xml)|*.xml";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            var dialogResult = openFileDialog.ShowDialog();
            if (dialogResult == true)
            {
                // Merge files
                PhrasesFile newPhrasesFile = new PhrasesFile(openFileDialog.FileName);
                dataIntegrator.PhrasesFile.Merge(newPhrasesFile);
            }
        }

        /// <summary>
        /// Export check modules' configuration CFG file
        /// </summary>
        /// <param name="dataIntegrator"></param>
        public static void ExportConfig(DataIntegrator dataIntegrator)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Konfigurační soubor (*.cfg)|*.cfg";
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            saveFileDialog.FileName = ContentDataFile.CheckModulesConfigFileName;

            var dialogResult = saveFileDialog.ShowDialog();
            if (dialogResult == true)
            {
                File.Copy(dataIntegrator.Session.CheckModulesConfigFilePath.FullName, 
                    Path.Combine(Path.GetDirectoryName(saveFileDialog.FileName), 
                    ContentDataFile.CheckModulesConfigFileName), true);
            }
        }

        public static void LoadNewSftpConnectionSettings(DataIntegrator dataIntegrator)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Zabezpečený archiv ZIP (*.zip)|*.zip";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            // Import SFTP settings
            if (openFileDialog.ShowDialog() == true)
            {
                ZipFile zipFile = new ZipFile(openFileDialog.FileName);
                zipFile.Password = "$z5W!wyNa4#257jk*Gm";
                zipFile.ExtractAll(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ExtractExistingFileAction.OverwriteSilently);

                string filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sftp_connection_info.xml");
                XDocument xml = XDocument.Load(filename);
                var sftpSettingsEnum = from n in xml.Descendants("Sftp")
                                       select new
                                       {
                                           Host = n.Element("Host")?.Value,
                                           Password = n.Element("Password")?.Value,
                                           RootDir = n.Element("RootDir")?.Value,
                                           UserName = n.Element("UserName")?.Value
                                       };
                File.Delete(filename);
                var extractedSettings = sftpSettingsEnum.First();
                dataIntegrator.Settings.SftpServer = extractedSettings.Host;
                dataIntegrator.Settings.SftpUserName = extractedSettings.UserName;
                dataIntegrator.Settings.SftpRootDirectory = extractedSettings.RootDir;
                dataIntegrator.Settings.SftpPassword = extractedSettings.Password;
                dataIntegrator.SettingsFile.Update();
            }
        }

        /// <summary>
        /// Window closing event handler
        /// </summary>
        /// <param name="sender">Metro window object</param>
        /// <param name="e">Cancel window closing arguments</param>
        private void OnWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!CheckUserInfo())
            {
                MessageBoxDialog msgDialog = new MessageBoxDialog("Některá nastavení nelze uložit",
                    "Některé z vyplněných polí v nastavení není možné uložit, dokud nebudou opravena zvýrazněná políčka. Chcete i přesto nastavení ukončit? Některá nastavení nebudou uložena.", MessageBoxDialogButtons.YesNo,
                    MahApps.Metro.IconPacks.PackIconFontAwesomeKind.ExclamationCircle, Colors.Goldenrod);
                msgDialog.Owner = this;
                var dialogResult = msgDialog.ShowDialog();
                e.Cancel = dialogResult == false;
            }
        }

        /// <summary>
        /// Event when input text of PhoneNumber TextBox has changed
        /// </summary>
        private void OnTbxPhoneNumberPreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = !Utilities.IsNumericInput(e.Text);
        }

        /// <summary>
        /// Event when user click on Load new SFTP connection settings button
        /// </summary>
        private void OnBtnLoadSftpConnectionSettingsClick(object sender, RoutedEventArgs e)
        {
            LoadNewSftpConnectionSettings(DataIntegrator);
            RefreshSftpSettings();
        }

        /// <summary>
        /// Event when user click on Test SFTP connection button
        /// </summary>
        private async void OnBtnTestSftpConnectionClick(object sender, RoutedEventArgs e)
        {
            // Test SFTP connection
            FtpClient ftpClient = new FtpClient(DataIntegrator.Settings.SftpServer, new System.Net.NetworkCredential(DataIntegrator.Settings.SftpUserName, DataIntegrator.Settings.SftpPassword));

            try
            {
                await ftpClient.ConnectAsync();
                pgrTest.Visibility = Visibility.Hidden;
                MessageBoxDialog msgBoxDialog = new MessageBoxDialog("Test spojení se serverem",
                    "Spojení se serverem bylo úspěšně navázáno.",
                    MessageBoxDialogButtons.OK, MahApps.Metro.IconPacks.PackIconFontAwesomeKind.CheckCircleOutline, Colors.ForestGreen);
                msgBoxDialog.Owner = this;
                msgBoxDialog.ShowDialog();
            }
            catch (System.Net.Sockets.SocketException)
            {
                pgrTest.Visibility = Visibility.Hidden;
                MessageBoxDialog msgBoxDialog = new MessageBoxDialog("Test spojení se serverem",
                    "Spojení se serverem se nezdařilo! Na adresu hostitele není možné se připojit!",
                    MessageBoxDialogButtons.OK, MahApps.Metro.IconPacks.PackIconFontAwesomeKind.TimesCircleOutline, Colors.Crimson);
                msgBoxDialog.Owner = this;
                msgBoxDialog.ShowDialog();
            }
            catch
            {
                pgrTest.Visibility = Visibility.Hidden;
                MessageBoxDialog msgBoxDialog = new MessageBoxDialog("Test spojení se serverem",
                    "Spojení se serverem se nezdařilo! Zkontrolujte, zda jste připojeni k Internetu nebo zda jsou přihlašovací údaje serveru správné",
                    MessageBoxDialogButtons.OK, MahApps.Metro.IconPacks.PackIconFontAwesomeKind.TimesCircleOutline, Colors.Crimson);
                msgBoxDialog.Owner = this;
                msgBoxDialog.ShowDialog();
            }
        }

        /// <summary>
        /// Event when user click on Change SFTP settings to default button
        /// </summary>
        private void OnBtnChangeSftpToDefaultClick(object sender, RoutedEventArgs e)
        {
            MessageBoxDialog msgDialog = new MessageBoxDialog("Obnovit původní nastavení",
                "Opravdu chcete obnovit původní nastavení připojení k serveru?", MessageBoxDialogButtons.YesNo, PackIconFontAwesomeKind.QuestionCircleOutline,
                Colors.Goldenrod);
            msgDialog.Owner = this;
            var dlgResult = msgDialog.ShowDialog();
            if (dlgResult == true)
            {
                DataIntegrator.Settings.SftpServer = SftpLogin.HostAddress;
                DataIntegrator.Settings.SftpUserName = SftpLogin.UserName;
                DataIntegrator.Settings.SftpPassword = SftpLogin.Password;
                DataIntegrator.Settings.SftpRootDirectory = SftpLogin.MainDir;

                RefreshSftpSettings();
            }
        }

        /// <summary>
        /// Refresh SFTP settings boxes
        /// </summary>
        private void RefreshSftpSettings()
        {
            tbxHostAddress.Text = DataIntegrator.Settings.SftpServer;
            tbxUserName.Text = DataIntegrator.Settings.SftpUserName;
            pwbPassword.Password = DataIntegrator.Settings.SftpPassword;
            tbxRootDir.Text = DataIntegrator.Settings.SftpRootDirectory;
        }
    }

    /// <summary>
    /// Enumeration of available tabs in settings tab-control
    /// </summary>
    public enum SettingsTabs
    {
        Application = 0,
        Audio = 1,
        User = 2,
        Server = 3
    }
}
