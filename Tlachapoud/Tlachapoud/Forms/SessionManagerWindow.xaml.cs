﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

using Tlachapoud.Core.Data;
using System.IO;
using System.Xml.Linq;
using Tlachapoud.Core.IO;

namespace Tlachapoud.Forms
{
    /// <summary>
    /// Session Manager Window for logged-in user
    /// </summary>
    public partial class SessionManagerWindow : MetroWindow
    {
        /// <summary>
        /// Local property storing UserProfile of logged-in user
        /// </summary>
        private UserProfile LoggedUser { get; set; }

        /// <summary>
        /// SessionManagerWindow constructor
        /// </summary>
        /// <param name="loggedUser"></param>
        public SessionManagerWindow(UserProfile loggedUser)
        {
            InitializeComponent();
            LoggedUser = loggedUser;

            lblUserName.Content = string.Format("{0} {1} ({2})", LoggedUser.FirstName.ToUpper(), LoggedUser.LastName.ToUpper(), LoggedUser.Email);

            // If there is no profile folder, create a new one
            if (!LoggedUser.ProfileFolderPath.Exists)
                LoggedUser.ProfileFolderPath.Create();

            RefreshSessions();
            lbxSessions.Focus();
        }

        /// <summary>
        /// Refrest list of sessions in ListBox control
        /// </summary>
        private void RefreshSessions()
        {
            List<Session> dataSource = new List<Session>();

            // Load already created sessions
            var sessions = LoggedUser.ProfileFolderPath.GetDirectories();
            foreach (var session in sessions)
            {
                dataSource.Add(Session.Load(session));
            }
            lbxSessions.ItemsSource = null;
            lbxSessions.ItemsSource = dataSource;
        }

        /// <summary>
        /// Event handler when user click on Logoff button
        /// </summary>
        /// <param name="sender">Button control</param>
        /// <param name="e">Routed event arguments</param>
        private void OnBtnLogoffClick(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.RemeberedUser = null;
            Properties.Settings.Default.Save();
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.Show();
            Close();
        }

        /// <summary>
        /// Event handler when user click on Open session button
        /// </summary>
        /// <param name="sender">Button control</param>
        /// <param name="e">Routed event arguments</param>
        private void OnBtnOpenSessionClick(object sender, RoutedEventArgs e)
        {
            // Exit this method if there are no sessions
            if (lbxSessions.ItemsSource == null || lbxSessions.Items.Count == 0)
                return;

            DataIntegrator dataIntegrator = new DataIntegrator(LoggedUser, (Session)lbxSessions.SelectedItem);
            MainWindow mainWindow = new MainWindow(LoggedUser, (Session)lbxSessions.SelectedItem, dataIntegrator);
            mainWindow.Show();
            Close();
        }

        /// <summary>
        /// Event handler when user click on Create a new session button
        /// </summary>
        /// <param name="sender">Button control</param>
        /// <param name="e">Routed event arguments</param>
        private async void OnBtnCreateSessionClick(object sender, RoutedEventArgs e)
        {
            var pom = await this.ShowInputAsync("Nové sezení", "Prosím, zadejte název nového sezení", 
                new MetroDialogSettings() { AffirmativeButtonText="vytvořit", NegativeButtonText="zrušit" });

            if (pom == null)
                return;

            // Bad session name
            if (pom.Trim().Length == 0)
            {
                await this.ShowMessageAsync("Sezení nelze vytvořit", "Zadali jste neplatný název sezení. Prosím, zadejte smysluplný název");
                return;
            }

            // Session with this name already exist
            if (LoggedUser.ProfileFolderPath.GetDirectories().Contains(
                new DirectoryInfo(Path.Combine(LoggedUser.ProfileFolderPath.FullName, pom))))
            {
                await this.ShowMessageAsync("Sezení nelze vytvořit", "Sezení se zadaným názvem již existuje. Prosím, zadejte jiný název");
                return;
            }

            // Create a new session
            var sessionFolderPath = Path.Combine(LoggedUser.ProfileFolderPath.FullName, pom);
            Directory.CreateDirectory(sessionFolderPath);

            // Copy Phrases.xml to the Profile data session folder
            XDocument appPhrases = XDocument.Load(ContentDataFile.PhrasesFilePath);
            appPhrases.Save(Path.Combine(sessionFolderPath, "phrases.xml"));

            // Copy Settings.xml to the Profile data session folder
            XDocument userSettings = XDocument.Load(LoggedUser.ProfileSettingsPath.FullName);
            userSettings.Save(Path.Combine(sessionFolderPath, "settings.xml"));

            // Copy CheckModules.cfg to the Profile data session folder
            XDocument checkModulesConfig = XDocument.Load(ContentDataFile.CheckModulesConfigFilePath);
            checkModulesConfig.Save(Path.Combine(sessionFolderPath, "CheckModules.cfg"));

            RefreshSessions();
        }

        /// <summary>
        /// Event handler when some session in ListBox has been selected
        /// </summary>
        /// <param name="sender">ListBox control</param>
        /// <param name="e">Selection changed event arguments</param>
        private void OnLbxSessionsSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnOpenSession.IsEnabled = true;
        }

        /// <summary>
        /// Event handler when user double-click some session in ListBox
        /// </summary>
        /// <param name="sender">MouseButton</param>
        /// <param name="e">Mouse button event arguments</param>
        private void OnLbxSessionsMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            OnBtnOpenSessionClick(null, null);
        }
    }
}
