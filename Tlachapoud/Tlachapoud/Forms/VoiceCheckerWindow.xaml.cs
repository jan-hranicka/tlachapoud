﻿using System.Windows;
using System.Windows.Controls;

// Import MahApps Controls library
using MahApps.Metro.Controls;

// Import Tlachapoud libraries
using Tlachapoud.Forms.VoiceCheckerPages;
using Tlachapoud.Core.Data;

namespace Tlachapoud.Forms
{
    /// <summary>
    /// Interaction logic for VoiceCheckerWindow.xaml
    /// </summary>
    public partial class VoiceCheckerWindow : MetroWindow
    {
        /// <summary>
        /// Data Integrator storing all application data
        /// </summary>
        public DataIntegrator DataIntegrator { get; private set; }
        /// <summary>
        /// Auxiliary object referencing OpenSettings button in this window
        /// </summary>
        public Button SettingsButton { get; private set; }

        public bool IsHideInstructionsChecked { get; set; }

        // Auxiliary Counter
        private int _counter = 0;

        /// <summary>
        /// Create a new Voice Checker Window
        /// </summary>
        /// <param name="dataIntegrator"></param>
        public VoiceCheckerWindow(DataIntegrator dataIntegrator)
        {
            InitializeComponent();

            SettingsButton = btnOpenSettings;
            DataIntegrator = dataIntegrator;

            if (DataIntegrator.Settings.VoiceCheckerHideInstructions)
            {
                btnNext.Visibility = Visibility.Hidden;
                frmContent.NavigationService.Navigate(new CheckingPage(DataIntegrator, this));
            }
            else
                frmContent.NavigationService.Navigate(new InstructionsPage(this));
        }

        /// <summary>
        /// Handle user's click on Back button
        /// </summary>
        private void OnBtnBackClick(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn.Visibility == Visibility.Visible)
            {
                frmContent.NavigationService.GoBack();
                btnNext.Visibility = Visibility.Visible;
            }

            if (--_counter == 0)
                btn.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Handle user's click on Next button
        /// </summary>
        private void OnBtnNextClick(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            if (frmContent.NavigationService.CanGoForward)
                frmContent.NavigationService.GoForward();
            else
            {
                frmContent.NavigationService.Navigate(new CheckingPage(DataIntegrator, this));
            }
            btnBack.Visibility = Visibility.Visible;

            if (++_counter >= 1)
                btn.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Handler user's click on Settings button and open audio settings
        /// </summary>
        private void OnBtnOpenSettingsClick(object sender, RoutedEventArgs e)
        {
            SettingsWindow settingsWindow = new SettingsWindow(DataIntegrator, SettingsTabs.Audio);
            settingsWindow.ShowDialog();
        }

        /// <summary>
        /// Disable window closing if Voice Checker has not been finished yet
        /// </summary>
        private void OnWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (DialogResult != true)
                e.Cancel = true;
        }
    }
}
