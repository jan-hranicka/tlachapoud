﻿using System;
using System.Threading;
using System.Net.NetworkInformation;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.IO;
using System.ComponentModel;
using System.Reflection;
using Microsoft.Win32;

// Import Tlachapoud Core modules
using Tlachapoud.Core.Audio;
using Tlachapoud.Core.Audio.DirectSound;
using Tlachapoud.Core.Audio.CheckModules;
using Tlachapoud.Core.Data;
using Tlachapoud.Core;
using Tlachapoud.Core.IO;
using Tlachapoud.Controls;
using Tlachapoud.Controls.Dialogs;
using Tlachapoud.Core.Net;

// Import MahApps libraries
using MahApps.Metro.Controls;
using MahApps.Metro.IconPacks;
using MahApps.Metro.Controls.Dialogs;

using Ionic.Zip;
using FluentFTP;

// Import NAudio library (WASAPI)
using NAudio.CoreAudioApi.Interfaces;
using NAudio.CoreAudioApi;
using System.Xml.Linq;

namespace Tlachapoud.Forms
{
    /// <summary>
    /// Main Application Window
    /// </summary>
    public partial class MainWindow : MetroWindow, INotifyPropertyChanged, IMMNotificationClient
    {
        #region Audio processing objects
        private DirectSoundAudioRecorder audioRecorder;
        private DirectSoundAudioPlayer audioPlayer;
        private DirectSoundAudioRecorder audioMonitor;
        private MMDeviceEnumerator deviceEnumerator;
        #endregion Audio processing objects

        /// <summary>
        /// Event for INotifyPropertyChanged interface
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Currently set opening Tab in settigns
        /// </summary>
        private SettingsTabs openingTab = SettingsTabs.Application;
        /// <summary>
        /// (PRIVATE) Data integrator for storing application data
        /// </summary>
        private DataIntegrator DataIntegrator { get; set; }

        #region Binding Properties
        /// <summary>
        /// Phrase text font size
        /// </summary>
        private double _phraseFontSize;
        public double PhraseFontSize
        {
            get { return _phraseFontSize; }
            set { _phraseFontSize = value; NotifyPropertyChanged("PhraseFontSize"); }
        }
        /// <summary>
        /// Button text font size
        /// </summary>
        private double _buttonFontSize;
        public double ButtonFontSize
        {
            get { return _buttonFontSize; }
            set { _buttonFontSize = value; NotifyPropertyChanged("ButtonFontSize"); }
        }
        /// <summary>
        /// Player button size
        /// </summary>
        private double _playerButtonSize;
        public double PlayerButtonSize
        {
            get { return _playerButtonSize; }
            set { _playerButtonSize = value; NotifyPropertyChanged("PlayerButtonSize"); }
        }
        /// <summary>
        /// Recorder button size
        /// </summary>
        private double _recorderButtonSize;
        public double RecorderButtonSize
        {
            get { return _recorderButtonSize; }
            set { _recorderButtonSize = value; NotifyPropertyChanged("RecorderButtonSize"); }
        }
        /// <summary>
        /// Navigation button size
        /// </summary>
        private double _navigationButtonSize;
        public double NavigationButtonSize
        {
            get { return _navigationButtonSize; }
            set { _navigationButtonSize = value; NotifyPropertyChanged("NavigationButtonSize"); }
        }
        /// <summary>
        /// Notify Property Changed event handler
        /// </summary>
        /// <param name="propertyName">Name of the property for callback</param>
        private void NotifyPropertyChanged(string propertyName)
        {

            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion Binding Properties

        // Auxiliary variable indicating whether application should be closed or wait for upload of data to SFTP
        private bool closeAppAfterUpload = false;

        /// <summary>
        /// Create a Main Window object
        /// </summary>
        /// <param name="userProfile">UserProfile linked to the opened recording session</param>
        /// <param name="currentSession">Recording Session</param>
        public MainWindow(UserProfile userProfile, Session currentSession, DataIntegrator dataIntegrator)
        {
            DataContext = this;
            DataIntegrator = dataIntegrator;

            // Set Binding Properties
            PhraseFontSize = DataIntegrator.Settings.AppPhraseFontSize;
            ButtonFontSize = DataIntegrator.Settings.AppButtonFontSize;
            PlayerButtonSize = DataIntegrator.Settings.AppPlayerButtonSize;
            RecorderButtonSize = DataIntegrator.Settings.AppRecorderButtonsSize;
            NavigationButtonSize = DataIntegrator.Settings.AppNavigationButtonSize;

            InitializeComponent();

            // Update Phrase Font Size Numeric Updown & Do calibration
            nudPhraseFontSize.Value = PhraseFontSize;
            nudPhraseFontSize.Minimum = Calibration.Text.PhraseFontSize.Min;
            nudPhraseFontSize.Maximum = Calibration.Text.PhraseFontSize.Max;


#if NOSESSIONS
            miApplicationGroup.Items.RemoveAt(0);
#else
            // Add session name to window title
            Title = string.Format("{0} [{1}]", Title, currentSession.Name);
#endif
#if !DEBUG
            miApplicationGroup.Items.RemoveAt(miApplicationGroup.Items.Count - 2);
#endif

            miLogoff.Header = string.Format("Odhlásit se ({0} {1})", DataIntegrator.User.FirstName, DataIntegrator.User.LastName);
            // Set Phrase text
            tbkPhrase.Text = DataIntegrator.CurrentPhrase.Text;
            tbkPhraseId.Text = string.Format("FRÁZE #{0}", DataIntegrator.CurrentPhrase.Id + 1);
            // Set range progress bar
            rpbSessionProgress.Complete = DataIntegrator.PhrasesFile.RecordedPhrases;
            rpbSessionProgress.Incomplete = DataIntegrator.PhrasesFile.TotalPhrases - DataIntegrator.PhrasesFile.RecordedPhrases;

            // Network availability status event handler
            NetworkChange.NetworkAvailabilityChanged += OnNetworkAvailabilityChanged;
            DataIntegrator.CheckModules.AudioDataChecked += OnAudioDataChecked;

            // Initialize Audio Recorder
            audioRecorder = new DirectSoundAudioRecorder(DataIntegrator.Settings.CaptureDevice);
            // Initialize Audio Player
            audioPlayer = new DirectSoundAudioPlayer(DataIntegrator.Settings.RenderDevice);
            // Initialize Audio Monitor
            audioMonitor = new DirectSoundAudioRecorder(DataIntegrator.Settings.CaptureDevice);
            audioMonitor.PeakVolumeCalculated += OnAudioMonitorPeakVolumeCalculated;

            if (audioMonitor.State != AudioRecorderState.NoCaptureDevice)
                audioMonitor.BeginMonitoring();

            UpdateRecordingProgress();

            // Register MMDevice enumerator and Notification Callback
            deviceEnumerator = new MMDeviceEnumerator();
            deviceEnumerator.RegisterEndpointNotificationCallback(this);
            // Update box with IO and Network information
            UpdateInfoBox();

            // Flyout content
            TextRange textRange = new TextRange(rtxFlyoutApp.Document.ContentStart, rtxFlyoutApp.Document.ContentEnd);
            var assembly = Assembly.GetExecutingAssembly();
            string resourceName = "Tlachapoud.Resources.RichTexts.FlyoutApp.rtf";
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                textRange.Load(stream, DataFormats.Rtf);
            }

            textRange = new TextRange(rtxFlyoutSettings.Document.ContentStart, rtxFlyoutSettings.Document.ContentEnd);
            assembly = Assembly.GetExecutingAssembly();
            resourceName = "Tlachapoud.Resources.RichTexts.FlyoutSettings.rtf";
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                textRange.Load(stream, DataFormats.Rtf);
            }
            chbNotShowStartupHelp.IsChecked = !DataIntegrator.Settings.AppShowStartupHelp;
            flyoutMain.IsOpen = DataIntegrator.Settings.AppShowStartupHelp;
        }

        /// <summary>
        /// Event handler when user checked automatic upload to SFTP on startup
        /// </summary>
        private void OnWindowContentRendered(object sender, EventArgs e)
        {
            if (DataIntegrator.Settings.AppAutomaticUploadToSftp)
                OnExportSftpClick(null, null);

            if (DataIntegrator.CaptureDeviceNotificationMsg != null)
            {
                MessageBoxDialog msgDialog = new MessageBoxDialog("Vstupní zařízení není aktivní",
                    DataIntegrator.CaptureDeviceNotificationMsg, MessageBoxDialogButtons.OK, PackIconFontAwesomeKind.MicrophoneSlash, Colors.Goldenrod);
                msgDialog.Owner = this;
                msgDialog.ShowDialog();
            }
            if (DataIntegrator.RenderDeviceNotificationMsg != null)
            {
                MessageBoxDialog msgDialog = new MessageBoxDialog("Vstupní zařízení není aktivní",
                    DataIntegrator.RenderDeviceNotificationMsg, MessageBoxDialogButtons.OK, PackIconFontAwesomeKind.VolumeOff, Colors.Goldenrod);
                msgDialog.Owner = this;
                msgDialog.ShowDialog();
            }

        }

        /// <summary>
        /// Event when user close the flyout
        /// </summary>
        private void OnFlyoutClosingFinished(object sender, RoutedEventArgs e)
        {
            DataIntegrator.Settings.AppShowStartupHelp = !(bool)chbNotShowStartupHelp.IsChecked;
            DataIntegrator.SettingsFile.Update();
        }

        /// <summary>
        /// Show capture device input level
        /// </summary>
        private void OnAudioMonitorPeakVolumeCalculated(object sender, MasterPeakVolumeEventArgs args)
        {
            pbrInputLevel.Value = args.PeakVolume * 100;
        }

        /// <summary>
        /// Handle Internet connection availability events
        /// </summary>
        private void OnNetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(delegate ()
            {
                lblInfoInternet.Content = NetworkInterface.GetIsNetworkAvailable() ? "Připojeno" : "Nepřipojeno";
                if (NetworkInterface.GetIsNetworkAvailable())
                {
                    lblInfoInternet.Foreground = (SolidColorBrush)FindResource("AccentColorBrush");
                    icoNetwork.Kind = PackIconModernKind.Network;
                }
                else
                {
                    lblInfoInternet.Foreground = new SolidColorBrush(Colors.Red);
                    icoNetwork.Kind = PackIconModernKind.NetworkDisconnect;
                }
            }));
        }

        /// <summary>
        /// Global handler fixing visual bug when circle metro buttons are disabled but icons stay colored
        /// </summary>
        private void OnButtonIsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Button btn = sender as Button;
            PackIconFontAwesome icon = btn.Content as PackIconFontAwesome;
            if (icon != null)
                icon.Opacity = btn.IsEnabled ? 1.0 : .25;
        }

        /// <summary>
        /// Update information box
        /// </summary>
        public void UpdateInfoBox()
        {
            lblInfoCaptureDevice.Content = DataIntegrator.Settings.CaptureDevice?.FriendlyName ?? "Mikrofon není nastaven";
            lblInfoCaptureDevice.Foreground = lblInfoCaptureDevice.Content.ToString() == "Mikrofon není nastaven" ? new SolidColorBrush(Colors.Crimson) : new SolidColorBrush(Colors.Black);
            lblInfoRenderDevice.Content = DataIntegrator.Settings.RenderDevice?.FriendlyName ?? "Reproduktory nejsou nastaveny";

            var activeCheckModules = DataIntegrator.CheckModuleConfigs.Where(m => m.IsActive).Select(m => m.ModuleName);
            lblInfoCheckModules.Content = string.Join(", ", activeCheckModules);
            if (string.IsNullOrEmpty(lblInfoCheckModules.Content.ToString()))
            {
                lblInfoCheckModules.Foreground = new SolidColorBrush(Colors.Crimson);
                lblInfoCheckModules.Content = "Kontrolní moduly jsou deaktivovány";
            }
            else
                lblInfoCheckModules.Foreground = new SolidColorBrush(Colors.Black);

            lblInfoInternet.Content = NetworkInterface.GetIsNetworkAvailable() ? "Připojeno" : "Nepřipojeno";
            if (NetworkInterface.GetIsNetworkAvailable())
                lblInfoInternet.Foreground = (SolidColorBrush)FindResource("AccentColorBrush");
            else
                lblInfoInternet.Foreground = new SolidColorBrush(Colors.Red);
        }

        /// <summary>
        /// Handle user's click on Open audio settings in main menu strip
        /// </summary>
        /// <param name="sender">Menu item object</param>
        /// <param name="e">Routed event arguments</param>
        private void OnBtnAudioSettingsClick(object sender, RoutedEventArgs e)
        {
            openingTab = SettingsTabs.Audio;
            OnOpenSettingsClick(null, null);
        }

        /// <summary>
        /// Update recording progress, disable/enable navigation buttons depending on previous/following phrase etc.
        /// </summary>
        private void UpdateRecordingProgress()
        {
            tbkPhrase.Text = DataIntegrator.CurrentPhrase.Text;
            tbkPhraseId.Text = string.Format("FRÁZE #{0}", DataIntegrator.CurrentPhrase.Id + 1);

            // Disable or enable recording button
            btnStartStopRecording.IsEnabled = audioRecorder.State != AudioRecorderState.NoCaptureDevice;

            // Set PREVIOUS buttons
            bool canGoPrevious = DataIntegrator.PhrasesFile.CanGoPrevious;
            bool canGoPreviousSkipped = DataIntegrator.PhrasesFile.CanGoPreviousSkipped;
            btnGoPrevious.IsEnabled = canGoPrevious;
            btnGoFirst.IsEnabled = canGoPrevious;
            btnGoPreviousSkipped.IsEnabled = canGoPreviousSkipped;
            if (canGoPreviousSkipped)
                badgeGoPreviousSkipped.BadgeBackground = new SolidColorBrush(Colors.Orange);
            else
                badgeGoPreviousSkipped.BadgeBackground = new SolidColorBrush(Colors.LightGray);

            // Set FOLLOWING buttons
            bool canGoFollowing = DataIntegrator.PhrasesFile.CanGoFollowing;
            bool canGoFollowingSkipped = DataIntegrator.PhrasesFile.CanGoFollowingSkipped;
            btnGoFollowing.IsEnabled = canGoFollowing;
            btnGoLast.IsEnabled = canGoFollowing;
            btnGoFollowingSkipped.IsEnabled = canGoFollowingSkipped;
            if (canGoFollowingSkipped)
                badgeGoFollowingSkipped.BadgeBackground = new SolidColorBrush(Colors.Orange);
            else
                badgeGoFollowingSkipped.BadgeBackground = new SolidColorBrush(Colors.LightGray);

            rpbSessionProgress.Complete = DataIntegrator.PhrasesFile.RecordedPhrases;
            rpbSessionProgress.Incomplete = DataIntegrator.PhrasesFile.TotalPhrases - DataIntegrator.PhrasesFile.RecordedPhrases;

            // Update recorded waves for current phrase
            var allRecordedPhrases = DataIntegrator.CurrentPhrase.FNames.Concat(DataIntegrator.CurrentPhrase.Rejected);
            if (allRecordedPhrases.Any())
            {
                lbxRecordedWaves.ItemsSource = null;
                //lbxRecordedWaves.ItemsSource = allRecordedPhrases.OrderByDescending(x => x.RecordTime).ThenBy(x => (int)x.CheckResult);
                lbxRecordedWaves.ItemsSource = allRecordedPhrases.OrderBy(x => x.CheckResult).ThenByDescending(x => x.RecordTime);
                lbxRecordedWaves.SelectedIndex = 0;
            }
            else
            {
                lbxRecordedWaves.ItemsSource = null;
            }

            switch (DataIntegrator.CurrentPhrase.State)
            {
                case PhraseState.Recorded:
                    {
                        icoPhraseStatus.Kind = PackIconFontAwesomeKind.Check;
                        icoPhraseStatus.Foreground = new SolidColorBrush(Colors.Green);
                        lblPhraseStatus.Content = "ÚSPĚŠNĚ NAHRÁNO";
                        btnSkipRecording.IsEnabled = false;
                        break;
                    }
                case PhraseState.Unrecorded:
                    {
                        if (DataIntegrator.CurrentPhrase.FNames.Count == 0 && DataIntegrator.CurrentPhrase.Rejected.Count == 0)
                        {
                            icoPhraseStatus.Kind = PackIconFontAwesomeKind.Question;
                            icoPhraseStatus.Foreground = new SolidColorBrush(Colors.Gray);
                            lblPhraseStatus.Content = "ČEKÁ NA NAHRÁNÍ";
                        }
                        else
                        {
                            icoPhraseStatus.Kind = PackIconFontAwesomeKind.Times;
                            icoPhraseStatus.Foreground = new SolidColorBrush(Colors.Red);
                            lblPhraseStatus.Content = "NEPROŠLA KONTROLOU";
                        }
                        btnSkipRecording.IsEnabled = true;
                        break;
                    }
                case PhraseState.Skipped:
                    {
                        icoPhraseStatus.Kind = PackIconFontAwesomeKind.Lock;
                        icoPhraseStatus.Foreground = new SolidColorBrush(Colors.Orange);
                        lblPhraseStatus.Content = "ODMÍTNUTO";
                        btnStartStopRecording.IsEnabled = false;
                        btnSkipRecording.IsEnabled = true;
                        break;
                    }
            }

            if (DataIntegrator.CurrentPhrase.State == PhraseState.Skipped)
            {
                icoSkipRecording.Kind = PackIconFontAwesomeKind.UnlockAlt;
                tbkSkip.Text = "ODEMKNOUT";
            }
            else
            {
                icoSkipRecording.Kind = PackIconFontAwesomeKind.Lock;
                tbkSkip.Text = "ODMÍTNOUT";
            }
        }

        #region Navigation Control Panel Handlers
        /// <summary>
        /// Handle user's click on Go Previous button
        /// </summary>
        private void OnBtnGoPreviousClick(object sender, RoutedEventArgs e)
        {
            if (!((Button)sender).IsEnabled)
                return;

            DataIntegrator.CurrentPhrase = DataIntegrator.PhrasesFile.GoPrevious();
            UpdateRecordingProgress();
        }

        /// <summary>
        /// Handle user's click on Go First button
        /// </summary>
        private void OnBtnGoFirstClick(object sender, RoutedEventArgs e)
        {
            if (!((Button)sender).IsEnabled)
                return;

            DataIntegrator.CurrentPhrase = DataIntegrator.PhrasesFile.GetFirst();
            UpdateRecordingProgress();
        }

        /// <summary>
        /// Handle user's click on Go Following button
        /// </summary>
        private void OnBtnGoFollowingClick(object sender, RoutedEventArgs e)
        {
            if (!((Button)sender).IsEnabled)
                return;

            DataIntegrator.CurrentPhrase = DataIntegrator.PhrasesFile.GoFollowing();
            UpdateRecordingProgress();
        }

        /// <summary>
        /// Handle user's click on Go Last button
        /// </summary>
        private void OnBtnGoLastClick(object sender, RoutedEventArgs e)
        {
            if (!((Button)sender).IsEnabled)
                return;

            DataIntegrator.CurrentPhrase = DataIntegrator.PhrasesFile.GetLast();
            UpdateRecordingProgress();
        }

        /// <summary>
        /// Handle user's click on Go Previous Skipped button
        /// </summary>
        private void OnBtnGoPreviousSkippedClick(object sender, RoutedEventArgs e)
        {
            if (!((Button)sender).IsEnabled)
                return;

            DataIntegrator.CurrentPhrase = DataIntegrator.PhrasesFile.GoPreviousSkipped();
            UpdateRecordingProgress();
        }

        /// <summary>
        /// Handle user's click on Go Following Skipped button
        /// </summary>
        private void OnBtnGoFollowingSkippedClick(object sender, RoutedEventArgs e)
        {
            if (!((Button)sender).IsEnabled)
                return;

            DataIntegrator.CurrentPhrase = DataIntegrator.PhrasesFile.GoFollowingSkipped();
            UpdateRecordingProgress();
        }
        #endregion Navigation Control Panel Handlers

        #region Audio Recorder Control Panel Handlers
        /// <summary>
        /// Event handler when user click on Start recording button
        /// Stop Audio Player if necessary, disable controls and begin audio recording
        /// </summary>
        /// <param name="sender">StartRecording button</param>
        /// <param name="e">Routed event arguments</param>
        private async void OnBtnStartStopRecordingClick(object sender, RoutedEventArgs e)
        {
            if (audioRecorder.State != AudioRecorderState.Recording && audioRecorder.State != AudioRecorderState.Monitoring)
            {
                // Check whether user is using device stored in settings
                if (DataIntegrator.SettingsFile.IsUsingSetCaptureDevice(DataIntegrator.Settings.CaptureDevice) == false)
                {
                    var dialogResult = await this.ShowMessageAsync("Nahrávací zařízení nesouhlasí s nastavením",
                        string.Format("Aktuální zařízení pro záznam zvuku {0} nesouhlasí se zařízením, které je uloženo v nastavení aplikace. Chcete použít pro nahrávání aktuálně dostupné zařízení? Je silně doporučeno používat po celou dobu nahrávání jedno a to samé vstupní zařízení. Připojte tedy prosím Váš mikrofon a v nastavení aplikace se ujistěte, že používáte toto zařízení pro záznam zvuku.", DataIntegrator.Settings.CaptureDevice.FriendlyName),
                        MessageDialogStyle.AffirmativeAndNegative, new MetroDialogSettings() { AffirmativeButtonText = "CHCI NYNÍ POUŽÍT JINÉ ZAŘÍZENÍ", NegativeButtonText = "PŘIPOJÍM A ZMĚNÍM ZAŘÍZENÍ V NASTAVENÍ", DefaultButtonFocus = MessageDialogResult.Negative });
                    if (dialogResult == MessageDialogResult.Affirmative)
                    {
                        //DataIntegrator.SettingsFile.Update();
                        audioRecorder.Dispose();
                        audioRecorder.ChangeDevice(DataIntegrator.Settings.CaptureDevice);
                    }
                    else
                        return;
                }

                if (DataIntegrator.SettingsFile.IsUsingSetCaptureDevice(DataIntegrator.Settings.CaptureDevice) == null)
                {
                    var dialogResult = await this.ShowMessageAsync("Nastavené nahrávácí zařízení nebylo nalezeno",
                        "Zařízení pro záznam zvuku nebylo dosud v nastavení aplikace uloženo. Chcete aktuální zařízení uložit a použít pro nahrávání? Je silně doporučeno používat pro celé nahrávání jedno a to samé vstupní zařízení. Pokud nechcete, nastavte prosím vstupní zařízení v nastavení aplikace.",
                        MessageDialogStyle.AffirmativeAndNegative, new MetroDialogSettings() { AffirmativeButtonText = "ANO, POUŽÍT", NegativeButtonText = "NE, VYBERU JINÉ", DefaultButtonFocus = MessageDialogResult.Affirmative });
                    if (dialogResult == MessageDialogResult.Affirmative)
                    {
                        //DataIntegrator.SettingsFile.Update();
                        audioRecorder.Dispose();
                        audioRecorder.ChangeDevice(DataIntegrator.Settings.CaptureDevice);
                    }
                    else
                        return;
                }

                // Disable buttons & menu
                List<Control> activeButtons = new List<Control>() {
                btnGoFirst, btnGoPrevious, btnGoPreviousSkipped, btnGoFollowing, btnGoFollowingSkipped, btnGoLast, btnSkipRecording, btnAudioSettings, menuMain
            };
                activeButtons.ForEach(btn => btn.IsEnabled = false);

                // Dispose audio player
                audioPlayer.Dispose();
                audioRecorder.BeginRecording();

                // Update button to "Stop Recording" button
                tbkStartStopRecording.Text = "ZASTAVIT";
                icoStartStopRecording.Kind = PackIconFontAwesomeKind.Stop;
                icoStartStopRecording.Foreground = new SolidColorBrush(Colors.Orange);
            }
            else
            {
                // Stop Audio Recorder
                audioRecorder.StopRecording();

                // Update button back to "Start Recording" button
                tbkStartStopRecording.Text = "NAHRÁVAT";
                icoStartStopRecording.Kind = PackIconFontAwesomeKind.Circle;
                icoStartStopRecording.Foreground = new SolidColorBrush(Colors.Red);

                // Load recorded audio from Memory
                audioPlayer.Load(audioRecorder.GetStream());

                // Run check modules
                DataIntegrator.CheckModules.Check(audioPlayer);
            }
        }

        /// <summary>
        /// Event handler when audio data have been checked by active check modules
        /// </summary>
        /// <param name="audioData">Checked audio data</param>
        /// <param name="args">Check module event arguments</param>
        private void OnAudioDataChecked(CorpusRecording.IAudioData audioData, CheckModuleEventArgs args)
        {
            audioPlayer.Dispose();
            audioRecorder.Dispose();

            // If Result=Approved, save it to WavesApproved
            if (args.Result == CheckModuleResult.Approved)
            {
                // Move temporary Wave file to ApprovedWaves folder
                var waveFileInfo = new FileInfo(Path.Combine(DataIntegrator.Session.WavesRecordedFolderPath.FullName, DataIntegrator.CurrentPhrase.GetFName().AddExtension("wav")));
                audioRecorder.WriteToFile(waveFileInfo.FullName);

                WaveFile waveFileApproved = new WaveFile(waveFileInfo,
                                                         audioRecorder.RecordTime,
                                                         audioRecorder.Duration,
                                                         CheckModuleResult.Approved,
                                                         WaveFileTag.NotSpecified,
                                                         new List<CheckModuleState>() { CheckModuleState.Fine },
                                                         DataIntegrator.Settings.CaptureDevice.ID);

                DataIntegrator.CurrentPhrase.State = PhraseState.Recorded;
                DataIntegrator.CurrentPhrase.FNames.Add(waveFileApproved);
                DataIntegrator.PhrasesFile.Update(DataIntegrator.CurrentPhrase, waveFileApproved);

                // Show Voice Checker if it is Nth phrase today
#if VOICECHECKER
                if (DataIntegrator.PhrasesFile.IsNthPhraseToday(DataIntegrator.Settings.VoiceCheckerInterval))
                {
                    VoiceCheckerWindow voiceCheckerWindow = new VoiceCheckerWindow(DataIntegrator);
                    voiceCheckerWindow.ShowDialog();
                }
#endif
            }
            else
            {
                // If Result=Rejected, save it to WavesRejected
                CheckResultDialog checkResultDialog = new CheckResultDialog("Nahrávka neprošla kontrolními moduly", "Kvalita nahrané fráze neodpovídá požadavkům aplikace. Kontrola zjistila následující nedostatky:", args.Messages);
                checkResultDialog.Owner = this;
                checkResultDialog.ShowDialog();

                // Move temporary Wave file to ApprovedWaves folder
                var waveRejectedFileInfo = new FileInfo(Path.Combine(DataIntegrator.Session.WavesRejectedFolderPath.FullName, DataIntegrator.CurrentPhrase.GetRejectedFName().AddExtension("wav")));
                audioRecorder.WriteToFile(waveRejectedFileInfo.FullName);

                WaveFile waveFile = new WaveFile(waveRejectedFileInfo,
                                                 audioRecorder.RecordTime,
                                                 audioRecorder.Duration,
                                                 CheckModuleResult.Rejected,
                                                 WaveFileTag.NotSpecified,
                                                 args.Messages.Select(m => m.State),
                                                 DataIntegrator.Settings.CaptureDevice.ID);

                // If recording is too short to do a check or another unspecified state occurs, tag it as "Bad"
                var badStates = new List<CheckModuleState>() { CheckModuleState.WaveTooShortIntensity, CheckModuleState.WaveTooShortPause, CheckModuleState.Unknown };
                if (waveFile.States.Any(badStates.Contains))
                    waveFile.Tag = WaveFileTag.Bad;

                DataIntegrator.CurrentPhrase.Rejected.Add(waveFile);
                DataIntegrator.PhrasesFile.Update(DataIntegrator.CurrentPhrase, waveFile);

                if (DataIntegrator.Settings.CheckModuleAutomaticSkip && DataIntegrator.CurrentPhrase.Rejected.Count() >= 3 && DataIntegrator.CurrentPhrase.State == PhraseState.Unrecorded)
                {
                    OnBtnSkipRecordingClick(null, null);
                }
            }

            // Enable controls
            List<Control> inactiveButtons = new List<Control>() {
                btnGoFirst, btnGoPrevious, btnGoPreviousSkipped, btnGoFollowing, btnGoFollowingSkipped, btnGoLast, btnSkipRecording, btnAudioSettings, menuMain
            };
            inactiveButtons.ForEach(btn => btn.IsEnabled = true);

            UpdateRecordingProgress();
            if (DataIntegrator.Settings.AppAutomaticMoveToFollowing)
                OnBtnGoFollowingClick(btnGoFollowing, null);
        }

        /// <summary>
        /// Event handler when user click on Skip button
        /// Skip recording of the current phrase and mark it Skipped in XML
        /// </summary>
        /// <param name="sender">Skip button</param>
        /// <param name="e">Routed event arguments</param>
        private async void OnBtnSkipRecordingClick(object sender, RoutedEventArgs e)
        {
            if (DataIntegrator.CurrentPhrase.State == PhraseState.Skipped)
            {
                btnStartStopRecording.IsEnabled = true;
                btnSkipRecording.IsEnabled = false;
                tbkSkip.Text = "ODEMČENO";
                return;
            }

            MessageDialogResult dialogResult = MessageDialogResult.Affirmative;
            if (sender != null)
            {
                // Skip current Phrase and update
                dialogResult = await this.ShowMessageAsync("Odmítnout nahrávku", "Opravdu si přejete tuto nahrávku odmítnout? K jejímu nahrávání se můžete kdykoliv vrátit",
                MessageDialogStyle.AffirmativeAndNegative, new MetroDialogSettings() { AffirmativeButtonText = "ANO", NegativeButtonText = "NE", DefaultButtonFocus = MessageDialogResult.Affirmative });
            }

            if (dialogResult == MessageDialogResult.Affirmative)
            {
                DataIntegrator.CurrentPhrase.State = PhraseState.Skipped;
                DataIntegrator.PhrasesFile.Update(DataIntegrator.CurrentPhrase);
                rpbSessionProgress.Complete++;
                rpbSessionProgress.Incomplete--;
                UpdateRecordingProgress();
            }
        }
        #endregion Audio Recorder Control Panel Handlers

        #region Application MenuItem Group

        private void OnMiChangeSessionClick(object sender, RoutedEventArgs e)
        {
#if !NOSESSIONS

            SessionManagerWindow sessionManagerWindow = new SessionManagerWindow(CurrentUser);
            sessionManagerWindow.Show();
            Close();
#endif
        }

        private void OnMiLogoffClick(object sender, RoutedEventArgs e)
        {
            // If users is remembered, ask first
            bool? dialogResult;
            if (Properties.Settings.Default.RemeberedUser != null)
            {
                MessageBoxDialog msgDialog = new MessageBoxDialog("Odhlášení",
                "Váš uživatelský účet je zapamatován, odhlášením toto zapamatování zrušíte a při dalším startu aplikace se budete muset přihlásit. Opravdu chcete být odhlášeni?",
                MessageBoxDialogButtons.YesNo, PackIconFontAwesomeKind.QuestionCircleOutline,
                Colors.Goldenrod);
                msgDialog.Owner = this;
                dialogResult = msgDialog.ShowDialog();

            }
            else
            {
                MessageBoxDialog msgDialog = new MessageBoxDialog("Odhlášení",
                "Opravdu chcete být odhlášeni? Vaše nahraná data jsou průběžně ukládána, o žádná data nepřijdete.",
                MessageBoxDialogButtons.YesNo, PackIconFontAwesomeKind.QuestionCircleOutline);
                msgDialog.Owner = this;
                dialogResult = msgDialog.ShowDialog();
            }
            if (dialogResult == false)
                return;

            Properties.Settings.Default.RemeberedUser = null;
            Properties.Settings.Default.Save();

            LoginWindow loginWindow = new LoginWindow();
            loginWindow.Show();
            Close();
        }

        private void OnMiExitClick(object sender, RoutedEventArgs e)
        {
            MessageBoxDialog msgDialog = new MessageBoxDialog("Nahrát data na server",
                "Chcete před ukončením aplikace nahrávat Vaše zaznamenaná data na server?", MessageBoxDialogButtons.YesNo, PackIconFontAwesomeKind.Database);
            msgDialog.Owner = this;
            var dlgResult = msgDialog.ShowDialog();
            if (dlgResult == true)
            {
                closeAppAfterUpload = true;
                OnExportSftpClick(miExportSftp, null);
            }
            else
                Environment.Exit(0);
            
        }
        #endregion Application MenuItem Group      

        #region Help MenuItem Group
        private void OnMiAboutClick(object sender, RoutedEventArgs e)
        {
            AboutWindow aboutWindow = new AboutWindow();
            aboutWindow.Owner = this;
            aboutWindow.ShowDialog();
        }
        #endregion Help MenuItem Group       

        #region Audio Player Control Panel
        /// <summary>
        /// Event handler when user click on Play button to start rendering loaded (selected) Wave file
        /// </summary>
        /// <param name="sender">Play button</param>
        /// <param name="e">Routed event arguments</param>
        private void OnBtnPlayClick(object sender, RoutedEventArgs e)
        {
            // There is no render device
            if (audioPlayer.State == AudioPlayerState.NoRenderDevice)
                return;

            // When Player is in Paused state, just continue Playing
            if (audioPlayer.State == AudioPlayerState.Paused)
            {
                audioPlayer.Play();
                return;
            }

            // Initialize new player
            audioPlayer.Dispose();
            audioPlayer.Load(((WaveFile)lbxRecordedWaves.SelectedItem).Path);
            audioPlayer.Play();
        }

        /// <summary>
        /// Event handler when user click on Pause button to temporarily pause Wave file rendering
        /// </summary>
        /// <param name="sender">Pause button</param>
        /// <param name="e">Routed event arguments</param>
        private void OnBtnPauseClick(object sender, RoutedEventArgs e)
        {
            // There is no render device
            if (audioPlayer.State == AudioPlayerState.NoRenderDevice)
                return;

            audioPlayer.Pause();
        }

        /// <summary>
        /// Event handler when user click on Stop button to stop rendering loaded (selected) Wave file
        /// </summary>
        /// <param name="sender">Stop button</param>
        /// <param name="e">Routed event arguments</param>
        private void OnBtnStopClick(object sender, RoutedEventArgs e)
        {
            // There is no render device
            if (audioPlayer.State == AudioPlayerState.NoRenderDevice)
                return;

            audioPlayer.Stop();
        }
        #endregion Audio Player Control Panel 

        #region Settings MenuItem Group
        /// <summary>
        /// Event handler when user click on Settings menu item
        /// </summary>
        /// <param name="sender">Settings menu item</param>
        /// <param name="e">Routed event arguments</param>
        private void OnOpenSettingsClick(object sender, RoutedEventArgs e)
        {
            var tmpCaptureDevice = DataIntegrator.Settings.CaptureDevice;
            var tmpRenderDevice = DataIntegrator.Settings.RenderDevice;

            SettingsWindow settingsWindow = new SettingsWindow(DataIntegrator, openingTab);
            settingsWindow.Owner = this;
            var dlgResult = settingsWindow.ShowDialog();

            if (dlgResult == true)
            {
                // Update Binding Properties and Layout
                PhraseFontSize = DataIntegrator.Settings.AppPhraseFontSize;
                ButtonFontSize = DataIntegrator.Settings.AppButtonFontSize;
                RecorderButtonSize = DataIntegrator.Settings.AppRecorderButtonsSize;
                PlayerButtonSize = DataIntegrator.Settings.AppPlayerButtonSize;
                NavigationButtonSize = DataIntegrator.Settings.AppNavigationButtonSize;
                nudPhraseFontSize.Value = DataIntegrator.Settings.AppPhraseFontSize;

                // If Capture device has been changed, Init Recorder & Monitor again
                if (tmpCaptureDevice?.ID != DataIntegrator.Settings.CaptureDevice?.ID)
                {
                    audioRecorder.ChangeDevice(DataIntegrator.Settings.CaptureDevice);
                    audioMonitor.ChangeDevice(DataIntegrator.Settings.CaptureDevice);
                    audioMonitor.BeginMonitoring();
                }

                // If Render device has been changed, Init Player again
                if (tmpRenderDevice?.ID != DataIntegrator.Settings.RenderDevice?.ID)
                {
                    audioPlayer.Dispose();
                    audioPlayer = new DirectSoundAudioPlayer(DataIntegrator.Settings.RenderDevice);
                }

                // Re-Initialize Check Modules
                DataIntegrator.CheckModuleConfigs = DataIntegrator.CheckModuleConfigs.ForEach(m => m.IsActive = DataIntegrator.Settings.CheckModuleIntensityActive, m => m.ModuleName == AvailableCheckModules.Intensity);
                DataIntegrator.CheckModuleConfigs = DataIntegrator.CheckModuleConfigs.ForEach(m => m.IsActive = DataIntegrator.Settings.CheckModulePausesActive, m => m.ModuleName == AvailableCheckModules.PauseLen);
                DataIntegrator.CheckModules.Init(DataIntegrator.CheckModuleConfigs);
                UpdateInfoBox();
            }
            openingTab = SettingsTabs.Application;
            badgeSettings.Badge = "";
            UpdateRecordingProgress();
        }

        /// <summary>
        /// Load and replace Check Modules configuration file with a new one
        /// </summary>
        private void OnMiLoadConfigClick(object sender, RoutedEventArgs e) { SettingsWindow.LoadNewConfig(DataIntegrator); }

        /// <summary>
        /// Load Phrases XML file and merge it with the existing one
        /// </summary>
        private void OnMiLoadPhrasesClick(object sender, RoutedEventArgs e)
        {
            SettingsWindow.LoadNewPhrases(DataIntegrator);
            UpdateRecordingProgress();
        }
        #endregion Settings MenuItem Group

        #region Export MenuItem Group
        /// <summary>
        /// Export Check Modules configuration file
        /// </summary>
        private void OnMiExportConfigClick(object sender, RoutedEventArgs e) { SettingsWindow.ExportConfig(DataIntegrator); }


        /// <summary>
        /// Asynchronously export recorded audio data to a ZIP file
        /// </summary>
        /// <param name="sender">Export ZIP Menu Item</param>
        /// <param name="e">Routed event arguments</param>
        private async void OnExportZipClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.AddExtension = true;
            saveDialog.DefaultExt = "zip";
            saveDialog.FileName = string.Format("{0}_{1}_RecordedData.zip", DataIntegrator.User.FirstName, DataIntegrator.User.LastName);
            saveDialog.Filter = "archiv ZIP (*.zip) | *.zip";
            saveDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            //var dlgResult = saveDialog.ShowDialog();
            if (saveDialog.ShowDialog() == true)
            {
                var exportProgress = await this.ShowProgressAsync("Exportuji ZIP", "Data se exportují, vyčkejte prosím...");
                exportProgress.Minimum = 0;
                exportProgress.Maximum = 100;

                // Save user profile information to its directory to include this file into the ZIP
                string userInfoXml = Path.Combine(DataIntegrator.User.ProfileFolderPath.FullName, ContentDataFile.UserInfoFilename);
                DataIntegrator.User.SaveToXML(userInfoXml);

                await Task.Run(() =>
                {
                    // Delete file if it exists because Ionic.Zip cannot overwrite it
                    if (File.Exists(saveDialog.FileName))
                        File.Delete(saveDialog.FileName);

                    // Start zipping data
                    using (ZipFile zipFile = new ZipFile(saveDialog.FileName))
                    {
                        zipFile.Password = "ZcuFavNtisKky2017";
                        zipFile.ExtractExistingFile = ExtractExistingFileAction.OverwriteSilently;
                        zipFile.AddFile(DataIntegrator.Session.PhrasesFilePath.FullName, "");   // Phrases XML file
                        zipFile.AddFile(DataIntegrator.Session.CheckModulesConfigFilePath.FullName, "");   // Check modules configuration file
                        zipFile.AddFile(userInfoXml, "");  // XML with information about user
                        zipFile.AddDirectory(DataIntegrator.Session.WavesRecordedFolderPath.FullName, ContentDataFile.WavesApprovedFolderPath);
                        zipFile.AddDirectory(DataIntegrator.Session.WavesRejectedFolderPath.FullName, ContentDataFile.WavesRejectedFolderPath);

                        // Show exporting/saving progress
                        zipFile.SaveProgress += (o, args) =>
                        {
                            int percentage = 0;

                            if (args.EntriesTotal != 0)
                            {
                                percentage = args.EntriesSaved * 100 / args.EntriesTotal;
                                exportProgress.SetProgress(percentage);
                            }
                        };
                        zipFile.Save();
                    }
                });

                await exportProgress.CloseAsync();
            }
        }

        /// <summary>
        /// Asynchronously export data to the SFTP server
        /// </summary>
        /// <param name="sender">Export SFTP menu item</param>
        /// <param name="e">Routed event arguments</param>
        private async void OnExportSftpClick(object sender, RoutedEventArgs e)
        {
            bool uploadFailed = false;

            // If there is not Internet connection, show warning message, otherwise start uploading to SFTP
            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                await this.ShowMessageAsync("Spojení se serverem nelze navázat",
                    "Internetové připojení není k dispozici. Nahraná data nelze nahrát. Ujistěte se, že jste připojeni k Internetu a akci zopakujte.",
                    MessageDialogStyle.Affirmative,
                    new MetroDialogSettings() { AffirmativeButtonText = "OK" });
                return;
            }
            else
            {
                var exportProgress = await this.ShowProgressAsync("Export dat na server", "Spojování se serverem...");
                exportProgress.Minimum = 0;
                exportProgress.Maximum = 100;

                // Save user profile information to its directory to include this file into the ZIP
                string userInfoXml = Path.Combine(DataIntegrator.User.ProfileFolderPath.FullName, ContentDataFile.UserInfoFilename);
                DataIntegrator.User.SaveToXML(userInfoXml);

                // Start uploading
                await Task.Run(() =>
                {
                    FtpClient ftpClient = new FtpClient(DataIntegrator.Settings.SftpServer, new System.Net.NetworkCredential(DataIntegrator.Settings.SftpUserName, DataIntegrator.Settings.SftpPassword));
                    try
                    {
                        ftpClient.Connect();
                    }
                    catch (Exception)
                    {
                        exportProgress.SetTitle("Spojení se serverem se nezdařilo");
                        exportProgress.SetMessage("Spojení se serverem nelze navázat. Zkontrolujte prosím, zda je nastavení připojení správné.");
                        Thread.Sleep(2000);
                        return;
                    }

                    // Check whether root directory exists
                    if (!ftpClient.DirectoryExists(DataIntegrator.Settings.SftpRootDirectory))
                    {
                        exportProgress.SetMessage("Spojení se nezdařilo!");
                        Thread.Sleep(1000);
                        return;
                    }

                    // Check whether profile folder exists
                    try
                    {
                        string sftpProfilePath = Path.Combine(DataIntegrator.Settings.SftpRootDirectory, string.Join("_", DataIntegrator.User.Email, DataIntegrator.User.Guid));
                        string sftpSessionPath = Path.Combine(sftpProfilePath, DataIntegrator.Session.Name);
                        string sftpApprovedWavesPath = Path.Combine(sftpSessionPath, ContentDataFile.WavesApprovedFolderPath);
                        string sftpRejectedWavesPath = Path.Combine(sftpSessionPath, ContentDataFile.WavesRejectedFolderPath);
                        if (!ftpClient.DirectoryExists(sftpProfilePath))
                        {
                            exportProgress.SetMessage("Vytvářím uživatelský profil na serveru...");
                            ftpClient.CreateDirectory(sftpProfilePath);
                            ftpClient.CreateDirectory(sftpApprovedWavesPath);
                            ftpClient.CreateDirectory(sftpRejectedWavesPath);
                            Thread.Sleep(1000);
                        }

                        exportProgress.SetMessage("Připravuji data pro upload...");
                        exportProgress.Minimum = 0;
                        exportProgress.Maximum = DataIntegrator.Session.WavesRecordedFolderPath.GetFiles("*.wav").Concat(DataIntegrator.Session.WavesRejectedFolderPath.GetFiles("*.wav")).Count() + 1;
                        int prog = 0;

                        // Upload Approved waves
                        foreach (var waveFile in DataIntegrator.Session.WavesRecordedFolderPath.GetFiles("*.wav"))
                        {
                            string waveSftpPath = Path.Combine(sftpApprovedWavesPath, waveFile.Name);
                            ftpClient.UploadFile(waveFile.FullName, waveSftpPath, FtpExists.Append);
                            exportProgress.SetMessage(string.Format("Uploaduji soubor {0}", waveFile.Name));
                            exportProgress.SetProgress(++prog);
                        }

                        // Upload Rejected waves
                        foreach (var waveFile in DataIntegrator.Session.WavesRejectedFolderPath.GetFiles("*.wav"))
                        {
                            string waveSftpPath = Path.Combine(sftpRejectedWavesPath, waveFile.Name);
                            ftpClient.UploadFile(waveFile.FullName, waveSftpPath, FtpExists.Append);
                            exportProgress.SetMessage(string.Format("Uploaduji soubor {0}", waveFile.Name));
                            exportProgress.SetProgress(++prog);
                        }

                        // Upload XML with information about user
                        ftpClient.UploadFile(userInfoXml, Path.Combine(sftpSessionPath, ContentDataFile.UserInfoFilename), FtpExists.NoCheck);

                        // Upload phrases.xml file
                        exportProgress.SetMessage("Uploaduji soubor frází...");
                        ftpClient.UploadFile(DataIntegrator.Session.PhrasesFilePath.FullName, Path.Combine(sftpSessionPath, DataIntegrator.Session.PhrasesFilePath.Name), FtpExists.NoCheck);
                        exportProgress.SetProgress(++prog);
                        Thread.Sleep(500);
                    }
                    catch (FtpException)
                    {
                        exportProgress.SetTitle("Spojení se serverem nelze navázat");
                        exportProgress.SetMessage("Došlo k chybě při nahrávání a spojení se serverem bylo ukončeno. Ujistěte se, že jste připojeni k Internetu a nahrávání dat zopakujte.");
                        uploadFailed = true;
                    }
                    finally
                    {
                        if (!uploadFailed)
                            exportProgress.SetMessage("Data byla úspěšně nahrána!");
                    }
                });

                Thread.Sleep(1000);
                await exportProgress.CloseAsync();

                // Close app after upload if user wants to
                if (closeAppAfterUpload)
                    Environment.Exit(0);
            }
        }
#endregion Export MenuItem Group

        #region Help MenuItem Group
        /// <summary>
        /// Open application documentation PDF file with Adobe Reader if it's installed on PC
        /// </summary>
        /// <param name="sender">Menu Item</param>
        /// <param name="e">Routed event arguments</param>
        private void OnOpenDocumentationClick(object sender, RoutedEventArgs e)
        {
            if (Utilities.IsAdobeReaderInstalled)
            {
                var pdfPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources", "Documentation.pdf");
                Utilities.OpenWithAdobeReader(pdfPath);
            }
            else
            {
                var pdfPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources", "Documentation.pdf");
                System.Diagnostics.Process.Start(pdfPath);
            }
        }

        /// <summary>
        /// Open shortcuts window
        /// </summary>
        /// <param name="sender">Menu Item</param>
        /// <param name="e">Routed event arguments</param>
        private void OnShowShortcutsClick(object sender, RoutedEventArgs e)
        {
            ShortcutsWindow shortcutsWindow = new ShortcutsWindow();
            shortcutsWindow.Owner = this;
            shortcutsWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            shortcutsWindow.ShowDialog();
        }

        /// <summary>
        /// Open welcome flyout directly
        /// </summary>
        /// <param name="sender">Menu Item</param>
        /// <param name="e">Routed event arguments</param>
        private void OnOpenWelcomeFlyoutClick(object sender, RoutedEventArgs e) { flyoutMain.IsOpen = true; }
        #endregion Help MenuItem Group

        /// <summary>
        /// Key shortcuts event handler for Main Window - state machine
        /// </summary>
        /// <param name="sender">Main Window</param>
        /// <param name="e">Keyboard event arguments (used for key codes)</param>
        private void OnGlobalKeyDown(object sender, KeyEventArgs e)
        {
            // Left Arrow -> Go Back
            if (e.Key == Key.Left && (Keyboard.Modifiers == ModifierKeys.None))
            {
                OnBtnGoPreviousClick(btnGoPrevious, null);
                return;
            }
            // Right Arrow -> Go Next
            if ((e.Key == Key.Right || e.Key == Key.Enter) && (Keyboard.Modifiers == ModifierKeys.None))
            {
                OnBtnGoFollowingClick(btnGoFollowing, null);
                return;
            }
            // CTRL + Left Arrow -> Go First
            if (e.Key == Key.Left && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                OnBtnGoFirstClick(btnGoFirst, null);
                return;
            }
            // CTRL + Right Arrow -> Go Last
            if (e.Key == Key.Right && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                OnBtnGoLastClick(btnGoLast, null);
                return;
            }
            // SHIFT + Left Arrow -> Go Previous Skipped
            if (e.Key == Key.Left && (Keyboard.Modifiers == ModifierKeys.Shift))
            {
                OnBtnGoPreviousSkippedClick(btnGoPreviousSkipped, null);
                return;
            }
            // SHIFT + Right Arrow -> Go Following Skipped
            if (e.Key == Key.Right && (Keyboard.Modifiers == ModifierKeys.Shift))
            {
                OnBtnGoFollowingSkippedClick(btnGoFollowingSkipped, null);
                return;
            }

            // Space -> Start/Stop Recording
            if (e.Key == Key.N)
            {
                if (btnStartStopRecording.IsEnabled)
                    OnBtnStartStopRecordingClick(btnStartStopRecording, null);
                return;
            }
            // Backspace -> Skip
            if (e.Key == Key.Back)
            {
                if (btnSkipRecording.IsEnabled)
                    OnBtnSkipRecordingClick(btnSkipRecording, null);
                return;
            }
            // TAB -> Open shortcuts window
            if (e.Key == Key.Q)
            {
                OnShowShortcutsClick(null, null);
                return;
            }
            // P -> Play wave file
            if (e.Key == Key.P)
            {
                OnBtnPlayClick(null, null);
                return;
            }
            // S -> Stop wave file
            if (e.Key == Key.S)
            {
                OnBtnStopClick(null, null);
                return;
            }
            // Down arrow -> Move selection down in WaveFile listbox
            if (e.Key == Key.Down)
            {
                int lastIndex = lbxRecordedWaves.Items.Count - 1;
                if (lbxRecordedWaves.SelectedIndex < lastIndex)
                    lbxRecordedWaves.SelectedIndex++;

                return;
            }
            // Up arrow -> Move selection up in WaveFile listbox
            if (e.Key == Key.Up)
            {
                if (lbxRecordedWaves.SelectedIndex > 0)
                    lbxRecordedWaves.SelectedIndex--;

                return;
            }
            // Esc -> Close flyout
            if (e.Key == Key.Escape)
            {
                if (flyoutMain.IsOpen)
                    flyoutMain.IsOpen = false;
                return;
            }
            // F1 -> Open documentation
            if (e.Key == Key.F1)
            {
                OnOpenDocumentationClick(null, null);
                return;
            }
        }

        #region IMMNotificationClient interface members implementation
        public void OnDeviceStateChanged(string deviceId, DeviceState newState)
        {
            Dispatcher.BeginInvoke(new Action(delegate ()
            {
                MMDevice changedMMDevice = deviceEnumerator.GetDevice(deviceId);

                if (changedMMDevice.DataFlow == DataFlow.Capture)
                {
                    if (DataIntegrator.Settings.CaptureDevice?.State != DeviceState.Active)
                    {
                        if (DataIntegrator.Settings.CaptureDevice != null)
                        {
                            DataIntegrator.Settings.CaptureDevice = deviceEnumerator.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active)?.First();

                            if (DataIntegrator.Settings.CaptureDevice == null)
                            {
                                badgeSettings.Badge = "!";
                                return;
                            }

                            // Reload audio monitoring
                            audioMonitor = new DirectSoundAudioRecorder(DataIntegrator.Settings.CaptureDevice);
                            audioMonitor.PeakVolumeCalculated += OnAudioMonitorPeakVolumeCalculated;
                            audioMonitor.BeginMonitoring();
                            badgeSettings.Badge = "!";
                            UpdateInfoBox();
                            return;
                        }

                        // User plugged in the device stored in settings
                        if (DataIntegrator.SettingsFile.ReadElement("InputDeviceId") == deviceId)
                        {
                            DataIntegrator.Settings.CaptureDevice = deviceEnumerator.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active).Where(d => d.ID == deviceId).First();

                            // Reload audio monitoring
                            audioMonitor = new DirectSoundAudioRecorder(DataIntegrator.Settings.CaptureDevice);
                            audioMonitor.PeakVolumeCalculated += OnAudioMonitorPeakVolumeCalculated;
                            audioMonitor.BeginMonitoring();

                            // Reload audio recorder
                            audioRecorder.ChangeDevice(DataIntegrator.Settings.CaptureDevice);

                            UpdateInfoBox();
                            UpdateRecordingProgress();
                            return;
                        }

                        badgeSettings.Badge = "!";
                        return;
                    }
                    else
                    {
                        // User plugged in the device stored in settings
                        if (DataIntegrator.SettingsFile.ReadElement("InputDeviceId") == deviceId)
                        {
                            DataIntegrator.Settings.CaptureDevice = deviceEnumerator.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active).Where(d => d.ID == deviceId)?.First();

                            // Reload audio monitoring
                            audioMonitor = new DirectSoundAudioRecorder(DataIntegrator.Settings.CaptureDevice);
                            audioMonitor.PeakVolumeCalculated += OnAudioMonitorPeakVolumeCalculated;
                            audioMonitor.BeginMonitoring();

                            // Reload audio recorder
                            audioRecorder.ChangeDevice(DataIntegrator.Settings.CaptureDevice);

                            UpdateInfoBox();
                            UpdateRecordingProgress();
                            return;
                        }
                    }

                    /*if (DataIntegrator.Settings.CaptureDevice.State != DeviceState.Active)
                    {
                        //MessageBox.Show("Aktuálně nastavené zařízení pro záznam zvuku není dostupné. Jako nové zařízení pro záznam je nastaveno defaultní zařízení!");
                        DataIntegrator.Settings.CaptureDevice = deviceEnumerator.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active).First();

                        // Reload audio monitoring
                        audioMonitor = new DirectSoundAudioRecorder(DataIntegrator.Settings.CaptureDevice);
                        audioMonitor.BeginMonitoring();
                        badgeSettings.Badge = "!";
                        UpdateInfoBox();
                        return;
                    }

                    if (DataIntegrator.Settings.CaptureDevice.State == DeviceState.Active && deviceId != DataIntegrator.Settings.CaptureDevice.ID)
                    {
                        //MessageBox.Show("Bylo připojeno nové zařízení pro záznam zvuku. Chcete jej použít jako výchozí zařízení pro záznam?", "Nové zařízení", MessageBoxButton.YesNo);
                        badgeSettings.Badge = "!";
                        UpdateInfoBox();
                        return;
                    }*/
                }

                if (changedMMDevice.DataFlow == DataFlow.Render)
                {
                    if (DataIntegrator.Settings.RenderDevice?.State != DeviceState.Active)
                    {
                        //MessageBox.Show("Aktuálně nastavené zařízení pro přehrávání zvuku není dostupné. Jako nové zařízení pro přehrávání je nastaveno defaultní zařízení!");
                        DataIntegrator.Settings.RenderDevice = deviceEnumerator.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.Active)?.First();

                        // Reload Audio Player
                        audioPlayer.Dispose();
                        audioPlayer = new DirectSoundAudioPlayer(DataIntegrator.Settings.RenderDevice);
                        badgeSettings.Badge = "!";
                        UpdateInfoBox();
                        return;
                    }
                    else
                    {
                        // User plugged in the device stored in settings
                        var x = DataIntegrator.SettingsFile.ReadElement("OutputDeviceId");
                        if (DataIntegrator.SettingsFile.ReadElement("OutputDeviceId") == deviceId)
                        {
                            DataIntegrator.Settings.RenderDevice = deviceEnumerator.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.Active).Where(d => d.ID == deviceId)?.First();

                            // Reload audio monitoring
                            audioPlayer.Dispose();
                            audioPlayer = new DirectSoundAudioPlayer(DataIntegrator.Settings.RenderDevice);
                            return;
                        }
                    }

                    if (DataIntegrator.Settings.RenderDevice.State == DeviceState.Active && deviceId != DataIntegrator.Settings.RenderDevice.ID)
                    {
                        //MessageBox.Show("Bylo připojeno nové zařízení pro záznam zvuku. Chcete jej použít jako výchozí zařízení pro přehrávání?", "Nové zařízení", MessageBoxButton.YesNo);
                        badgeSettings.Badge = "!";
                        UpdateInfoBox();
                        return;
                    }
                }
            }));
        }

        public void OnDeviceAdded(string pwstrDeviceId)
        {
            //MessageBox.Show("Device added");
        }

        public void OnDeviceRemoved(string deviceId)
        {
            //MessageBox.Show("Device removed");
        }

        public void OnDefaultDeviceChanged(DataFlow flow, Role role, string defaultDeviceId)
        {
            //MessageBox.Show(string.Format("Device for {0} ({1}) status changed: {2}", flow, role, defaultDeviceId));
        }

        public void OnPropertyValueChanged(string pwstrDeviceId, PropertyKey key)
        {
            //MessageBox.Show("Not supported");
        }
        #endregion IMMNotificationClient interface members implementation

        private void OnPhraseFontSizeValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        {
            DataIntegrator.Settings.AppPhraseFontSize = (double)nudPhraseFontSize.Value;
            DataIntegrator.SettingsFile.Update();

            PhraseFontSize = DataIntegrator.Settings.AppPhraseFontSize;
        }

        /// <summary>
        /// Event when user change tag for any item listed in listbox control
        /// </summary>
        /// <param name="sender">Tagger control</param>
        /// <param name="args">Event arguments providing new tag value</param>
        private void OnTaggerTagChanged(object sender, TagChangedEventArgs args)
        {
            var item = (WaveFile)lbxRecordedWaves.SelectedItem;

            // Now update Phrases XML
            ((WaveFile)lbxRecordedWaves.SelectedItem).Tag = args.NewTag;
            DataIntegrator.PhrasesFile.UpdateTag(item);
        }

        /// <summary>
        /// Event when user want to close the application window. Disable closing when recording is active.
        /// </summary>
        private void OnWindowClosing(object sender, CancelEventArgs e)
        {
            if (audioRecorder.State == AudioRecorderState.Recording)
            {
                e.Cancel = true;
            }

            MessageBoxDialog msgDialog = new MessageBoxDialog("Nahrát data na server",
                "Chcete před ukončením aplikace nahrávat Vaše zaznamenaná data na server?", MessageBoxDialogButtons.YesNo, PackIconFontAwesomeKind.Database);
            msgDialog.Owner = this;
            var dlgResult = msgDialog.ShowDialog();
            if (dlgResult == true)
            {
                e.Cancel = true;
                closeAppAfterUpload = true;
                OnExportSftpClick(miExportSftp, null);
            }
        }

        /// <summary>
        /// Load secured ZIP file with new SFTP connection settings
        /// </summary>
        /// <param name="sender">MenuItem object</param>
        /// <param name="e">Routed event arguments</param>
        private void OnSftpConnectionSettingsLoad(object sender, RoutedEventArgs e)
        {
            SettingsWindow.LoadNewSftpConnectionSettings(DataIntegrator);
        }
    }
}
