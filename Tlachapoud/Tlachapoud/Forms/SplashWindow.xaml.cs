﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

using MahApps.Metro.Controls;
using System.Threading;
using System.ComponentModel;

using Tlachapoud.Core.IO;
using Tlachapoud.Core;
using System.Reflection;
using System.Xml.Linq;
using System.Configuration;
using Tlachapoud.Core.Data;

namespace Tlachapoud.Forms
{
    /// <summary>
    /// Interaction logic for SplashWindow.xaml
    /// </summary>
    public partial class SplashWindow : Window
    {
        private BackgroundWorker bgWorker;
        private DataIntegrator dataIntegrator;

        public SplashWindow()
        {
            DataContext = this;
            // Copy user settings from previous application version if necessary
            if (Tlachapoud.Properties.Settings.Default.UpdateSettings)
            {
                Tlachapoud.Properties.Settings.Default.Upgrade();
                Tlachapoud.Properties.Settings.Default.UpdateSettings = false;
                Tlachapoud.Properties.Settings.Default.Save();
            }
            InitializeComponent();
        }

        private void wSplash_ContentRendered(object sender, EventArgs e)
        {
            bgWorker = new BackgroundWorker();
            bgWorker.WorkerReportsProgress = true;
            bgWorker.DoWork += BgWorker_DoWork;
            bgWorker.ProgressChanged += BgWorker_ProgressChanged;
            bgWorker.RunWorkerAsync();
            bgWorker.RunWorkerCompleted += BgWorker_RunWorkerCompleted;
        }

        private void BgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!string.IsNullOrEmpty(Properties.Settings.Default.RemeberedUser))
            {
                UsersFile usersFile = new UsersFile(ContentDataFile.UsersFilePath);
                var user = usersFile.Select(Properties.Settings.Default.RemeberedUser, UserProfileData.Email);

                if (user != null)
                {
                    // User is re

                    if (!user.ProfileFolderPath.Exists)
                        user.ProfileFolderPath.Create();
#if NOSESSIONS
                    // Check whether there are sessions, if no, create default 'PhrasesRecording' session
                    var existingSessions = Session.GetSessions(user.ProfileFolderPath);
                    Session newSession;
                    if (!existingSessions.Any())                   
                        // Create a new DEFAULT session
                        newSession = Session.Create(user);              
                    else
                        // Load the first session
                        newSession = existingSessions.First();

                    dataIntegrator = new DataIntegrator(user, newSession);
                    
                    MainWindow mainWindow = new MainWindow(user, newSession, dataIntegrator);
                    mainWindow.Show();
                    Close();
#else
                    SessionManagerWindow sessionManagerWindow = new SessionManagerWindow(user);
                    sessionManagerWindow.Show();
                    Close();
#endif
                }
                else
                {
                    LoginWindow loginWindow = new LoginWindow();
                    loginWindow.Show();
                    Close();
                }
            }
            else
            {
                LoginWindow loginWindow = new LoginWindow();
                loginWindow.Show();
                Close();
            }
        }

        private void BgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pbLoading.Value = e.ProgressPercentage;
        }

        private void BgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            CheckApplicationData();
        }

        private void CheckApplicationData()
        {
            bgWorker.ReportProgress(0);
            // ..\AppData folder
            if (!Directory.Exists(ContentDataFile.AppDataFolderPath))
                Directory.CreateDirectory(ContentDataFile.AppDataFolderPath);

            
            Thread.Sleep(1040);
            bgWorker.ReportProgress(20);

            // ..\UserData folder
            if (!Directory.Exists(ContentDataFile.UserDataFolderPath))
                Directory.CreateDirectory(ContentDataFile.UserDataFolderPath);

            Thread.Sleep(1080);
            bgWorker.ReportProgress(40);

            // ..\Data\users.xml
            UsersFile usersFile = new UsersFile(ContentDataFile.UsersFilePath);

            Thread.Sleep(140);
            bgWorker.ReportProgress(60);

            // ..\Data\Phrases.xml
            // TODO: Check whether this file exists, if not, download it if there is an internet connection or use embedded one
            FileInfo fileInfo = new FileInfo(ContentDataFile.PhrasesFilePath);
            if (!fileInfo.Exists)
            {
                var assembly = Assembly.GetExecutingAssembly();
                using (var xmlStream = assembly.GetManifestResourceStream("Tlachapoud.Resources.laryngo.v9.wantfname.xml"))
                {
                    XDocument xmlPhrases = XDocument.Load(xmlStream);
                    xmlPhrases.Save(fileInfo.FullName);
                }
            }

            Thread.Sleep(185);
            bgWorker.ReportProgress(80);

            // ..\Data\CheckModules.cfg
            // TODO: Check whether this file exists, if not, download it if there is an internet connection or use embedded one
            fileInfo = new FileInfo(ContentDataFile.CheckModulesConfigFilePath);
            if (!fileInfo.Exists)
            {
                var assembly = Assembly.GetExecutingAssembly();
                using (var xmlStream = assembly.GetManifestResourceStream("Tlachapoud.Resources.Audio.CheckModules.cfg"))
                {
                    XDocument xmlPhrases = XDocument.Load(xmlStream);
                    xmlPhrases.Save(fileInfo.FullName);
                }
            }

            Thread.Sleep(40);
            bgWorker.ReportProgress(100);
        }

        public string AssemblyVersion
        {
            get
            {
                var assemblyVersion = Assembly.GetExecutingAssembly().GetName().Version;
                return string.Format("Verze {0}.{1}", assemblyVersion.Major, assemblyVersion.Minor);
            }
        }
    }
}
