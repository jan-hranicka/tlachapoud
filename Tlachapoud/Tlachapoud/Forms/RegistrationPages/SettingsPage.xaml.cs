﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

// Import Tlachapoud Core library
using Tlachapoud.Core.Audio;
using Tlachapoud.Core.Audio.DirectSound;
using Tlachapoud.Core.Audio.Wasapi;
using Tlachapoud.Core.Net;

// Import FluentFTP library
using FluentFTP;

// Import MahApps Metro library
using MahApps.Metro.Controls.Dialogs;

// Import NAudio Core Audio API library
using NAudio.CoreAudioApi;
using Tlachapoud.Controls.Dialogs;
using System.Windows.Media;
using NAudio.CoreAudioApi.Interfaces;
using System;

namespace Tlachapoud.Forms.RegistrationPages
{
    /// <summary>
    /// Interaction logic for SettingsPage.xaml
    /// </summary>
    public partial class SettingsPage : Page, IMMNotificationClient
    {
        private RegistrationWindow parentWindow;

        #region WASAPI MMDevice objects
        private IEnumerable<MMDevice> captureDevices;
        private IEnumerable<MMDevice> renderDevices;
        #endregion WASAPI MMDevice objects

        private DirectSoundAudioRecorder audioRecorder;
        private DirectSoundAudioPlayer audioPlayer;
        private MMDeviceEnumerator deviceEnumerator;
        private Stream testAudioStream;

        /// <summary>
        /// Create a new Settings Page
        /// </summary>
        /// <param name="parentWindow">Parent window as an owner of this object used for passing on data</param>
        public SettingsPage(RegistrationWindow parentWindow)
        {
            InitializeComponent();
            this.parentWindow = parentWindow;
            tbxSftpRootDirectory.Text = SftpLogin.MainDir;
            tbxSftpServer.Text = SftpLogin.HostAddress;
            tbxSftpUserName.Text = SftpLogin.UserName;
            pwbSftpPassword.Password = SftpLogin.Password;


#if !VOICECHECKER
            gbxVoiceChecker.Visibility = Visibility.Hidden;
#endif

            // Register MMDevice enumerator and Notification Callback
            deviceEnumerator = new MMDeviceEnumerator();
            deviceEnumerator.RegisterEndpointNotificationCallback(this);

            // Initialize Capture and Render devices
            captureDevices = DeviceProvider.GetCaptureDevices();
            renderDevices = DeviceProvider.GetRenderDevices();

            cobCaptureDevice.ItemsSource = captureDevices;
            cobCaptureDevice.DisplayMemberPath = "FriendlyName";
            // If there is any capture device, select the active one or default (0)
            if (captureDevices.Any())
            {
                var activeDevice = captureDevices.Where(d => d.State == DeviceState.Active);
                if (activeDevice.Any())
                    cobCaptureDevice.SelectedItem = activeDevice.First();
                else
                    cobCaptureDevice.SelectedIndex = 0;
            }

            cobRenderDevice.ItemsSource = renderDevices;
            cobRenderDevice.DisplayMemberPath = "FriendlyName";
            // If there is any capture device, select the active one or default (0)
            if (renderDevices.Any())
            {
                var activeDevice = renderDevices.Where(d => d.State == DeviceState.Active);
                if (activeDevice.Any())
                    cobRenderDevice.SelectedItem = activeDevice.First();
                else
                    cobRenderDevice.SelectedIndex = 0;
            }


            // Initialize Audio Recorder (for monitoring) and Player
            audioRecorder = new DirectSoundAudioRecorder((MMDevice)cobCaptureDevice.SelectedItem);
            audioRecorder.PeakVolumeCalculated += OnPeakVolumeCalculated;
            audioPlayer = new DirectSoundAudioPlayer((MMDevice)cobRenderDevice.SelectedItem);

            // Initialize supported wave formats
            //var captureFormat = ((MMDevice)cobCaptureDevice.SelectedItem).AudioClient.MixFormat;  
            var captureFormat = SupportedWaveFormats.Mono48kHz16bit;
            cobCaptureWaveFormat.ItemsSource = new List<string>() { string.Format("Kanál {2}, {1} bitů, {0} Hz",
                                                                                    captureFormat.SampleRate,
                                                                                    captureFormat.BitsPerSample,
                                                                                    captureFormat.Channels) };
            cobCaptureWaveFormat.SelectedIndex = 0;
            var renderFormat = ((MMDevice)cobRenderDevice.SelectedItem).AudioClient.MixFormat;
            lblRenderWaveFormat.Content = string.Format("Kanál {2}, {1} bitů, {0} Hz", renderFormat.SampleRate, renderFormat.BitsPerSample, renderFormat.Channels);

            audioRecorder.BeginMonitoring();
        }

        /// <summary>
        /// Event handler showing input level on each maximum calculated peak period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPeakVolumeCalculated(object sender, MasterPeakVolumeEventArgs e)
        {
            pbInputLevel.Value = e.PeakVolume * 100;
        }

        /// <summary>
        /// Event when user change input device via combobox
        /// </summary>
        /// <param name="sender">Input device combobox</param>
        /// <param name="e">Selection changed event arguments</param>
        private void OnCaptureDeviceSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (audioRecorder != null)
            {
                audioRecorder.Dispose();
                audioRecorder = new DirectSoundAudioRecorder((MMDevice)cobCaptureDevice.SelectedItem);
                audioRecorder.PeakVolumeCalculated += OnPeakVolumeCalculated;
                audioRecorder.BeginMonitoring();
            }
        }

        /// <summary>
        /// Event when user click on Play Test button, play a test wave embedded in application
        /// </summary>
        /// <param name="sender">Play Test button</param>
        /// <param name="e">Routed event arguments</param>
        private void OnBtnPlayTestClick(object sender, RoutedEventArgs e)
        {
            if (testAudioStream != null)
                testAudioStream.Dispose();

            var assembly = Assembly.GetExecutingAssembly();
            testAudioStream = assembly.GetManifestResourceStream("Tlachapoud.Resources.Audio.pcm44_16f_mono.wav");
            if (audioPlayer.State == AudioPlayerState.Playing)
                audioPlayer.Stop();

            audioPlayer.Dispose();
            audioPlayer.Load(testAudioStream);
            audioPlayer.Play();
        }

        /// <summary>
        /// Event when user change output device via combobox
        /// </summary>
        /// <param name="sender">Output device combobox</param>
        /// <param name="e">Selection changed event arguments</param>
        private void OnRenderDeviceSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cobRenderDevice.SelectedItem == null)
                return;

            if (audioPlayer != null)
                audioPlayer.Dispose();

            audioPlayer = new DirectSoundAudioPlayer((MMDevice)cobRenderDevice.SelectedItem);
            var renderFormat = ((MMDevice)cobRenderDevice.SelectedItem).AudioClient.MixFormat;
            lblRenderWaveFormat.Content = string.Format("Kanál {2}, {1} bitů, {0} Hz", renderFormat.SampleRate, renderFormat.BitsPerSample, renderFormat.Channels);
        }

        /// <summary>
        /// Event handling user's click on the Back button, move to the previous page
        /// </summary>
        /// <param name="sender">Back button</param>
        /// <param name="e">Routed event arguments</param>
        private void OnBtnBackClick(object sender, RoutedEventArgs e)
        {
            if (audioPlayer.State != AudioPlayerState.Stopped)
                audioPlayer.Stop();

            parentWindow.RegistrationBar.Step = 1;
            NavigationService.GoBack();
        }

        /// <summary>
        /// Event handling user's click on the Next button, move to the following page 
        /// </summary>
        /// <param name="sender">Next button</param>
        /// <param name="e">Routed event arguments</param>
        private void OnBtnNextClick(object sender, RoutedEventArgs e)
        {
            if (audioPlayer.State != AudioPlayerState.Stopped)
                audioPlayer.Stop();

            parentWindow.UserSettings.CaptureDevice = (MMDevice)cobCaptureDevice.SelectedItem;
            parentWindow.UserSettings.RenderDevice = (MMDevice)cobRenderDevice.SelectedItem;
            parentWindow.UserSettings.VoiceCheckerActive = tswVoiceCheckerActive.IsChecked == true;
            parentWindow.UserSettings.VoiceCheckerInterval = (int)nudVoiceCheckerInterval.Value;
            parentWindow.UserSettings.VoiceCheckerHistory = (int)nudVoiceCheckerHistory.Value;
            parentWindow.UserSettings.SftpServer = tbxSftpServer.Text;
            parentWindow.UserSettings.SftpUserName = tbxSftpUserName.Text;
            parentWindow.UserSettings.SftpRootDirectory = tbxSftpRootDirectory.Text;

            if (pwbSftpPassword.SecurePassword.Length > 0)
                parentWindow.UserSettings.SftpPassword = pwbSftpPassword.Password;

            parentWindow.RegistrationBar.Step = 4;
            NavigationService.Navigate(new SummaryPage(parentWindow));
        }

        private async void OnBtnModifySftpCredentialsClick(object sender, RoutedEventArgs e)
        {
            var pom = await parentWindow.ShowLoginAsync("Přihlášení administrátora", "Změna SFTP požaduje administrátorská oprávnění");
            // TODO: Check whether admin login is correct and then unlock SFTP settings to modify
            // TODO: Remember for registration whether admin is logged in or not!! 
        }

        /// <summary>
        /// Dispose audio player and test audio stream when closing this page
        /// </summary>
        private void OnPageUnloaded(object sender, RoutedEventArgs e)
        {
            if (audioPlayer.State != AudioPlayerState.Stopped)
                audioPlayer.Stop();
            if (testAudioStream != null)
                testAudioStream.Dispose();
        }

        /// <summary>
        /// Event handler when user click on SFTP Test button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnBtnTestSftpConnectionClick(object sender, RoutedEventArgs e)
        {
            pgrTest.Visibility = Visibility.Visible;
            FtpClient ftpClient = new FtpClient(SftpLogin.HostAddress, new System.Net.NetworkCredential(SftpLogin.UserName, SftpLogin.Password));

            try
            {
                await ftpClient.ConnectAsync();
                pgrTest.Visibility = Visibility.Hidden;
                MessageBoxDialog msgBoxDialog = new MessageBoxDialog("Test spojení se serverem",
                    "Spojení se serverem bylo úspěšně navázáno.",
                    MessageBoxDialogButtons.OK, MahApps.Metro.IconPacks.PackIconFontAwesomeKind.CheckCircleOutline, Colors.ForestGreen);
                msgBoxDialog.Owner = parentWindow;
                msgBoxDialog.ShowDialog();
            }
            catch (System.Net.Sockets.SocketException)
            {
                pgrTest.Visibility = Visibility.Hidden;
                MessageBoxDialog msgBoxDialog = new MessageBoxDialog("Test spojení se serverem",
                    "Spojení se serverem se nezdařilo! Na adresu hostitele není možné se připojit!",
                    MessageBoxDialogButtons.OK, MahApps.Metro.IconPacks.PackIconFontAwesomeKind.TimesCircleOutline, Colors.Crimson);
                msgBoxDialog.Owner = parentWindow;
                msgBoxDialog.ShowDialog();
            }
            catch
            {
                pgrTest.Visibility = Visibility.Hidden;
                MessageBoxDialog msgBoxDialog = new MessageBoxDialog("Test spojení se serverem",
                    "Spojení se serverem se nezdařilo! Zkontrolujte, zda jste připojeni k Internetu nebo zda jsou přihlašovací údaje serveru správné",
                    MessageBoxDialogButtons.OK, MahApps.Metro.IconPacks.PackIconFontAwesomeKind.TimesCircleOutline, Colors.Crimson);
                msgBoxDialog.Owner = parentWindow;
                msgBoxDialog.ShowDialog();
            }
        }

        #region IMMNotification client interface members
        public void OnDeviceStateChanged(string deviceId, DeviceState newState)
        {
            Dispatcher.BeginInvoke(new Action(delegate ()
            {
                audioRecorder.StopMonitoring();
                audioPlayer.Stop();

                // Initialize Capture and Render devices
                captureDevices = DeviceProvider.GetCaptureDevices();
                renderDevices = DeviceProvider.GetRenderDevices();

                cobCaptureDevice.ItemsSource = null;
                cobCaptureDevice.ItemsSource = captureDevices;
                cobCaptureDevice.DisplayMemberPath = "FriendlyName";
                // If there is any capture device, select the active one or default (0)
                if (captureDevices.Any())
                {
                    var activeDevice = captureDevices.Where(d => d.State == DeviceState.Active);
                    if (activeDevice.Any())
                        cobCaptureDevice.SelectedItem = activeDevice.First();
                    else
                        cobCaptureDevice.SelectedIndex = 0;
                }

                cobRenderDevice.ItemsSource = null;
                cobRenderDevice.ItemsSource = renderDevices;
                cobRenderDevice.DisplayMemberPath = "FriendlyName";
                // If there is any capture device, select the active one or default (0)
                if (renderDevices.Any())
                {
                    var activeDevice = renderDevices.Where(d => d.State == DeviceState.Active);
                    if (activeDevice.Any())
                        cobRenderDevice.SelectedItem = activeDevice.First();
                    else
                        cobRenderDevice.SelectedIndex = 0;
                }
            }));
        }

        public void OnDeviceAdded(string pwstrDeviceId)
        {
            return;
        }

        public void OnDeviceRemoved(string deviceId)
        {
            return;
        }

        public void OnDefaultDeviceChanged(DataFlow flow, Role role, string defaultDeviceId)
        {
            return;
        }

        public void OnPropertyValueChanged(string pwstrDeviceId, PropertyKey key)
        {
            return;
        }
        #endregion IMMNotification client interface members
    }
}
