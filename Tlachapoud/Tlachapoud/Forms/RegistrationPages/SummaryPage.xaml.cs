﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Tlachapoud.Core;
using Tlachapoud.Core.IO;
using Tlachapoud.Core.Net;

namespace Tlachapoud.Forms.RegistrationPages
{
    /// <summary>
    /// Interaction logic for SummaryPage.xaml
    /// </summary>
    public partial class SummaryPage : Page
    {
        RegistrationWindow parentWindow;

        public Visibility VoiceCheckerEnabled { get; set; }

        public SummaryPage(RegistrationWindow parentWindow)
        {
#if VOICECHECKER
            VoiceCheckerEnabled = Visibility.Visible;
#else
            VoiceCheckerEnabled = Visibility.Hidden;
#endif

            DataContext = this;
            InitializeComponent();

            this.parentWindow = parentWindow;

            // Fill labels from user registration
            lblFirstName.Content = parentWindow.UserProfile.FirstName;
            lblLastName.Content = parentWindow.UserProfile.LastName;
            lblDegree.Content = string.IsNullOrEmpty(parentWindow.UserProfile.Degree) ? "-" : parentWindow.UserProfile.Degree;
            lblPhoneNumber.Content = string.IsNullOrEmpty(parentWindow.UserProfile.PhoneNumber) ? "-" : parentWindow.UserProfile.PhoneNumber;
            lblLoginEmail.Content = parentWindow.UserProfile.Email;
            lblLoginNickname.Content = string.Format("{0} (alternativa)", parentWindow.UserProfile.Nickname);

            // Sftp from settings
            lblSftpServer.Content = parentWindow.UserSettings.SftpServer;
            lblSftpLogin.Content = parentWindow.UserSettings.SftpUserName;
            lblSftpRoot.Content = parentWindow.UserSettings.SftpRootDirectory;

            // Voice checker
            lblVoiceCheckerStatus.Content = parentWindow.UserSettings.VoiceCheckerActive == true ? "Aktivní" : "Neaktivní";
            lblVoiceCheckerInterval.Content = string.Format("Každých {0} vět", parentWindow.UserSettings.VoiceCheckerInterval);
            lblVoiceCheckerHistory.Content = string.Format("Maximálně {0} dní nazpět", parentWindow.UserSettings.VoiceCheckerHistory);
        }

        private void OnBtnConfirmRegistrationClick(object sender, RoutedEventArgs e)
        {
            // Add %USER% to Users.xml
            UsersFile usersFile = new UsersFile(ContentDataFile.UsersFilePath);
            if (!usersFile.Exists(parentWindow.UserProfile, UserProfileData.Email))
                usersFile.WriteData(parentWindow.UserProfile);
            usersFile.Save();

            // Create %USER_NICKNAME% folder in Data directory
            if (!parentWindow.UserProfile.ProfileFolderPath.Exists)
                parentWindow.UserProfile.ProfileFolderPath.Create();

            // Save user settings to a profile folder
            SettingsFile settingsFile = new SettingsFile();
            settingsFile.Load(Path.Combine(parentWindow.UserProfile.ProfileFolderPath.FullName, "settings.xml"));
            settingsFile.Data = parentWindow.UserSettings;
            settingsFile.Update();

            parentWindow.IsComplete = true;
            parentWindow.Close();
        }

        private void OnBtnBackClick(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}
