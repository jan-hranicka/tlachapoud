﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Navigation;
using System.Reflection;
using System.IO;

namespace Tlachapoud.Forms.RegistrationPages
{
    /// <summary>
    /// Page with license agreement for user
    /// </summary>
    public partial class LicensePage : Page
    {
        /// <summary>
        /// Parent window
        /// </summary>
        RegistrationWindow parentWindow;

        /// <summary>
        /// License Page constructor
        /// </summary>
        /// <param name="parentWindow">Owning RegistrationWindow</param>
        public LicensePage(RegistrationWindow parentWindow)
        {
            InitializeComponent();
            this.parentWindow = parentWindow;

            var textRange = new TextRange(rtxLicense.Document.ContentStart, rtxLicense.Document.ContentEnd);

            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "Tlachapoud.Resources.RichTexts.license.rtf";
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                textRange.Load(stream, DataFormats.Rtf);
            }
        }

        /// <summary>
        /// If Agree with license is checked, user can go to the next page
        /// </summary>
        private void OnCobAgreeChecked(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;

            btnNext.IsEnabled = checkBox.IsChecked == true;
        }

        /// <summary>
        /// Event when user click on Cancel button
        /// </summary>
        private void OnBtnCancelClick(object sender, RoutedEventArgs e)
        {
            parentWindow.Close();
        }

        /// <summary>
        /// Event when user click on Next button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBtnNextClick(object sender, RoutedEventArgs e)
        {
            if (!NavigationService.CanGoForward)
                NavigationService.Navigate(new PersonalDataPage(parentWindow));
            else
                NavigationService.GoForward();
            parentWindow.RegistrationBar.Step = 2;
        }
    }
}
