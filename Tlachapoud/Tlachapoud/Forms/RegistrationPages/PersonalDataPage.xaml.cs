﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Text.RegularExpressions;

// Import Tlachapoud Core library
using Tlachapoud.Core.Data;
using Tlachapoud.Core.IO;
using Tlachapoud.Core;
using System.Collections.Generic;

namespace Tlachapoud.Forms.RegistrationPages
{
    /// <summary>
    /// Interaction logic for PersonalDataPage.xaml
    /// </summary>
    public partial class PersonalDataPage : Page
    {
        #region Static Color Brushes
        static readonly SolidColorBrush ColorWarning = new SolidColorBrush(Colors.LightPink);   // Brush for Warnings
        static readonly SolidColorBrush ColorDefault = new SolidColorBrush(Colors.White);       // Default brush
        #endregion Static Color Brushes

        /// <summary>
        /// Parent window container
        /// </summary>
        RegistrationWindow parentWindow;

        /// <summary>
        /// Create a new page to obtain user's personal data
        /// </summary>
        /// <param name="parentWindow">Parent window with cross-page navigation</param>
        public PersonalDataPage(RegistrationWindow parentWindow)
        {
            InitializeComponent();

            List<int> years = new List<int>();
            // Initialize dates to the picker
            for (int year = DateTime.Now.Year; year >= 1900; year--)
            {
                years.Add(year);
            }
            cbxYearOfBirth.ItemsSource = years;

            this.parentWindow = parentWindow;
            tbxFirstName.Focus();
        }

        /// <summary>
        /// Event handler when user click on Back button
        /// Navigate one page back in navigation service and change registration step
        /// </summary>
        /// <param name="sender">Back button</param>
        /// <param name="e">Routed event arguments</param>
        private void OnBtnBackClick(object sender, RoutedEventArgs e)
        {
            parentWindow.RegistrationBar.Step = 1;
            NavigationService.GoBack();
        }

        /// <summary>
        /// Event handler when user click on Next button
        /// Check whether all required boxes are filled in and contain correct values
        /// Navigate one page forward in navigation service and change registration step
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBtnNextClick(object sender, RoutedEventArgs e)
        {
            // Reset background colors in textboxes & passwordboxes
            foreach (var item in new Control[] { tbxFirstName, tbxLastName, tbxEmail, tbxEmailAgain, tbxNickname, pbxPassword, pbxPasswordAgain, tbxAccentKind })
            {
                item.Background = ColorDefault;

                // TextBox condition
                var tb = item as TextBox;
                if (tb != null)
                {
                    if (string.IsNullOrEmpty(tb.Text) && tb.IsEnabled)
                        tb.Background = ColorWarning;
                    continue;
                }

                // PasswordBox condition
                var pwb = item as PasswordBox;
                if (pwb != null)
                {
                    if (string.IsNullOrEmpty(pwb.Password) && pwb.IsEnabled)
                        pwb.Background = ColorWarning;
                }
            }
            // Reset background colors for PhoneNumber and Degree, which are not required
            tbxDegree.Background = ColorDefault;
            tbxPhoneNumber.Background = ColorDefault;

            // Reset background colors in combobox
            if (cbxYearOfBirth.SelectedIndex < 0)
                cbxYearOfBirth.Background = ColorWarning;
            else
                cbxYearOfBirth.Background = ColorDefault;

            // Create a new user profile
            UserProfile userProfile = new UserProfile()
            {
                FirstName = tbxFirstName.Text.Trim(),
                LastName = tbxLastName.Text.Trim(),
                YearOfBirth = (int?)(cbxYearOfBirth.SelectedItem),
                Gender = rbnMale.IsChecked == true ? Gender.Male : Gender.Female,
                Email = tbxEmailAgain.Text,
                Nickname = tbxNickname.Text.Trim(),
                PhoneNumber = tbxPhoneNumber.Text.Trim(),
                Degree = tbxDegree.Text.Trim(),
                HasRegionalAccent = rbnHasAccent.IsChecked == true,
                RegionalAccentKind = tbxAccentKind.Text.Trim(),
                IsUsedToSpeakCoherently = rbnUsedToSpeakCoherently.IsChecked == true,
                EnvironmentUserName = Environment.UserName
            };
            userProfile.SetPassword(pbxPassword.SecurePassword);

            UsersFile usersFile = new UsersFile(ContentDataFile.UsersFilePath);

            // Not all required boxes are filled in
            if (!userProfile.IsComplete())
            {
                warningDlgBox.Message = "Některé z polí není vyplněno. Prosím, vyplňte správně všechna povinná pole a poté pokračujte dále.";
                warningDlgBox.Visibility = Visibility.Visible;
                return;
            }

            // Check TextBox for FirstName
            Regex regex = new Regex(@"[\d]", RegexOptions.CultureInvariant);
            if (userProfile.FirstName.Length == 0 || regex.IsMatch(userProfile.FirstName))
            {
                SetWarning(tbxFirstName, "Pole pro jméno nesmí být prázdné ani nesmí obsahovat číslice!");
                return;
            }

            // Check TextBox for LastName
            regex = new Regex(@"[\d]", RegexOptions.CultureInvariant);
            if (userProfile.LastName.Length == 0 || regex.IsMatch(userProfile.LastName))
            {
                SetWarning(tbxLastName, "Pole pro příjmení nesmí být prázdné ani nesmí obsahovat číslice!");
                return;
            }

            // Check PasswordBox
            regex = new Regex(@"[\s]", RegexOptions.CultureInvariant);
            if (chbPasswordProtect.IsChecked == true && (regex.IsMatch(userProfile.Password) || pbxPassword.Password.Length < 6))
            {
                SetWarning(pbxPassword, "Heslo musí mít minimálně 6 znaků a nesmí obsahovat mezery!");
                return;
            }

            // Check PasswordBox (again)
            if (chbPasswordProtect.IsChecked == true && (pbxPasswordAgain.Password.Length > 0 && pbxPasswordAgain.Password != pbxPassword.Password))
            {
                SetWarning(pbxPasswordAgain, "Zadaná hesla nejsou stejná!");
                pbxPassword.Background = ColorWarning;
                return;
            }

            // Check PhoneNumer
            regex = new Regex(@"^[\d+][\d\s]+$");
            if (!string.IsNullOrEmpty(userProfile.PhoneNumber) && !regex.IsMatch(userProfile.PhoneNumber))
            {
                SetWarning(tbxPhoneNumber, "Zadané telefonní číslo má nesprávný formát. Prosím, zadejte číslo ve tvaru +420 123 456 789 nebo 123 456 789.");
                return;
            }

            // User with this email address already exist
            if (usersFile.Exists(userProfile, UserProfileData.Email))
            {
                SetWarning(tbxEmail, "Zadanou emailovou adresu již používá jiný uživatel!");
                return;
            }

            // Check Email
            regex = new Regex(@"[\w\d._-]+@[\d\w._-]+\.+[\d\w]", RegexOptions.CultureInvariant);
            if (!regex.IsMatch(tbxEmail.Text))
            {
                SetWarning(tbxEmail, "Zadaný email nemá správný formát!");
                return;
            }

            // Check Email (again)
            if (tbxEmail.Text != tbxEmailAgain.Text)
            {
                SetWarning(tbxEmailAgain, "Zadané emaily nejsou shodné!");
                tbxEmail.Background = ColorWarning;
                return;
            }

            // User with this email address already exist
            if (usersFile.Exists(userProfile, UserProfileData.Nickname))
            {
                SetWarning(tbxNickname, "Zadanou přezdívku již používá jiný uživatel!");
                return;
            }

            // Nickname must have at least 3 characters
            regex = new Regex(@"[\s]", RegexOptions.CultureInvariant);
            Regex digRegex = new Regex(@"^[\d]", RegexOptions.CultureInvariant);
            if (userProfile.Nickname.Length < 3 || regex.IsMatch(userProfile.Nickname) || digRegex.IsMatch(userProfile.Nickname))
            {
                SetWarning(tbxNickname, "Přezdívka musí mít nejméně 3 znaky, nesmí obsahovat bílé znaky ani začínat číslicí!");
                return;
            }

            // Is date selected
            if (cbxYearOfBirth.SelectedIndex < 0)
            {
                SetWarning(cbxYearOfBirth, "Vyberte prosím rok narození.");
                return;
            }
            /*if (dtpBirthdate.SelectedDate.Value > DateTime.Today || dtpBirthdate.SelectedDate.Value < new DateTime(1900, 1, 1))
            {
                SetWarning(dtpBirthdate, "Vybraný datum je neplatný nebo starší 1.1.1900!");
                return;
            }*/

            // Check whether RegionalAccentKind is not if HasRegionalAccent is True
            if (userProfile.HasRegionalAccent && string.IsNullOrEmpty(userProfile.RegionalAccentKind))
            {
                SetWarning(tbxAccentKind, "Prosím, vyplňte Váš regionální akcent nebo označte, že žádný akcent nemáte.");
                return;
            }

            if (chbPasswordProtect.IsChecked == false)
                userProfile.SetPassword("".ToSecureString(), false);

            warningDlgBox.Visibility = Visibility.Hidden;
            parentWindow.UserProfile = userProfile;
            parentWindow.RegistrationBar.Step = 3;
            if (NavigationService.CanGoForward)
                NavigationService.GoForward();
            else
                NavigationService.Navigate(new SettingsPage(parentWindow));
        }

        /// <summary>
        /// Class method for setting warning visible with a specific message
        /// </summary>
        /// <param name="control">Control to change the Background property</param>
        /// <param name="message">Message for a user</param>
        private void SetWarning(Control control, string message)
        {
            warningDlgBox.Message = message;
            warningDlgBox.Visibility = Visibility.Visible;
            control.Background = ColorWarning;
        }

        /// <summary>
        /// Event when input text of PhoneNumber TextBox has changed
        /// </summary>
        private void OnTbxPhoneNumberPreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = !Utilities.IsNumericInput(e.Text);
        }
    }
}
