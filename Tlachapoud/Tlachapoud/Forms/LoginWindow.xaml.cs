﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;

// Import MahApps Metro library
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

// Import Tlachapoud Core library
using Tlachapoud.Core.IO;
using Tlachapoud.Core.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Tlachapoud.Controls.Dialogs;
using System.Windows.Media;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using FluentFTP;
using Tlachapoud.Core.Net;

namespace Tlachapoud.Forms
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class LoginWindow : MetroWindow
    {
#if NOSESSION
        private SessionManagerWindow sessionManagerWindow;
#endif
        private bool rememberUser = false;
        private IEnumerable<UserProfile> registeredUsers;
        private bool uploadFailed = false;
        private List<string> listOfFailed = new List<string>();

        /// <summary>
        /// Constructor initializing window components
        /// </summary>
        public LoginWindow()
        {
            InitializeComponent();

#if !DEBUG
            chbRemeberUser.Visibility = Visibility.Hidden;
#endif

            var users = new UsersFile(ContentDataFile.UsersFilePath);
            registeredUsers = users?.Data ?? new List<UserProfile>();
            cbxLogin.ItemsSource = registeredUsers.Where(u => u.EnvironmentUserName == Environment.UserName).OrderByDescending(u => u.RegistrationDate);

            // Select the first user if there is exactly one
            if (cbxLogin.Items.Count == 1)
                cbxLogin.SelectedIndex = 0;

            if (!registeredUsers.Any())
            {
                btnUploadAll.IsEnabled = false;
                btnLogin.IsEnabled = false;
                cbxLogin.IsEnabled = false;
                chbRemeberUser.IsEnabled = false;
                tbkNoRegisteredUser.Visibility = Visibility.Visible;
                tbkSelectUserProfile.Visibility = Visibility.Hidden;
            }
        }

        private void OnBtnRegisterClick(object sender, RoutedEventArgs e)
        {
            // Create a new instance of Registration Window
            RegistrationWindow registrationWindow = new RegistrationWindow();
            registrationWindow.Owner = this;

            // Hide Login Window until Registration Window is closed
            Hide();
            if (registrationWindow.ShowDialog() == true)
            {
                // Refresh window
                cbxLogin.IsEnabled = true;
                btnUploadAll.IsEnabled = true;
                tbkNoRegisteredUser.Visibility = Visibility.Hidden;
                tbkSelectUserProfile.Visibility = Visibility.Visible;
                cbxLogin.SelectedItem = null;

                // Update database of user profiles
                registeredUsers = new UsersFile(ContentDataFile.UsersFilePath).Data;
                cbxLogin.ItemsSource = null;
                cbxLogin.ItemsSource = registeredUsers.Where(u => u.EnvironmentUserName == Environment.UserName).OrderByDescending(u => u.RegistrationDate);
            }
            Show();
        }

        /// <summary>
        /// Even when user click on Login button
        /// </summary>
        /// <param name="sender">Login button</param>
        /// <param name="e">Routed event arguments</param>
        private void OnBtnLoginClick(object sender, RoutedEventArgs e)
        {
            var selectedUserProfile = (UserProfile)cbxLogin.SelectedItem;
            var hashedPassword = UserProfile.ComputeHash(pwbPassword.Password, new SHA512CryptoServiceProvider(), Encoding.ASCII.GetBytes(selectedUserProfile.Email));

            // Login PasswordBox is empty
            if (!string.IsNullOrEmpty(selectedUserProfile.Password))
            {
                if (pwbPassword.SecurePassword.Length == 0)
                {
                    WarningMessage("Pole pro heslo není vyplněno!", pwbPassword);
                    return;
                }

                // Login Password is not correct
                if (selectedUserProfile.Password != hashedPassword)
                {
                    WarningMessage("Zadané heslo není správné!", pwbPassword);
                    return;
                }
            }

            if (rememberUser)
            {
                Properties.Settings.Default.RemeberedUser = selectedUserProfile.Email;
                Properties.Settings.Default.Save();
            }

            grdWarning.Visibility = Visibility.Hidden;
#if NOSESSIONS
            // Check whether there are sessions, if no, create default 'PhrasesRecording' session
            var existingSessions = Session.GetSessions(selectedUserProfile.ProfileFolderPath);
            Session newSession;
            if (!existingSessions.Any())
                // Create a new DEFAULT session
                newSession = Session.Create(selectedUserProfile);
            else
                // Load the first session
                newSession = existingSessions.First();

            DataIntegrator dataIntegrator = new DataIntegrator(selectedUserProfile, newSession);
            MainWindow mainWindow = new MainWindow(selectedUserProfile, newSession, dataIntegrator);
            mainWindow.Show();
            Close();
#else
            sessionManagerWindow = new SessionManagerWindow(user);
            sessionManagerWindow.Show();
            Close();
#endif
        }

        /// <summary>
        /// Set and show warning dialog in login window
        /// </summary>
        /// <param name="message">Warning message text</param>
        /// <param name="control">PasswordBox control</param>
        private void WarningMessage(string message, PasswordBox control)
        {
            tbkWarning.Text = message;
            grdWarning.Visibility = Visibility.Visible;
            control.Focus();
            control.Clear();
        }

        /// <summary>
        /// Event handler when object got focus
        /// </summary>
        /// <param name="sender">TextBox or PasswordBox</param>
        /// <param name="e">Not used there</param>
        private void OnGotFocus(object sender, RoutedEventArgs e)
        {
            if (sender as TextBox != null)
                // Select all in TextBox
                ((TextBox)sender).SelectAll();
            else if (sender as PasswordBox != null)
                // Select all in PasswordBox
                ((PasswordBox)sender).SelectAll();

        }

        /// <summary>
        /// Handler user's click on check box to remeber user profile for automatic login on next application startup
        /// </summary>
        private void OnChbRemeberUserStateChanged(object sender, RoutedEventArgs e)
        {
            if (chbRemeberUser.IsChecked == true)
            {
                /*var dlgResult = await this.ShowMessageAsync("Zapamatovat uživatele", "Pokud zvolíte možnost zapamatování uživatele, bude po přihlášení tento uživatel uložen a při opětovném spuštění aplikace bude automaticky přihlašován. To může ohrozit Vaše soukromí v případě, že tuto aplikaci používá více lidí. Přejete si přesto přihlášeného uživatele zapamatovat?",
                    MessageDialogStyle.AffirmativeAndNegative,
                    new MetroDialogSettings() { AffirmativeButtonText = "Ano", NegativeButtonText = "Ne" });*/

                MessageBoxDialog msgDialog = new MessageBoxDialog("Zapamatovat uživatele",
                    "Pokud zvolíte možnost zapamatování uživatele, bude po přihlášení tento uživatel uložen a při opětovném spuštění aplikace bude automaticky přihlašován. To může ohrozit Vaše soukromí v případě, že tuto aplikaci používá více lidí. Přejete si přesto přihlášeného uživatele zapamatovat?",
                    MessageBoxDialogButtons.YesNo,
                    MahApps.Metro.IconPacks.PackIconFontAwesomeKind.ExclamationCircle,
                    Colors.Goldenrod
                    );
                msgDialog.Owner = this;
                var dlgResult = msgDialog.ShowDialog();

                if (dlgResult == true)
                    rememberUser = true;
                else
                {
                    rememberUser = false;
                    chbRemeberUser.IsChecked = false;
                }
            }
            else
                rememberUser = false;
        }

        /// <summary>
        /// Handler event when user select another item in combo box with user profiles registered in application
        /// </summary>
        private void OnLoginSelectionChanged(object sender, RoutedEventArgs e)
        {
            var selectedUserProfile = (UserProfile)cbxLogin.SelectedItem;
            if (selectedUserProfile != null)
            {
                tbkSelectUserProfile.Visibility = Visibility.Hidden;
                pwbPassword.IsEnabled = !string.IsNullOrEmpty(selectedUserProfile.Password);
                chbRemeberUser.IsEnabled = true;
                btnLogin.IsEnabled = true;
            }
        }

        /// <summary>
        /// Event when user click on Upload All button. Upload data of all users in app.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnBtnUploadAllClick(object sender, RoutedEventArgs e)
        {
            listOfFailed.Clear();
            MessageBoxDialog msgDialog = new MessageBoxDialog("Nahrát data všech uživatelů",
                "Chcete nahrát na server data všech dostupných uživatelů? Tato akce může trvat i několik minut nebo desítek minut v závislosti na počtu uživatelů.",
                MessageBoxDialogButtons.YesNo, MahApps.Metro.IconPacks.PackIconFontAwesomeKind.Upload);
            msgDialog.Owner = this;
            var dlgResult = msgDialog.ShowDialog();

            if (dlgResult != true)
                return;

            var widthOld = Width;
            grdMain.Visibility = Visibility.Hidden;
            Width = 800;
            CenterScreen();

            foreach (UserProfile user in registeredUsers.Where(u => u.EnvironmentUserName == Environment.UserName))
            {
                // Check whether there are sessions, if no, create default 'PhrasesRecording' session
                var existingSessions = Session.GetSessions(user.ProfileFolderPath);
                Session session;
                if (!existingSessions.Any())
                    // Create a new DEFAULT session
                    session = Session.Create(user);
                else
                    // Load the first session
                    session = existingSessions.First();

                await UploadData(user, session);
            }


            Width = widthOld;
            CenterScreen();
            grdMain.Visibility = Visibility.Visible;

            // Show information that data has been successfully uploaded to the server
            MessageBoxDialog msgDialogFin;
            if (!listOfFailed.Any())
            {
                msgDialogFin = new MessageBoxDialog("Nahrávání dokončeno", "Data uživatelů byla úspěšně nahrána na server.", MessageBoxDialogButtons.OK, MahApps.Metro.IconPacks.PackIconFontAwesomeKind.CheckCircleOutline, Colors.Green);
            }
            else
            {
                msgDialogFin = new MessageBoxDialog("Nahrávání selhalo", string.Format("Nahrávání dat nebylo úspěšné. Některá data následujících uživatelů nebyla nahrána na server. Prosím, zkontrolujte připojení k Internetu, nastavení serveru daných profilů a nahrávání opakujte:\n\n{0}", string.Join("\n", listOfFailed)), MessageBoxDialogButtons.OK, MahApps.Metro.IconPacks.PackIconFontAwesomeKind.TimesCircleOutline, Colors.Crimson);
            }
            msgDialogFin.Owner = this;
            msgDialogFin.ShowDialog();
        }

        /// <summary>
        /// Center this window on center of the screen
        /// </summary>
        private void CenterScreen()
        {
            Left = (SystemParameters.WorkArea.Width - Width) / 2 + SystemParameters.WorkArea.Left;
            Top = (SystemParameters.WorkArea.Height - Height) / 2 + SystemParameters.WorkArea.Top;
            InvalidateMeasure();
            UpdateLayout();
        }

        /// <summary>
        /// Asynchronously upload data of given user and session
        /// </summary>
        /// <param name="user">User profile to upload</param>
        /// <param name="session">Session to upload</param>
        /// <returns></returns>
        private async Task UploadData(UserProfile user, Session session)
        {
            uploadFailed = false;
            var exportProgress = await this.ShowProgressAsync(string.Format("Upload dat uživatele {0} {1} ({2})", user.FirstName, user.LastName, user.Email), "Spojování se serverem...", 
                false, new MetroDialogSettings() { AnimateShow = false, AnimateHide = false });
            exportProgress.Minimum = 0;
            exportProgress.Maximum = 100;

            // Save user profile information to its directory to include this file into the ZIP
            string userInfoXml = Path.Combine(user.ProfileFolderPath.FullName, ContentDataFile.UserInfoFilename);
            user.SaveToXML(userInfoXml);

            // Start uploading
            await Task.Run(() =>
            {
                SettingsFile settingsFile = new SettingsFile();
                settingsFile.Load(user.ProfileSettingsPath.FullName);
                FtpClient ftpClient = new FtpClient(settingsFile.Data.SftpServer, new System.Net.NetworkCredential(settingsFile.Data.SftpUserName, settingsFile.Data.SftpPassword));
                try
                {
                    ftpClient.Connect();
                } catch
                {
                    exportProgress.SetTitle("Spojení se serverem nelze navázat");
                    exportProgress.SetMessage(string.Format("Došlo k chybě při nahrávání uživatelského profilu {0} {1} ({2}) a spojení se serverem bylo ukončeno. Ujistěte se, že jste připojeni k Internetu a nahrávání dat zopakujte.", user.FirstName, user.LastName, user.Email));
                    Thread.Sleep(1500);
                    uploadFailed = true;
                    listOfFailed.Add(string.Format("{0} {1} ({2})", user.FirstName, user.LastName, user.Email));
                    return;
                }

                // Check whether root directory exists
                if (!ftpClient.DirectoryExists(settingsFile.Data.SftpRootDirectory))
                {
                    exportProgress.SetMessage("Spojení se serverem nezdařilo!");
                    Thread.Sleep(1000);
                    listOfFailed.Add(string.Format("{0} {1} ({2})", user.FirstName, user.LastName, user.Email));
                    return;
                }

                // Check whether profile folder exists
                try
                {
                    string sftpProfilePath = Path.Combine(settingsFile.Data.SftpRootDirectory, string.Join("_", user.Email, user.Guid));
                    string sftpSessionPath = Path.Combine(sftpProfilePath, session.Name);
                    string sftpApprovedWavesPath = Path.Combine(sftpSessionPath, ContentDataFile.WavesApprovedFolderPath);
                    string sftpRejectedWavesPath = Path.Combine(sftpSessionPath, ContentDataFile.WavesRejectedFolderPath);
                    if (!ftpClient.DirectoryExists(sftpProfilePath))
                    {
                        exportProgress.SetMessage("Vytvářím uživatelský profil na serveru...");
                        ftpClient.CreateDirectory(sftpProfilePath);
                        ftpClient.CreateDirectory(sftpApprovedWavesPath);
                        ftpClient.CreateDirectory(sftpRejectedWavesPath);
                        Thread.Sleep(1000);
                    }

                    exportProgress.SetMessage("Připravuji data pro upload...");
                    exportProgress.Minimum = 0;
                    bool noData = false;
                    try
                    {
                        exportProgress.Maximum = session.WavesRecordedFolderPath.GetFiles("*.wav").Concat(session.WavesRejectedFolderPath.GetFiles("*.wav")).Count() + 1;
                    }
                    catch (DirectoryNotFoundException)
                    {
                        exportProgress.SetMessage(string.Format("Uživatel {0} {1} ({2}) nemá žádná nahraná data...", user.FirstName, user.LastName, user.Email));
                        Thread.Sleep(1500);
                        noData = true;
                    }
                    catch
                    {
                        exportProgress.SetMessage(string.Format("Chyba načítání dat uživatele {0} {1} ({2})...", user.FirstName, user.LastName, user.Email));
                        Thread.Sleep(500);
                        noData = true;
                    }
                    int prog = 0;

                    if (!noData)
                    {
                        // Upload Approved waves
                        foreach (var waveFile in session.WavesRecordedFolderPath.GetFiles("*.wav"))
                        {
                            string waveSftpPath = Path.Combine(sftpApprovedWavesPath, waveFile.Name);
                            ftpClient.UploadFile(waveFile.FullName, waveSftpPath, FtpExists.Append);
                            exportProgress.SetMessage(string.Format("Uploaduji soubor {0}", waveFile.Name));
                            exportProgress.SetProgress(++prog);
                        }

                        // Upload Rejected waves
                        foreach (var waveFile in session.WavesRejectedFolderPath.GetFiles("*.wav"))
                        {
                            string waveSftpPath = Path.Combine(sftpRejectedWavesPath, waveFile.Name);
                            ftpClient.UploadFile(waveFile.FullName, waveSftpPath, FtpExists.Append);
                            exportProgress.SetMessage(string.Format("Uploaduji soubor {0}", waveFile.Name));
                            exportProgress.SetProgress(++prog);
                        }
                    }

                    // Upload XML with information about user
                    ftpClient.UploadFile(userInfoXml, Path.Combine(sftpSessionPath, ContentDataFile.UserInfoFilename), FtpExists.NoCheck);

                    // Upload phrases.xml file
                    exportProgress.SetMessage("Uploaduji soubor frází...");
                    ftpClient.UploadFile(session.PhrasesFilePath.FullName, Path.Combine(sftpSessionPath, session.PhrasesFilePath.Name), FtpExists.NoCheck);
                    exportProgress.SetProgress(++prog);
                    Thread.Sleep(500);
                }
                catch (FtpException)
                {
                    exportProgress.SetTitle("Spojení se serverem nelze navázat");
                    exportProgress.SetMessage("Došlo k chybě při nahrávání a spojení se serverem bylo ukončeno. Ujistěte se, že jste připojeni k Internetu a nahrávání dat zopakujte.");
                    uploadFailed = true;
                }
                finally
                {
                    if (!uploadFailed)
                        exportProgress.SetMessage(string.Format("Data uživatele {0} {1} ({2}) byla úspěšně nahrána!", user.FirstName, user.LastName, user.Email));
                    else
                        listOfFailed.Add(string.Format("{0} {1} ({2})", user.FirstName, user.LastName, user.Email));
                }
            });

            Thread.Sleep(1000);
            await exportProgress.CloseAsync();
        }
    }
}
