﻿using System.Collections.Generic;

// MahApps library
using MahApps.Metro.Controls;

// Tlachapoud libraries
using Tlachapoud.Controls;
using Tlachapoud.Forms.RegistrationPages;
using Tlachapoud.Core.Data;
using Tlachapoud.Controls.Dialogs;
using System.Windows.Media;

namespace Tlachapoud.Forms
{
    /// <summary>
    /// Interaction logic for RegistrationWindow.xaml
    /// </summary>
    public partial class RegistrationWindow : MetroWindow
    {
        /// <summary>
        /// Property encapsulating Registration Bar object
        /// </summary>
        public RegistrationBar RegistrationBar { get; set; }

        /// <summary>
        /// Property encapsulating User Profile
        /// </summary>
        public UserProfile UserProfile { get; set; }
        /// <summary>
        /// Property encapsulating User Settings
        /// </summary>
        public UserSettings UserSettings { get; set; }
        /// <summary>
        /// Flag marking registration procedure as completed
        /// </summary>
        public bool IsComplete { get; set; }

        /// <summary>
        /// Registration Window Constructor
        /// </summary>
        public RegistrationWindow()
        {
            InitializeComponent();

            // Set RegistrationBar to default
            RegistrationBar = cRegistrationBar;
            RegistrationBar.Items = new List<string>() { "Licenční podmínky", "Osobní údaje", "Nastavení", "Shrnutí" };
            RegistrationBar.Step = 1;

            UserSettings = new UserSettings();
            fRegPanel.NavigationService.Navigate(new LicensePage(this)); // Set License Page the first one
        }

        /// <summary>
        /// Event raised when user clicked on window Close button
        /// Aks user whether really wants to close the registration window
        /// </summary>
        #pragma warning disable
        private async void OnRegistrationWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (IsComplete)
            {
                DialogResult = true;
                return;
            }

            if (RegistrationBar.Step > 1 || fRegPanel.NavigationService.CanGoForward)
            {
                MessageBoxDialog msgDialog = new MessageBoxDialog(Messages.Registration.CloseWindowTitle,
                    Messages.Registration.CloseWindowMsg, MessageBoxDialogButtons.YesNo,
                    MahApps.Metro.IconPacks.PackIconFontAwesomeKind.ExclamationCircle, Colors.Goldenrod);
                msgDialog.Owner = this;
                var dialogResult = msgDialog.ShowDialog();
                e.Cancel = dialogResult == false;
            }
            if (!e.Cancel)
                DialogResult = false;
        }
    }
}
