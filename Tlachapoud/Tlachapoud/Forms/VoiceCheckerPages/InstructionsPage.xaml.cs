﻿using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace Tlachapoud.Forms.VoiceCheckerPages
{
    /// <summary>
    /// Page with instructions for Voice Checker
    /// </summary>
    public partial class InstructionsPage : Page
    {
        /// <summary>
        /// Auxiliary object for parent window
        /// </summary>
        private VoiceCheckerWindow parentWindow;

        /// <summary>
        /// Create and initialize Instructions Page for Voice Checker
        /// </summary>
        public InstructionsPage(VoiceCheckerWindow parentWindow)
        {
            this.parentWindow = parentWindow;
            InitializeComponent();

            var textRange = new TextRange(rtxLicense.Document.ContentStart, rtxLicense.Document.ContentEnd);

            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "Tlachapoud.Resources.RichTexts.vcinstructions.rtf";
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                textRange.Load(stream, DataFormats.Rtf);
            }
        }

        /// <summary>
        /// Store whether user want to hide instructions page for next window showup
        /// </summary>
        private void OnHideInstructionsChanged(object sender, RoutedEventArgs e)
        {
            parentWindow.IsHideInstructionsChecked = chbHideInstructions.IsChecked == true;
        }
    }
}
