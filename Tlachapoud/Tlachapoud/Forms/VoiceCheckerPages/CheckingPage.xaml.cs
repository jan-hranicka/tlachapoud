﻿using MahApps.Metro.IconPacks;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.IO;

// Import Tlachapoud libraries
using Tlachapoud.Core.Audio.DirectSound;
using Tlachapoud.Core.Data;

namespace Tlachapoud.Forms.VoiceCheckerPages
{
    /// <summary>
    /// Voice Checker main page
    /// </summary>
    public partial class CheckingPage : Page
    {
        #region Audio objects
        private DirectSoundAudioRecorder audioRecorder;
        private DirectSoundAudioPlayer audioPlayer;
        #endregion Audio objects

        private DataIntegrator dataIntegrator;
        private VoiceCheckerWindow parentWindow;
        private Phrase selectedPhrase;

        private string playText = "Pomocí tlačítka PŘEHRÁT spusťte přehrávání. Budou Vám přehrány dvě verze stejné fráze - první starší, druhá nahraná v tomto okně. Vaším úkolem bude odhadnout, zda se kvalita Vašeho hlasu nezměnila. Pozorně tedy poslouchejte. Pokud nejste s nyní nahranou verzí spokojeni, pomocí tlačítka NAHRÁT ZNOVU nahrajte novou verzi.";
        private string decideText = "Slyšeli jste dvě verze stejné fráze. Změnila se nějak kvalita Vašeho hlasu? Pokud ANO, doporučujeme nahrávání na chvíli přerušit a aplikace bude tudíž automaticky ukončena.";

        /// <summary>
        /// Create a new Checking page
        /// </summary>
        public CheckingPage(DataIntegrator dataIntegrator, VoiceCheckerWindow parentWindow)
        {
            this.dataIntegrator = dataIntegrator;
            this.parentWindow = parentWindow;
            InitializeComponent();

            selectedPhrase = dataIntegrator.PhrasesFile.GetByHistory(dataIntegrator.Settings.VoiceCheckerHistory);
            tbkPhrase.Text = selectedPhrase.Text;
            btnStopRecording.IsEnabled = false;

            // Initialize Audio Recorder & Player
            audioRecorder = new DirectSoundAudioRecorder(dataIntegrator.Settings.CaptureDevice);            
            audioPlayer = new DirectSoundAudioPlayer(dataIntegrator.Settings.RenderDevice);
            audioPlayer.PlaybackStopped += OnAudioPlayerPlaybackStopped;
        }

        /// <summary>
        /// Handle user's click on Start Recording button
        /// </summary>
        private void OnBtnStartRecordingClick(object sender, RoutedEventArgs e)
        {
            btnStartRecording.IsEnabled = false;
            btnStopRecording.IsEnabled = true;
            parentWindow.SettingsButton.IsEnabled = false;

            var tempWaveFile = Path.Combine(dataIntegrator.Session.SessionFolderPath.FullName, "tmp_checker.wav");
            audioRecorder.BeginRecording(tempWaveFile);
        }

        /// <summary>
        /// Handle user's click on Stop Recording button
        /// </summary>
        private void OnBtnStopRecordingClick(object sender, RoutedEventArgs e)
        {
            btnStartRecording.IsEnabled = false;
            btnStopRecording.IsEnabled = false;
            parentWindow.SettingsButton.IsEnabled = true;

            // Stop Audio Recorder
            audioRecorder.StopRecording();

            splRecorder.Visibility = Visibility.Hidden;
            splPlayer.Visibility = Visibility.Visible;
            grChecking.Visibility = Visibility.Visible;
            tbkInfo.Text = playText;
            icoInfo.Kind = PackIconFontAwesomeKind.InfoCircle;
        }

        /// <summary>
        /// Change button icon opacity when it is disabled
        /// </summary>
        private void OnButtonIsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Button btn = sender as Button;
            PackIconEntypo icon = btn.Content as PackIconEntypo;
            if (icon != null)
                icon.Opacity = btn.IsEnabled ? 1.0 : .25;
        }

        /// <summary>
        /// Handle user's click on Re-record button
        /// </summary>
        private void OnBtnRerecordClick(object sender, RoutedEventArgs e)
        {
            audioPlayer.Stop();
            audioPlayer.Dispose();
            audioRecorder.Dispose();

            splRecorder.Visibility = Visibility.Visible;
            splPlayer.Visibility = Visibility.Hidden;
            grChecking.Visibility = Visibility.Hidden;
            splDecisionButtons.Visibility = Visibility.Hidden;
            btnStartRecording.IsEnabled = true;
            btnStopRecording.IsEnabled = false;
            _pom = false;
        }

        private bool _pom = false;
        /// <summary>
        /// Handle user's click on Play button
        /// </summary>
        private void OnBtnPlayClick(object sender, RoutedEventArgs e)
        {
            audioPlayer.Dispose();
            audioPlayer = new DirectSoundAudioPlayer(dataIntegrator.Settings.RenderDevice);
            audioPlayer.PlaybackStopped += OnAudioPlayerPlaybackStopped;
            audioPlayer.Load(selectedPhrase.FNames.First().Path);
            _pom = true;
            audioPlayer.Play();
        }

        /// <summary>
        /// Handle audio player playback stopped event
        /// </summary>
        private void OnAudioPlayerPlaybackStopped(object sender, EventArgs e)
        {
            if (_pom)
            {
                audioPlayer.Load(audioRecorder.FilePath);
                audioPlayer.Reset();            
                _pom = false;
                audioPlayer.Play();
                return;
            }
            
            audioPlayer.Dispose();
            splDecisionButtons.Visibility = Visibility.Visible;
            tbkInfo.Text = decideText;
            icoInfo.Kind = PackIconFontAwesomeKind.QuestionCircle;
        }

        /// <summary>
        /// Handle user's click on Yes button
        /// </summary>
        private void OnBtnYesClick(object sender, RoutedEventArgs e)
        {
            if (parentWindow.IsHideInstructionsChecked)
            {
                dataIntegrator.Settings.VoiceCheckerHideInstructions = parentWindow.IsHideInstructionsChecked;
                dataIntegrator.SettingsFile.Update();
            }
            parentWindow.DialogResult = true;
        }

        /// <summary>
        /// Handle user's click on No button
        /// </summary>
        private void OnBtnNoClick(object sender, RoutedEventArgs e)
        {
            if (parentWindow.IsHideInstructionsChecked)
            {
                dataIntegrator.Settings.VoiceCheckerHideInstructions = parentWindow.IsHideInstructionsChecked;
                dataIntegrator.SettingsFile.Update();
            }
            parentWindow.DialogResult = false;
            Environment.Exit(0);
        }
    }
}
