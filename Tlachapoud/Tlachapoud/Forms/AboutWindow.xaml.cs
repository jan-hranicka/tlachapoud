﻿using MahApps.Metro.Controls;
using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;

namespace Tlachapoud.Forms
{
    /// <summary>
    /// Interaction logic for About Window
    /// </summary>
    public partial class AboutWindow : MetroWindow
    {
        /// <summary>
        /// Create a new About window
        /// </summary>
        public AboutWindow()
        {
            DataContext = this;
            InitializeComponent();

            TextRange textRange = new TextRange(rtxAbout.Document.ContentStart, rtxAbout.Document.ContentEnd);
            var assembly = Assembly.GetExecutingAssembly();
            string resourceName = "Tlachapoud.Resources.RichTexts.About.rtf";
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                textRange.Load(stream, DataFormats.Rtf);
            }

            // Close this window when ESC key pressed
            KeyDown += delegate (object sender, KeyEventArgs e) { Close(); };
        }

        /// <summary>
        /// Get Copyright Text
        /// </summary>
        public string Copyright
        {
            get
            {
                return "NTIS, ZČU 2018";
            }
        }

        /// <summary>
        /// Get .NET Framwork version
        /// </summary>
        public string FrameworkVersion
        {
            get
            {
                var frameworkVersion = Environment.Version;
                return string.Format("{0}.{1}.{2}", frameworkVersion.Major, frameworkVersion.Minor, frameworkVersion.Build);
            }
        }

        #region Assembly Attribute Accessors
        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                var assemblyVersion = Assembly.GetExecutingAssembly().GetName().Version;
                return string.Format("{0}.{1}  (Build {2})", assemblyVersion.Major, assemblyVersion.Minor, assemblyVersion.Build);
            }
        }

        public string AssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public string AssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
        #endregion
    }
}
