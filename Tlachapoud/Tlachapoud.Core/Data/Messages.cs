﻿namespace Tlachapoud.Core.Data
{
    public struct Messages
    {
        public struct Registration
        {
            public static readonly string CloseWindowMsg = "Registrace nebyla dokončena.Opravdu chcete registraci zrušit? Doposud zadané údaje a nastavení budou nenávratně smazána.";
            public static readonly string CloseWindowTitle = "Zrušit registraci";
        }
    }
}
