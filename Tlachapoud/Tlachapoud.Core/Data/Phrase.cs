﻿using System.Collections.Generic;
using System.Linq;

namespace Tlachapoud.Core.Data
{
    /// <summary>
    /// Object representing a phrase
    /// </summary>
    public class Phrase
    {
        #region Phrase Main Properties
        /// <summary>
        /// Id of a phrase (indexed from zero)
        /// </summary>
        public int Id { get; private set; }
        /// <summary>
        /// Text of a phrase
        /// </summary>
        public string Text { get; private set; }
        /// <summary>
        /// Wanted file name mask of a phrase
        /// </summary>
        public string WantFName { get; set; }
        /// <summary>
        /// State of a phrase (enumeration value)
        /// </summary>
        public PhraseState State { get; set; }
        /// <summary>
        /// List of successfully recorded wave files
        /// </summary>
        public List<WaveFile> FNames { get; set; }
        /// <summary>
        /// List of rejected recorded wave files
        /// </summary>
        public List<WaveFile> Rejected { get; set; }
        #endregion Phrase Main Properties

        /// <summary>
        /// Create a new Phrase object containing all data about a given phrase
        /// </summary>
        /// <param name="id">Id of the phrase</param>
        /// <param name="text">Text of the phrase</param>
        /// <param name="wantFName">Wanted file name mask of the phrase</param>
        /// <param name="state">State of the phrase</param>
        public Phrase(int id, string text, string wantFName=null, PhraseState state=PhraseState.Unrecorded)
        {
            Id = id;
            Text = text;
            WantFName = wantFName;
            State = state;
            FNames = new List<WaveFile>();
            Rejected = new List<WaveFile>();
        }

        /// <summary>
        /// Get a file name for a new phrase successfully recorded representation
        /// </summary>
        /// <returns>File name formatted as: mask_number</returns>
        public string GetFName()
        {
            int id = FNames == null ? 0 : FNames.Count();
            return string.Format("{0}_{1}", WantFName, id.ToString("00"));
        }

        /// <summary>
        /// Get last successfully recorded wave file
        /// </summary>
        /// <returns>WaveFile object of a last successfully recorded wave file</returns>
        public WaveFile GetLastFName()
        {
            return FNames.Last();
        }

        /// <summary>
        /// Get a file name for a new phrase rejected representation
        /// </summary>
        /// <returns>File name formatted as: mask_number</returns>
        public string GetRejectedFName()
        {
            int id = Rejected == null ? 0 : Rejected.Count();
            return string.Format("{0}_r{1}", WantFName, id.ToString("00"));
        }

        /// <summary>
        /// Get last rejected wave file
        /// </summary>
        /// <returns>WaveFile object of a last rejected wave file</returns>
        public WaveFile GetLastRejectedFName()
        {
            return Rejected.Last();
        }
    }

    /// <summary>
    /// Enumeration of all provided phrase states
    /// </summary>
    public enum PhraseState
    {
        Unrecorded,
        Recorded,
        Skipped
    }
}
