﻿using NAudio.CoreAudioApi;
using System;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using Tlachapoud.Core.Net;

namespace Tlachapoud.Core.Data
{
    public class UserSettings
    {
        #region Settings Properties
        // Voice Checker
        /// <summary>
        /// Is VoiceChecker active flag
        /// </summary>
        public bool VoiceCheckerActive { get; set; }
        /// <summary>
        /// VoiceChecker interval between particular checks
        /// </summary>
        public int VoiceCheckerInterval { get; set; }
        /// <summary>
        /// History defining how many days to look ahead for phrase for checking
        /// </summary>
        public int VoiceCheckerHistory { get; set; }
        /// <summary>
        /// Show VoiceChecker help flag
        /// </summary>
        public bool VoiceCheckerHideInstructions { get; set; }

        // Input & Output device
        public MMDevice CaptureDevice { get; set; }
        public MMDevice RenderDevice { get; set; }

        // Sftp connection
        public string SftpServer { get; set; }
        public string SftpUserName { get; set; }

        private string encryptedPassword;
        public string SftpPassword {
            set { encryptedPassword = SftpSecuredData.Encrypt(value); }
            get { return SftpSecuredData.Decrypt(encryptedPassword); }
        }

        public string SftpRootDirectory { get; set; }

        // Check Modules
        /// <summary>
        /// Is Intensity CheckModule active flag
        /// </summary>
        public bool CheckModuleIntensityActive { get; set; }
        /// <summary>
        /// Is Pauses CheckModule active flag
        /// </summary>
        public bool CheckModulePausesActive { get; set; }
        /// <summary>
        /// Automatically skip phrase after 3 rejected attemps to record it flag
        /// </summary>
        public bool CheckModuleAutomaticSkip { get; set; }

        // Application
        /// <summary>
        /// Show startup flyout flag
        /// </summary>
        public bool AppShowStartupHelp { get; set; }
        /// <summary>
        /// Phrase font size
        /// </summary>
        public double AppPhraseFontSize { get; set; } 
        /// <summary>
        /// General application button size
        /// </summary>
        public double AppButtonFontSize { get; set; }
        /// <summary>
        /// Navigation buttons size
        /// </summary>
        public double AppNavigationButtonSize { get; set; }
        /// <summary>
        /// Player buttons size
        /// </summary>
        public double AppPlayerButtonSize { get; set; }
        /// <summary>
        /// Recorder buttons size
        /// </summary>
        public double AppRecorderButtonsSize { get; set; }
        /// <summary>
        /// Is automatic upload to SFTP enabled flag
        /// </summary>
        public bool AppAutomaticUploadToSftp { get; set; }
        /// <summary>
        /// Is automatic move to following phrase enabled flag
        /// </summary>
        public bool AppAutomaticMoveToFollowing { get; set; }
        #endregion  Settings Properties

        /// <summary>
        /// Createa a new instance of user settings
        /// </summary>
        public UserSettings()
        {
            // Set default values for Check Modules
            CheckModuleAutomaticSkip = DefaultSettings.CheckModules.IsAutomaticSkipEnabled;
            CheckModulePausesActive = DefaultSettings.CheckModules.IsPausesModuleActive;
            CheckModuleIntensityActive = DefaultSettings.CheckModules.IsIntensityModuleActive;

            // Set default values for Voice Checker
#if VOICECHECKER
            VoiceCheckerActive = DefaultSettings.VoiceChecker.IsActive;
            VoiceCheckerHistory = DefaultSettings.VoiceChecker.History;
            VoiceCheckerInterval = DefaultSettings.VoiceChecker.Interval;
            VoiceCheckerHideInstructions = DefaultSettings.VoiceChecker.IsStartupHelpEnabled;
#endif

            // Set default values for SFTP
            SftpServer = DefaultSettings.Sftp.Server;
            SftpUserName = DefaultSettings.Sftp.Username;
            SftpRootDirectory = DefaultSettings.Sftp.RootDir;
            SftpPassword = DefaultSettings.Sftp.Password;

            // Set default values for Application
            AppAutomaticMoveToFollowing = DefaultSettings.Application.IsAutomaticMoveToFollowingEnabled;
            AppAutomaticUploadToSftp = DefaultSettings.Application.IsAutomaticUploadToSftpEnabled;
            AppButtonFontSize = DefaultSettings.Application.ButtonsFontSize;
            AppNavigationButtonSize = DefaultSettings.Application.NavigationButtonsSize;
            AppPhraseFontSize = DefaultSettings.Application.PhraseFontSize;
            AppPlayerButtonSize = DefaultSettings.Application.PlayerButtonsSize;
            AppRecorderButtonsSize = DefaultSettings.Application.RecorderButtonsSize;
            AppShowStartupHelp = DefaultSettings.Application.IsStartupHelpEnabled;
        } 

        /// <summary>
        /// Compute hash from given string using provided hash algorithm
        /// </summary>
        /// <param name="input">String to hash</param>
        /// <param name="algorithm">Algorithm that should be used to compute hash</param>
        /// <returns></returns>
        private static string ComputeHash(SecureString password, HashAlgorithm algorithm)
        {
            byte[] inputBytes = Encoding.UTF8.GetBytes(new System.Net.NetworkCredential(null, password).Password);
            byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
    }
}
