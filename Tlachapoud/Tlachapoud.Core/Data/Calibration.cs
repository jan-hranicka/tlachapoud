﻿namespace Tlachapoud.Core.Data
{
    /// <summary>
    /// Application calibration
    /// </summary>
    public struct Calibration
    {
        /// <summary>
        /// Calibration of Buttons
        /// </summary>
        public struct Buttons
        {
            public static CalibrationObject<double> NavigationButton { get; } = new CalibrationObject<double>(40, 60, DefaultSettings.Application.NavigationButtonsSize);
            public static CalibrationObject<double> RecorderButton { get; } = new CalibrationObject<double>(50, 75, DefaultSettings.Application.RecorderButtonsSize);
        }

        /// <summary>
        /// Calibration of Text
        /// </summary>
        public struct Text
        {
            public static CalibrationObject<double> ButtonFontSize { get; } = new CalibrationObject<double>(8, 18, DefaultSettings.Application.ButtonsFontSize);
            public static CalibrationObject<double> PhraseFontSize { get; } = new CalibrationObject<double>(20, 42, DefaultSettings.Application.PhraseFontSize);
        }

        /// <summary>
        /// Create a new object storing calibration of type T
        /// </summary>
        /// <typeparam name="T">Data type for calibrations</typeparam>
        public struct CalibrationObject<T>
        {
            private T _minValue;
            /// <summary>
            /// Get max calibrated value
            /// </summary>
            public T Min { get { return _minValue; } }

            private T _maxValue;
            /// <summary>
            /// Get min calibrated value
            /// </summary>
            public T Max { get { return _maxValue; } }

            private T _defValue;
            public T Default { get { return _defValue; } }

            /// <summary>
            /// Create a new calibration object
            /// </summary>
            /// <param name="minValue">Min value</param>
            /// <param name="maxValue">Max value</param>
            public CalibrationObject(T minValue, T maxValue, T defValue)
            {
                _minValue = minValue;
                _maxValue = maxValue;
                _defValue = defValue;
            }
        }
    }
}
