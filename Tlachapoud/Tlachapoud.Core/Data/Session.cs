﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using Tlachapoud.Core.IO;

namespace Tlachapoud.Core.Data
{
    /// <summary>
    /// Class representing a recording session making a copy of workspace for a new parallel recording in application
    /// </summary>
    public class Session
    {
        /// <summary>
        /// Default session name
        /// </summary>
        public const string DEFAULT_NAME = "PhrasesRecording";

        #region Session Properties
        /// <summary>
        /// Session name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Session creation date
        /// </summary>
        public DateTime CreationDate { get; set; }
        /// <summary>
        /// Number of phrases available in session
        /// </summary>
        public int TotalPhrases { get; set; }
        /// <summary>
        /// Number of phrases already recorded in session
        /// </summary>
        public int RecordedPhrases { get; set; }
        /// <summary>
        /// Flag indicating whether session recording is already finished (true) or not (false)
        /// </summary>
        public bool IsFinished { get; set; }
        /// <summary>
        /// Phrases XML file FileInfo object
        /// </summary>
        public FileInfo PhrasesFilePath { get; set; }
        /// <summary>
        /// Session folder DirectoryInfo object
        /// </summary>
        public DirectoryInfo SessionFolderPath { get; set; }
        /// <summary>
        /// DirectoryInfo object for directory storing successfully recorded wave files
        /// </summary>
        public DirectoryInfo WavesRecordedFolderPath
        {
            get { return new DirectoryInfo(Path.Combine(SessionFolderPath.FullName, ContentDataFile.WavesApprovedFolderPath)); }
        }
        /// <summary>
        /// DirectoryInfo object for directory storing unsuccessfully (rejected) recorded wave files
        /// </summary>
        public DirectoryInfo WavesRejectedFolderPath
        {
            get { return new DirectoryInfo(Path.Combine(SessionFolderPath.FullName, ContentDataFile.WavesRejectedFolderPath)); }
        }
        /// <summary>
        /// FileInfo object for CheckModules configuration file
        /// </summary>
        public FileInfo CheckModulesConfigFilePath
        {
            get { return new FileInfo(Path.Combine(SessionFolderPath.FullName, ContentDataFile.CheckModulesConfigFileName)); }
        }
        /// <summary>
        /// Percentual progress of recording in case of the session
        /// </summary>
        public double Progress
        {
            get
            {
                if (TotalPhrases == 0)
                    return 10;

                return RecordedPhrases/TotalPhrases;
            }
        }
        #endregion Session Properties

        /// <summary>
        /// Load session from directory
        /// </summary>
        /// <param name="dirInfo">DIrectory with session workspace</param>
        /// <returns>Session object storing all information about session</returns>
        public static Session Load(DirectoryInfo dirInfo)
        {
            // Load Phrases.xml to get the info
            PhrasesFile phrasesFile = new PhrasesFile(Path.Combine(dirInfo.FullName, ContentDataFile.PhrasesXmlFileName));


            return new Session() {
                CreationDate = dirInfo.CreationTime,
                Name = dirInfo.Name,
                TotalPhrases = phrasesFile.TotalPhrases,
                RecordedPhrases = phrasesFile.RecordedPhrases,
                IsFinished = phrasesFile.IsFinished,
                PhrasesFilePath = new FileInfo(Path.Combine(dirInfo.FullName, ContentDataFile.PhrasesXmlFileName)),
                SessionFolderPath = dirInfo
            };
        }

        /// <summary>
        /// Get all sessions from given directory if there are any
        /// </summary>
        /// <param name="dirInfo">Directory storing sessions</param>
        /// <returns>IEnumerable object of session objects</returns>
        public static IEnumerable<Session> GetSessions(DirectoryInfo dirInfo)
        {
            if (!dirInfo.Exists)
                yield return null;

            var sessions = dirInfo.GetDirectories();
            foreach (var session in sessions)
            {
                yield return Load(session);
            }
        }

        /// <summary>
        /// Create a new session for given user with a specific name (default name set if name is null)
        /// </summary>
        /// <param name="userProfile">User profile</param>
        /// <param name="sessionName">Session name</param>
        /// <returns></returns>
        public static Session Create(UserProfile userProfile, string sessionName=null)
        {
            // Create a new session
            var sessionFolderPath = Path.Combine(userProfile.ProfileFolderPath.FullName, sessionName == null ? DEFAULT_NAME : sessionName);
            Directory.CreateDirectory(sessionFolderPath);

            // Copy Phrases.xml to the Profile data session folder
            XDocument appPhrases = XDocument.Load(ContentDataFile.PhrasesFilePath);
            appPhrases.Save(Path.Combine(sessionFolderPath, ContentDataFile.PhrasesXmlFileName));

            // Copy Settings.xml to the Profile data session folder
            if (userProfile.ProfileSettingsPath.Exists)
            {
                XDocument userSettings = XDocument.Load(userProfile.ProfileSettingsPath.FullName);
                userSettings.Save(Path.Combine(sessionFolderPath, ContentDataFile.SettingsXmlFileName));
            }
            else
            {
                SettingsFile settingsFile = new SettingsFile();
                settingsFile.Load(userProfile.ProfileSettingsPath.FullName);
            }

            // Copy CheckModules.cfg to the Profile data session folder
            XDocument checkModulesConfig = XDocument.Load(ContentDataFile.CheckModulesConfigFilePath);
            checkModulesConfig.Save(Path.Combine(sessionFolderPath, ContentDataFile.CheckModulesConfigFileName));

            return Load(new DirectoryInfo(sessionFolderPath));
        }
    }
}
