﻿using Tlachapoud.Core.Net;

namespace Tlachapoud.Core.Data
{
    /// <summary>
    /// Default settings structure
    /// </summary>
    public struct DefaultSettings
    {
        /// <summary>
        /// Default application settings
        /// </summary>
        public struct Application
        {
            public static bool IsStartupHelpEnabled { get { return true; } }
            public static double PhraseFontSize { get { return 26d; } }
            public static double ButtonsFontSize { get { return 14d; } }
            public static double PlayerButtonsSize { get { return 55d; } }
            public static double RecorderButtonsSize { get { return 65d; } }
            public static double NavigationButtonsSize { get { return 50d; } }
            public static bool IsAutomaticUploadToSftpEnabled { get { return false; } }
            public static bool IsAutomaticMoveToFollowingEnabled { get { return true; } }
        }

        /// <summary>
        /// Default CheckModules settings
        /// </summary>
        public struct CheckModules
        {
            public static bool IsIntensityModuleActive { get { return true; } }
            public static bool IsPausesModuleActive { get { return true; } }
            public static bool IsAutomaticSkipEnabled { get { return false; } }
        }

#if VOICECHECKER
        public struct VoiceChecker
        {
            public static bool IsStartupHelpEnabled { get { return false; } }
            public static bool IsActive { get { return true; } }
            public static int Interval { get { return 80; } }
            public static int History { get { return 3; } }
        }
#endif

        /// <summary>
        /// Default SFTP settings
        /// </summary>
        public struct Sftp
        {
            public static string Server { get { return SftpLogin.HostAddress; } }
            public static string Username { get { return SftpLogin.UserName; } }
            public static string Password { get { return SftpLogin.Password; } }
            public static string RootDir { get { return SftpLogin.MainDir; } }
        }
    }
}
