﻿using System;
using System.IO;
using System.Reflection;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Linq;
using Tlachapoud.Core.IO;

namespace Tlachapoud.Core.Data
{
    /// <summary>
    /// Object representing user profile with personal data
    /// </summary>
    public class UserProfile
    {
        /// <summary>
        /// User personal data 
        /// </summary>
        #region User Personal Data Properties    
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; private set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }
        public string Nickname { get; set; }
        public int? YearOfBirth { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Degree { get; set; }
        public bool HasRegionalAccent { get; set; }
        public string RegionalAccentKind { get; set; }
        public bool IsUsedToSpeakCoherently { get; set; }
        public string EnvironmentUserName { get; set; }
        public string Guid { get; set; }
        #endregion User Personal Data Properties

        /// <summary>
        /// Get user's profile main folder path as a DirectoryInfo object
        /// </summary>
        public DirectoryInfo ProfileFolderPath
        {
            get { return new DirectoryInfo(Path.Combine(ContentDataFile.UserDataFolderPath, Email)); }
        }

        /// <summary>
        /// Get user's profile settings file path as a FileInfo object
        /// </summary>
        public FileInfo ProfileSettingsPath
        {
            get { return new FileInfo(Path.Combine(ProfileFolderPath.FullName, "settings.xml")); }
        }

        /// <summary>
        /// Flag whether the user profile has set all data required for registration
        /// </summary>
        /// <returns>True if all required data have been already set, otherwise false</returns>
        public bool IsComplete()
        {
            return ((!string.IsNullOrEmpty(FirstName))
                && !(string.IsNullOrEmpty(LastName))
                && (!string.IsNullOrEmpty(Password))
                && (!string.IsNullOrEmpty(Email))
                && (!string.IsNullOrEmpty(Nickname))
                && (YearOfBirth != null));
        }

        /// <summary>
        /// Set the password, compute SHA256 hash from secured string
        /// </summary>
        /// <param name="securePassword">Secured string containing password</param>
        /// <param name="hash">Compute SHA256 hash from secure string flag (default: true)</param>
        public void SetPassword(SecureString securePassword, bool hash=true)
        {
            if (hash)
                Password = ComputeHash(new System.Net.NetworkCredential(string.Empty, securePassword).Password, new SHA512CryptoServiceProvider(), Encoding.ASCII.GetBytes(Email));
            else
                Password = new System.Net.NetworkCredential(string.Empty, securePassword).Password;
        }

        /// <summary>
        /// Set the new GUID
        /// </summary>
        public void SetGuid()
        {
            if (string.IsNullOrEmpty(Guid))
            {
                Guid = System.Guid.NewGuid().ToString();
            }
        }

        /// <summary>
        /// Compute hash from given string using provided hash algorithm
        /// </summary>
        /// <param name="input">String to hash</param>
        /// <param name="algorithm">Algorithm that should be used to compute hash</param>
        /// <param name="salt">Salt for password</param>
        /// <returns></returns>
        public static string ComputeHash(string input, HashAlgorithm algorithm, Byte[] salt)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            // Combine salt and input bytes
            Byte[] saltedInput = new Byte[salt.Length + inputBytes.Length];
            salt.CopyTo(saltedInput, 0);
            inputBytes.CopyTo(saltedInput, salt.Length);

            Byte[] hashedBytes = algorithm.ComputeHash(saltedInput);

            return BitConverter.ToString(hashedBytes);
        }

        /// <summary>
        /// Export user's profile information as an XML file
        /// </summary>
        /// <param name="path">Full path where to generate XML file</param>
        public void SaveToXML(string path)
        {
            XDocument xml = new XDocument(new XElement("User",
                    new XElement(UserProfileData.FirstName.ToString(), FirstName),
                    new XElement(UserProfileData.LastName.ToString(), LastName),
                    new XElement(UserProfileData.Degree.ToString(), Degree),
                    new XElement(UserProfileData.Email.ToString(), Email),
                    new XElement(UserProfileData.Gender.ToString(), Gender.ToString()),
                    new XElement(UserProfileData.YearOfBirth.ToString(), YearOfBirth.ToString()),
                    new XElement(UserProfileData.PhoneNumber.ToString(), PhoneNumber),
                    new XElement(UserProfileData.Password.ToString(), Password),
                    new XElement(UserProfileData.Nickname.ToString(), Nickname),
                    new XElement(UserProfileData.IsUsedToSpeakCoherently.ToString(), IsUsedToSpeakCoherently),
                    new XElement(UserProfileData.HasRegionalAccent.ToString(), HasRegionalAccent),
                    new XElement(UserProfileData.RegionalAccentKind.ToString(), RegionalAccentKind),
                    new XElement(UserProfileData.RegistrationDate.ToString(), RegistrationDate),
                    new XElement(UserProfileData.EnvironmentUserName.ToString(), EnvironmentUserName),
                    new XElement(UserProfileData.Guid.ToString(), Guid))
                );
            xml.Save(path);
        }

        /// <summary>
        /// Get property value as a string from enumerated values
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public string this[UserProfileData propertyName]
        {
            get
            {
                Type propType = typeof(UserProfile);
                PropertyInfo propInfo = propType.GetProperty(propertyName.ToString());
                return propInfo.GetValue(this, null).ToString();
            }
        }
    }

    /// <summary>
    /// Enumeration containing Male and Female gender type
    /// </summary>
    public enum Gender
    {
        Male,
        Female
    }
}
