﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using Tlachapoud.Core.Audio.CheckModules;
using Tlachapoud.Core.IO;
using Tlachapoud.Core.Net;

namespace Tlachapoud.Core.Data
{
    /// <summary>
    /// Class representing an encapsulating structure storing important application data
    /// </summary>
    public class DataIntegrator
    {
        #region Data Integrator Properties
        /// <summary>
        /// Current user logged into the app
        /// </summary>
        public UserProfile User { get; private set; }
        /// <summary>
        /// Currently selected session
        /// </summary>
        public Session Session { get; private set; }
        /// <summary>
        /// Currently used phrases.xml file
        /// </summary>
        public PhrasesFile PhrasesFile { get; private set; }
        /// <summary>
        /// Currently used settings.xml file
        /// </summary>
        public SettingsFile SettingsFile { get; private set; }
        /// <summary>
        /// Currently loaded settings for logged-in user
        /// </summary>
        public UserSettings Settings { get { return SettingsFile.Data; } }
        /// <summary>
        /// Currently loaded check module configuration
        /// </summary>
        public IEnumerable<CheckModuleConfig> CheckModuleConfigs { get; set; }
        /// <summary>
        /// Currently used and active check modules
        /// </summary>
        public CheckModules CheckModules { get; set; }
        /// <summary>
        /// Reference to the current phrase
        /// </summary>
        public Phrase CurrentPhrase { get; set; }
        /// <summary>
        /// Capture device not active notification message
        /// </summary>
        public string CaptureDeviceNotificationMsg { get; private set; }
        /// <summary>
        /// Render device not active notification message
        /// </summary>
        public string RenderDeviceNotificationMsg { get; private set; }
        #endregion Data Integrator Properties

        /// <summary>
        /// Create a new DataIntegrator instance
        /// </summary>
        /// <param name="userProfile">Logged-in user profile</param>
        /// <param name="session">Selected session</param>
        public DataIntegrator(UserProfile userProfile, Session session)
        {
            User = userProfile;
            Session = session ?? Session.Create(userProfile);

            PhrasesFile = new PhrasesFile(Session.PhrasesFilePath.FullName);
            PhrasesFile.SetTokenToFirstUnrecorded(); // Set current phrase to the first unrecorded
            CurrentPhrase = PhrasesFile.GetFirstUnrecorded();

            // Copy CheckModules.cfg to the Profile data session folder
            if (!File.Exists(Session.CheckModulesConfigFilePath.FullName))
            {
                XDocument checkModulesConfig = XDocument.Load(ContentDataFile.CheckModulesConfigFilePath);
                checkModulesConfig.Save(Session.CheckModulesConfigFilePath.FullName);
            }

            // Create folder for recorded waves
            if (!Session.WavesRecordedFolderPath.Exists)
                Session.WavesRecordedFolderPath.Create();
            if (!Session.WavesRejectedFolderPath.Exists)
                Session.WavesRejectedFolderPath.Create();

            // Load Check Modules
            CheckModuleConfigs = CheckModulesConfigFile.Load(Session.CheckModulesConfigFilePath.FullName);

            // Load settings or use default one
            SettingsFile = new SettingsFile();
            SettingsFile.CaptureDeviceNotActive += OnCaptureDeviceNotActive;
            SettingsFile.RenderDeviceNotActive += OnRenderDeviceNotActive;
            SettingsFile.Load(User.ProfileSettingsPath.FullName);

            // Initialize Check Modules
            if (!SettingsFile.Data.CheckModuleIntensityActive)
                CheckModuleConfigs = CheckModuleConfigs.ForEach(m => m.IsActive = false, m => m.ModuleName == AvailableCheckModules.Intensity);
            if (!SettingsFile.Data.CheckModulePausesActive)
                CheckModuleConfigs = CheckModuleConfigs.ForEach(m => m.IsActive = false, m => m.ModuleName == AvailableCheckModules.PauseLen);
            CheckModules = new CheckModules(CheckModuleConfigs);
        }

        /// <summary>
        /// Event raised when capture device set in settings is not active at the moment
        /// </summary>
        private void OnCaptureDeviceNotActive(object sender, DeviceSettingsEventArgs args)
        {
            if (args.State == DeviceSettingsState.SetButNotActive)
            {
                CaptureDeviceNotificationMsg = string.Format("Zařízení {0} uložené v nastavení není aktivní. Pokud si přejete nahrávat tímto zařízením, prosím, připojte jej k počítači. Pokud si přejete používat jiné zařízení, nastavte si jej v nastavení zvuku.",
                    args.DeviceSet.DeviceFriendlyName, args.DeviceDefault.DeviceFriendlyName);
            }

            if (args.State == DeviceSettingsState.SetButNotExists)
            {
                CaptureDeviceNotificationMsg = string.Format("Vstupní zařízení pro záznam zvuku není nastaveno. Prosím, připojte k počítači Váš mikrofon a nastavte jej v nastavení jako požadované vstupní zařízení.");
            }
        }

        /// <summary>
        /// Event raised when render device set in settings is not active at the moment
        /// </summary>
        private void OnRenderDeviceNotActive(object sender, DeviceSettingsEventArgs args)
        {
            if (args.State == DeviceSettingsState.SetButNotActive)
            {
                RenderDeviceNotificationMsg = string.Format("Zařízení {0} uložené v nastavení není aktivní. Jako defaultní vstupní zařízení pro nahrávání je nyní nastaveno zařízení {1}. Pokud si nepřejete nahrávat tímto zařízením, prosím, změňte jej v nastavení aplikace",
                    args.DeviceSet.DeviceFriendlyName, args.DeviceDefault.DeviceFriendlyName);
            }
        }
    }
}
