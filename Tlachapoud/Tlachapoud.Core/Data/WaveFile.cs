﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Tlachapoud.Core.Audio.CheckModules;

namespace Tlachapoud.Core.Data
{
    /// <summary>
    /// Object representing a Wave file containing recorded audio data
    /// </summary>
    public class WaveFile
    {
        #region WaveFile Properties
        /// <summary>
        /// Wave file path
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Wave file name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Duration of Wave file
        /// </summary>
        public TimeSpan Duration { get; set; }
        /// <summary>
        /// Record time in Unix format
        /// </summary>
        public ulong RecordTime { get; set; }
        /// <summary>
        /// List of messages from the CheckModules.Check method
        /// </summary>
        public IEnumerable<CheckModuleState> States { get; set; }
        /// <summary>
        /// Flag indicating whether recorded wave is Fine or not
        /// </summary>
        public bool IsFine { get { return States.First() == CheckModuleState.Fine; } }
        /// <summary>
        /// Wave file qualification tag (Preferred/Bad)
        /// </summary>
        public WaveFileTag Tag { get; set; }
        /// <summary>
        /// Check result (Approved/Rejected)
        /// </summary>
        public CheckModuleResult CheckResult { get; set; }
        /// <summary>
        /// Messages from the CheckModules.Check method formatted as oneline string
        /// </summary>
        public string StatesMessage { get { return string.Join(" ", States.Select(s => CheckModuleStateConvertor.GetMessage(s))); } }
        /// <summary>
        /// Id of device which capture the wave file
        /// </summary>
        public string CaptureDeviceId { get; set; }
        #endregion WaveFile Properties

        /// <summary>
        /// Create a new instance of WaveFile object storing most of information about recorded Wave
        /// </summary>
        /// <param name="fileInfo">FileInfo of Wave file</param>
        /// <param name="recordTime">Record time in Unix format</param>
        /// <param name="duration">Duration</param>
        /// <param name="checkResult">Result of check by CheckModules</param>
        /// <param name="tag">Qualification tag</param>
        /// <param name="states">List of states from CM check</param>
        public WaveFile(FileInfo fileInfo, ulong recordTime, TimeSpan duration, CheckModuleResult checkResult, WaveFileTag tag, IEnumerable<CheckModuleState> states, string captureDeviceId)
        {
            Path = fileInfo.FullName;
            Name = fileInfo.Name.Replace(".wav", "");
            CheckResult = checkResult;
            Tag = tag;
            States = states;
            RecordTime = recordTime;
            Duration = duration;
            CaptureDeviceId = captureDeviceId;
        }
    }

    /// <summary>
    /// Wave file qualification tags enumeration
    /// </summary>
    public enum WaveFileTag
    {
        NotSpecified,
        Good,
        Bad
    }
}
