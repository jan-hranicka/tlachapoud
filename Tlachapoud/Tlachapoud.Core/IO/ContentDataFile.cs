﻿using System;
using System.IO;

namespace Tlachapoud.Core.IO
{
    public struct ContentDataFile
    {
        public static readonly string PhrasesXmlFileName = "phrases.xml";
        public static readonly string UsersXmlFileName = "users.xml";
        public static readonly string CheckModulesConfigFileName = "CheckModules.cfg";
        public static readonly string SettingsXmlFileName = "settings.xml";
        public static readonly string AppDataFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "AppData");
        public static readonly string UserDataFolderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Phrase Recorder");
        public static readonly string UsersFilePath = Path.Combine(AppDataFolderPath, UsersXmlFileName);
        public static readonly string PhrasesFilePath = Path.Combine(AppDataFolderPath, PhrasesXmlFileName);
        public static readonly string CheckModulesConfigFilePath = Path.Combine(AppDataFolderPath, CheckModulesConfigFileName);
        public static readonly string WavesApprovedFolderPath = "WavesApproved";
        public static readonly string WavesRejectedFolderPath = "WavesRejected";
        public static readonly string UserInfoFilename = "UserInfo.xml";
    }
}
