﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Tlachapoud.Core.Data;

using NAudio.CoreAudioApi;
using Tlachapoud.Core.Audio.Wasapi;
using Tlachapoud.Core.Net;

namespace Tlachapoud.Core.IO
{
    /// <summary>
    /// Class representing reader and writer for settings XML file
    /// </summary>
    public class SettingsFile
    {
        /// <summary>
        /// Root element name
        /// </summary>
        private const string ROOT = "Settings";
        /// <summary>
        /// Data stored in Phrases XML file as ienumerable object of Phrase objects
        /// </summary>
        public UserSettings Data { get; set; }
        /// <summary>
        /// XML document Linq object
        /// </summary>
        public XDocument XmlDocument { get; protected set; }
        /// <summary>
        /// Phrases file FileInfo object
        /// </summary>
        private FileInfo fileInfo;
        /// <summary>
        /// Event when capture device set in settings is not currently active
        /// </summary>
        public event DeviceNotActiveEventHandler CaptureDeviceNotActive;
        /// <summary>
        /// Event when render device set in settings is not currently active
        /// </summary>
        public event DeviceNotActiveEventHandler RenderDeviceNotActive;
        /// <summary>
        /// Handler when device set in settings is not device
        /// </summary>
        public delegate void DeviceNotActiveEventHandler(object sender, DeviceSettingsEventArgs args);

        /// <summary>
        /// Create a new instance referring to settings XML file (or creating a new one)
        /// </summary>
        public SettingsFile()
        {        
        }

        public void Load(string path)
        {
            fileInfo = new FileInfo(path);
            if (!fileInfo.Exists)
            {
                Directory.CreateDirectory(fileInfo.DirectoryName);

                XmlDocument = new XDocument(new XElement(ROOT));
                XmlDocument.Save(fileInfo.FullName);
            }
            else
            {
                XmlDocument = XDocument.Load(fileInfo.FullName);
            }
            Data = LoadData();
        }
        /// <summary>
        /// Update settings information stored in XML file
        /// </summary>
        public void Update()
        {
            // Check whether XML document is loaded
            if (XmlDocument == null)
                throw new NullReferenceException("XML file has not been loaded - use Load method before calling this method!");

            var settings = Data;
            if (settings != null)
            {
                // Write Audio settings
                var inputDeviceFromXML = XmlDocument.Root.Descendants("InputDeviceId")?.FirstOrDefault()?.Value;
                var outputDeviceFromXML = XmlDocument.Root.Descendants("OutputDeviceId")?.FirstOrDefault()?.Value;

                XElement audioGroup = new XElement("AudioSettings",
                    new XElement("InputDeviceId", settings.CaptureDevice?.ID ?? inputDeviceFromXML),
                    new XElement("OutputDeviceId", settings.RenderDevice?.ID ?? outputDeviceFromXML));
                UpdateElement("AudioSettings", audioGroup);

                // Write Voice Checker settings
                XElement voiceCheckerGroup = new XElement("VoiceCheckerSettings",
                    new XElement("IsActive", settings.VoiceCheckerActive),
                    new XElement("Interval", settings.VoiceCheckerInterval),
                    new XElement("History", settings.VoiceCheckerHistory),
                    new XElement("HideInstructions", settings.VoiceCheckerHideInstructions));
                UpdateElement("VoiceCheckerSettings", voiceCheckerGroup);

                // Write Sftp settings
                XElement sftpGroup = new XElement("SftpSettings",
                    new XElement("Server", settings.SftpServer),
                    new XElement("UserName", settings.SftpUserName),
                    new XElement("Password", SftpSecuredData.Encrypt(settings.SftpPassword)),
                    new XElement("RootDirectory", settings.SftpRootDirectory));
                UpdateElement("SftpSettings", sftpGroup);

                // Write Check Modules settings
                XElement checkModulesGroup = new XElement("CheckModulesSettings",
                    new XElement("IntensityModule", settings.CheckModuleIntensityActive),
                    new XElement("PausesModule", settings.CheckModulePausesActive),
                    new XElement("AutomaticSkip", settings.CheckModuleAutomaticSkip));
                UpdateElement("CheckModulesSettings", checkModulesGroup);

                // Write Application settings
                XElement applicationGroup = new XElement("ApplicationSettings",
                    new XElement("PhraseFontSize", settings.AppPhraseFontSize),
                    new XElement("ShowStartupHelp", settings.AppShowStartupHelp),
                    new XElement("ButtonFontSize", settings.AppButtonFontSize),
                    new XElement("NavigationButtonSize", settings.AppNavigationButtonSize),
                    new XElement("RecorderButtonsSize", settings.AppRecorderButtonsSize),
                    new XElement("PlayerButtonSize", settings.AppPlayerButtonSize),
                    new XElement("AutomaticUploadToSftp", settings.AppAutomaticUploadToSftp),
                    new XElement("AutomaticMoveToFollowing", settings.AppAutomaticMoveToFollowing)
                    );
                UpdateElement("ApplicationSettings", applicationGroup);

                Save();
            }
        }

        /// <summary>
        /// Load data stored in settings XML file as UserSettings object
        /// </summary>
        /// <returns>UserSettings encapsulation object</returns>
        public UserSettings LoadData()
        {
            UserSettings userSettings = new UserSettings();

            /*if (!XmlDocument.Root.HasElements)
                return userSettings;*/

            // Capture devices
            IEnumerable<MMDevice> activeDevices = DeviceProvider.GetCaptureDevices();
            if (XmlDocument.Root.Descendants("InputDeviceId").Count() > 0)
            {
                // Check whether input device from settings still exist
                var storedId = XmlDocument.Root.Descendants("InputDeviceId").First().Value;
                var captureDevice = new MMDeviceEnumerator().EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.All)?.Where(d => d.ID == storedId);
                if (captureDevice?.Any() != null && captureDevice.Any())
                {
                    if (!captureDevice.Where(d => d.State == DeviceState.Active).Any())
                    {
                        //userSettings.CaptureDevice = activeDevices.First();
                        CaptureDeviceNotActive?.Invoke(this, new DeviceSettingsEventArgs(captureDevice?.First(), activeDevices.First(), DeviceSettingsState.SetButNotActive));
                    }
                    else
                    {
                        userSettings.CaptureDevice = captureDevice.First();
                    }
                }
                else
                {
                    // There is no device with this ID
                    //userSettings.CaptureDevice = activeDevices?.First();
                    CaptureDeviceNotActive?.Invoke(this, new DeviceSettingsEventArgs(null, activeDevices?.First(), DeviceSettingsState.SetButNotExists));
                }
            }
            else
            {
                // In case that there are no input device set, use the first active one
                userSettings.CaptureDevice = activeDevices?.First();
                CaptureDeviceNotActive?.Invoke(this, new DeviceSettingsEventArgs(null, activeDevices?.First(), DeviceSettingsState.NotSetYet));
            }

            // Render devices
            activeDevices = DeviceProvider.GetRenderDevices();
            if (XmlDocument.Root.Descendants("OutputDeviceId").Count() > 0)
            {
                // Check whether input device from settings still exist
                var storedId = XmlDocument.Root.Descendants("OutputDeviceId").First().Value;
                var renderDevice = new MMDeviceEnumerator().EnumerateAudioEndPoints(DataFlow.Render, DeviceState.All)?.Where(d => d.ID == storedId);
                if (renderDevice?.Any() != null && renderDevice.Any())
                {
                    if (!renderDevice.Where(d => d.State == DeviceState.Active).Any())
                    {
                        userSettings.RenderDevice = activeDevices.First();
                        RenderDeviceNotActive?.Invoke(this, new DeviceSettingsEventArgs(renderDevice.First(), activeDevices.First(), DeviceSettingsState.SetButNotActive));
                    }              
                    else
                    {
                        userSettings.RenderDevice = renderDevice.First();
                    }
                }
                {
                    // There is no device with this ID
                    if (userSettings.RenderDevice == null)
                    {
                        userSettings.RenderDevice = activeDevices?.First();
                        RenderDeviceNotActive?.Invoke(this, new DeviceSettingsEventArgs(null, activeDevices?.First(), DeviceSettingsState.SetButNotExists));
                    }
                }
            }
            else
            {
                // In case that there are no input device set, use the first active one
                userSettings.RenderDevice = activeDevices?.First();
                RenderDeviceNotActive?.Invoke(this, new DeviceSettingsEventArgs(null, activeDevices?.First(), DeviceSettingsState.NotSetYet));
            }

            // Voice Checker Settings
#if VOICECHECKER
            userSettings.VoiceCheckerActive = ReadElement(XmlDocument.Root, "IsActive", DefaultSettings.VoiceChecker.IsActive, bool.Parse);
            userSettings.VoiceCheckerInterval = ReadElement(XmlDocument.Root, "Interval", DefaultSettings.VoiceChecker.Interval, int.Parse);
            userSettings.VoiceCheckerHistory = ReadElement(XmlDocument.Root, "History", DefaultSettings.VoiceChecker.History, int.Parse);
            userSettings.VoiceCheckerHideInstructions = ReadElement(XmlDocument.Root, "HideInstructions", DefaultSettings.VoiceChecker.IsStartupHelpEnabled, bool.Parse);
#endif

            // SFTP Settings
            userSettings.SftpServer = ReadElement(XmlDocument.Root, "Server", DefaultSettings.Sftp.Server, Convert.ToString);
            userSettings.SftpUserName = ReadElement(XmlDocument.Root, "UserName", DefaultSettings.Sftp.Username, Convert.ToString);
            userSettings.SftpPassword = ReadElement(XmlDocument.Root, "Password", DefaultSettings.Sftp.Password, SftpSecuredData.Decrypt);
            userSettings.SftpRootDirectory = ReadElement(XmlDocument.Root, "RootDirectory", DefaultSettings.Sftp.RootDir, Convert.ToString);

            // Check Modules Settings
            userSettings.CheckModuleIntensityActive = ReadElement(XmlDocument.Root, "IntensityModule", DefaultSettings.CheckModules.IsIntensityModuleActive, bool.Parse);
            userSettings.CheckModulePausesActive = ReadElement(XmlDocument.Root, "PausesModule", DefaultSettings.CheckModules.IsPausesModuleActive, bool.Parse);
            userSettings.CheckModuleAutomaticSkip = ReadElement(XmlDocument.Root, "AutomaticSkip", DefaultSettings.CheckModules.IsAutomaticSkipEnabled, bool.Parse);

            // Application settings          
            userSettings.AppShowStartupHelp = ReadElement(XmlDocument.Root, "ShowStartupHelp", DefaultSettings.Application.IsStartupHelpEnabled, bool.Parse);
            userSettings.AppPhraseFontSize = ReadElement(XmlDocument.Root, "PhraseFontSize", DefaultSettings.Application.PhraseFontSize, double.Parse);
            userSettings.AppButtonFontSize = ReadElement(XmlDocument.Root, "ButtonFontSize", DefaultSettings.Application.ButtonsFontSize, double.Parse);
            userSettings.AppNavigationButtonSize = ReadElement(XmlDocument.Root, "NavigationButtonSize", DefaultSettings.Application.NavigationButtonsSize, double.Parse);
            userSettings.AppRecorderButtonsSize = ReadElement(XmlDocument.Root, "RecorderButtonsSize", DefaultSettings.Application.RecorderButtonsSize, double.Parse);
            userSettings.AppPlayerButtonSize = ReadElement(XmlDocument.Root, "PlayerButtonSize", DefaultSettings.Application.PlayerButtonsSize, double.Parse);
            userSettings.AppAutomaticUploadToSftp = ReadElement(XmlDocument.Root, "AutomaticUploadToSftp", DefaultSettings.Application.IsAutomaticUploadToSftpEnabled, bool.Parse);
            userSettings.AppAutomaticMoveToFollowing = ReadElement(XmlDocument.Root, "AutomaticMoveToFollowing", DefaultSettings.Application.IsAutomaticMoveToFollowingEnabled, bool.Parse);

            return userSettings;
        }

        /// <summary>
        /// Read XML element given by XName returning default generic value "T" and applying converting generic function to convert
        /// </summary>
        /// <typeparam name="T">Generic data type</typeparam>
        /// <param name="root">Root XML element</param>
        /// <param name="xname">XML node name</param>
        /// <param name="defaultValue">Default value</param>
        /// <param name="func">Converting function</param>
        /// <returns></returns>
        public static T ReadElement<T>(XElement root, string xname, T defaultValue, Func<string, T> func=null)
        {
            if (func == null)
                return defaultValue;
            var descendants = root.Descendants(xname);
            if (descendants.Any())
            {
                return func(descendants.First().Value);
            }
            else
                return defaultValue;
        }

        /// <summary>
        /// Read element from XML root and return its value
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        public string ReadElement(string xName)
        {
            return XmlDocument.Root.Descendants(xName)?.FirstOrDefault()?.Value;
        }

        /// <summary>
        /// Save changes into the XML
        /// </summary>
        public void Save() { XmlDocument.Save(fileInfo.FullName); }

        /// <summary>
        /// Add XML element to the root of loaded XML
        /// </summary>
        /// <param name="element">XML element</param>
        public void AddToRoot(XElement element) { XmlDocument.Root.Add(element); }

        /// <summary>
        /// Update specific XML element based on XName with given XML element
        /// </summary>
        /// <param name="name">Name of element to look for</param>
        /// <param name="element">XML element to replace with</param>
        private void UpdateElement(XName name, XElement element)
        {
            if (XmlDocument.Root.Element(name) == null)
                AddToRoot(element);
            else
                XmlDocument.Root.Element(name).ReplaceWith(element);
        }

        /// <summary>
        /// Flag indicating whether capture device set in settings is currently used in OS
        /// </summary>
        /// <param name="captureDevice">Capture device as WASAPI MMDevice</param>
        /// <returns></returns>
        public bool? IsUsingSetCaptureDevice(MMDevice captureDevice)
        {
            var savedCaptureDevice = XmlDocument.Root.Descendants("InputDeviceId");

            if (savedCaptureDevice.Any())
            {
                return captureDevice.ID == savedCaptureDevice.First().Value.Trim();
            }
            else
                return null;
        }
    }

    /// <summary>
    /// Class for DeviceSettings event arguments
    /// </summary>
    public class DeviceSettingsEventArgs
    {
        /// <summary>
        /// Device actually set
        /// </summary>
        public MMDevice DeviceSet { get; set; }
        /// <summary>
        /// Default device
        /// </summary>
        public MMDevice DeviceDefault { get; set; }
        /// <summary>
        /// Device settings state enumeration value
        /// </summary>
        public DeviceSettingsState State { get; set; }

        /// <summary>
        /// Create a new instance of device settings event arguments
        /// </summary>
        /// <param name="deviceSet">Device set</param>
        /// <param name="deviceDefault">Default device</param>
        /// <param name="state">Device settings state enum value</param>
        public DeviceSettingsEventArgs(MMDevice deviceSet, MMDevice deviceDefault, DeviceSettingsState state)
        {
            DeviceSet = deviceSet;
            DeviceDefault = deviceDefault;
            State = state;
        }
    }

    /// <summary>
    /// State of device in OS which is set in settings to be used
    /// </summary>
    public enum DeviceSettingsState
    {
        NotSetYet,
        SetButNotActive,
        SetButNotExists
    }
}
