﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

using Tlachapoud.Core.Data;
using Tlachapoud.Core.Audio.CheckModules;
using System.Reflection;

namespace Tlachapoud.Core.IO
{
    /// <summary>
    /// Class representing reader and writer for phrases XML file
    /// </summary>
    public class PhrasesFile
    {
        /// <summary>
        /// Root element name
        /// </summary>
        private const string ROOT = "phrases";
        /// <summary>
        /// Phrases file FileInfo object
        /// </summary>
        private readonly FileInfo fileInfo;
        /// <summary>
        /// XML document Linq object
        /// </summary>
        public XDocument XmlDocument { get; set; }
        /// <summary>
        /// Data stored in Phrases XML file as ienumerable object of Phrase objects
        /// </summary>
        public IEnumerable<Phrase> Data { get; private set; }

        private int token = 0;

        /// <summary>
        /// Create a new instance referring to phrases XML file (or creating a new one)
        /// </summary>
        /// <param name="path"></param>
        public PhrasesFile(string path)
        {
            fileInfo = new FileInfo(path);
            Load();

            // Update old phrases structure
            var phrasesToUpdate = XmlDocument.Descendants("phrase").Where(el => el.Element("ortho") == null);
            foreach (var phraseElement in phrasesToUpdate)
            {
                // ortho is implemented as an attribute
                if (phraseElement.Attributes("ortho").Any())
                {
                    phraseElement.Add(new XElement("ortho", phraseElement.Attribute("ortho").Value));
                    phraseElement.Attribute("ortho").Remove();
                }
                // Text is implemented as a value of Phrase element
                else if (!string.IsNullOrEmpty(phraseElement.Value))
                {
                    var value = phraseElement.Value;
                    phraseElement.SetValue(string.Empty);
                    phraseElement.Add(new XElement("ortho", value));
                }
            }
            Save();

            Data = LoadData();

            // If there are some phrases without WantFName, generate a new one for them
            var phrasesWithoutWantFName = Data.Where(n => n.WantFName == null);
            if (phrasesWithoutWantFName.Count() > 0)
            {
                // Generate a Queue of generated WantFNames
                Queue<string> generatedWantFNames = new Queue<string>();
                IEnumerable<string> alreadyUsedWantFNames = Data.Where(n => n.WantFName != null).Select(n => n.WantFName);
                for (int i = 1; i < phrasesWithoutWantFName.Count()*2; i++)
                {
                    generatedWantFNames.Enqueue(string.Format("record{0}", i.ToString("0000")));
                }
                var newWantFNames = new Queue<string>(generatedWantFNames.Except(alreadyUsedWantFNames));

                // Add WantFName to phrases and save it
                foreach (var item in phrasesWithoutWantFName)
                {
                    var phraseToUpdate = Data.Where(p => p.Id == item.Id).First();
                    phraseToUpdate.WantFName = newWantFNames.Dequeue();
                    Update(phraseToUpdate);
                }
                Save();
            }
        }

        /// <summary>
        /// Load phrases XML file or create a new one if does not exist
        /// </summary>
        public void Load()
        {
            if (!fileInfo.Exists)
            {
                Directory.CreateDirectory(fileInfo.DirectoryName);

                if (File.Exists(ContentDataFile.PhrasesFilePath))
                    File.Copy(ContentDataFile.PhrasesFilePath, fileInfo.FullName);
                else
                {
                    var assembly = Assembly.GetExecutingAssembly();
                    using (var xmlStream = assembly.GetManifestResourceStream("Tlachapoud.Resources.laryngo.v9.wantfname.xml"))
                    {
                        XDocument xmlPhrases = XDocument.Load(xmlStream);
                        xmlPhrases.Save(fileInfo.FullName);
                    }
                }
            }
            else
            {
                XmlDocument = XDocument.Load(fileInfo.FullName);
            }
        }

        /// <summary>
        /// Load data stored in phrases XML file as Phrase objects
        /// </summary>
        /// <returns>IEnumerable of Phrase objects</returns>
        public IEnumerable<Phrase> LoadData()
        {
            int id = 0;
            foreach (var element in XmlDocument.Root.Descendants("phrase"))
            {
                yield return ReadElementData(element, id);
                id++;
            }
        }

        /// <summary>
        /// Total number of phrases in loaded XML file
        /// </summary>
        public int TotalPhrases
        {
            get { return XmlDocument.Root.Descendants("phrase").Count(); }
        }

        /// <summary>
        /// Number of successfully recorded phrases in loaded XML file
        /// </summary>
        public int RecordedPhrases
        {
            get { return XmlDocument.Root.Descendants("phrase").Where(d => d.Attribute("State") != null).Count(); }
        }

        /// <summary>
        /// Flag indicating whether recording is finished or not
        /// </summary>
        public bool IsFinished
        {
            get { return XmlDocument.Root.Descendants("phrase").Where(d => d.Attribute("State") == null).Count() == 0; }
        }

        /// <summary>
        /// Save changes to loaded XML file
        /// </summary>
        public void Save() { XmlDocument.Save(fileInfo.FullName); }

        /// <summary>
        /// Add given XElement to XML document root
        /// </summary>
        /// <param name="element">XElement object</param>
        public void AddToRoot(XElement element) { XmlDocument.Root.Add(element); }

        /// <summary>
        /// Read data of a given XML element at specific index
        /// </summary>
        /// <param name="element">XML element</param>
        /// <param name="id">Specific index</param>
        /// <returns></returns>
        private Phrase ReadElementData(XElement element, int id)
        {
            // Process Text attribute
            Phrase phrase = new Phrase(id, element.Element("ortho").Value);

            // Process WantFName attribute
            if (element.Attribute("ID") != null)
                phrase.WantFName = element.Attribute("ID").Value;

            // Process State attribute
            if (element.Attribute("State") != null)
                phrase.State = (PhraseState)Enum.Parse(typeof(PhraseState), element.Attribute("State").Value);
            else
                phrase.State = PhraseState.Unrecorded;

            // Process RecordedFiles element
            if (element.Element("RecordedFiles") != null)
            {
                var recordedFiles = from n in element.Element("RecordedFiles").Descendants()
                                    select new WaveFile(
                                        new FileInfo(Path.Combine(fileInfo.Directory.FullName, ContentDataFile.WavesApprovedFolderPath, n.Attribute("Name").Value.AddExtension("wav"))),
                                        ulong.Parse(n.Attribute("RecordTime").Value),
                                        TimeSpan.Parse(n.Attribute("Duration").Value),
                                        CheckModuleResult.Approved,
                                        (WaveFileTag)Enum.Parse(typeof(WaveFileTag), n.Attribute("Tag").Value),
                                        n.Attribute("State").Value.Split(',').Select(s => (CheckModuleState)Enum.Parse(typeof(CheckModuleState), s)),
                                        n.Attribute("CaptureDeviceId")?.Value
                                        );
                phrase.FNames = recordedFiles.ToList();
            }

            // Process RejectedFiles element
            if (element.Element("RejectedFiles") != null)
            {
                var recordedFiles = from n in element.Element("RejectedFiles").Descendants()
                                    select new WaveFile(
                                        new FileInfo(Path.Combine(fileInfo.Directory.FullName, ContentDataFile.WavesRejectedFolderPath, n.Attribute("Name").Value.AddExtension("wav"))),
                                        ulong.Parse(n.Attribute("RecordTime").Value),
                                        TimeSpan.Parse(n.Attribute("Duration").Value),
                                        CheckModuleResult.Rejected,
                                        (WaveFileTag)Enum.Parse(typeof(WaveFileTag), n.Attribute("Tag").Value),
                                        n.Attribute("State").Value.Split(',').Select(s => (CheckModuleState)Enum.Parse(typeof(CheckModuleState), s)),
                                        n.Attribute("CaptureDeviceId")?.Value
                                        );
                phrase.Rejected = recordedFiles.ToList();
            }

            return phrase;
        }

        /// <summary>
        /// Set moving token to the first unrecorded phrase in loaded XML file
        /// </summary>
        public void SetTokenToFirstUnrecorded()
        {
            token = FirstUnrecordedId;
        }

        /// <summary>
        /// Get ID of first unrecorded phrase in loaded XML file
        /// </summary>
        public int FirstUnrecordedId { get { return GetFirstUnrecorded().Id; } }

        /// <summary>
        /// Get first phrase
        /// </summary>
        /// <returns>Phrase object</returns>
        public Phrase GetFirst()
        {
            token = 0;
            return Data.First();
        }

        /// <summary>
        /// Get last phrase
        /// </summary>
        /// <returns>Phrase object</returns>
        public Phrase GetLast()
        {
            var phrase = GetFirstUnrecorded();
            token = phrase.Id;
            return phrase;
        }

        /// <summary>
        /// Get first unrecorded phrase
        /// </summary>
        /// <returns>Phrase object</returns>
        public Phrase GetFirstUnrecorded()
        {
            var result = Data.Where(p => p.State == PhraseState.Unrecorded);
            if (result.Any())
                return result.First();
            else
                return Data.Last();
        }

        /// <summary>
        /// Get random phrase in history limited with max. search depth
        /// </summary>
        /// <param name="maxHistory">Max. history to look ahead</param>
        /// <returns>Phrase object</returns>
        public Phrase GetByHistory(int maxHistory)
        {
            ulong threshold = (ulong)maxHistory * 86400L;
            ulong epochTime = DateTime.Now.GetEpochTime();
            var candidates = Data.Where(p => p.State == PhraseState.Recorded && (epochTime - p.FNames.First().RecordTime) <= threshold);
            
            if (candidates.Any())
            {
                var rand = new Random();
                return candidates.ElementAt(rand.Next(candidates.Count()));
            }
            else
            {
                candidates = Data.Where(p => p.State == PhraseState.Recorded);
                var rand = new Random();
                return candidates.ElementAt(rand.Next(candidates.Count()));
            }
        }

        /// <summary>
        /// Flag indicating whether phrase with token is n-th phrase recorded today
        /// </summary>
        /// <param name="n">Threshold resulting in true if user recorded N phrases today</param>
        /// <returns>Boolean flag</returns>
        public bool IsNthPhraseToday(int n)
        {
            ulong todayEpochTime = DateTime.Today.GetEpochTime();
            var recordedToday = Data.Where(p => p.State == PhraseState.Recorded && (p.FNames.First().RecordTime > todayEpochTime));
            return (recordedToday.Count() % n) == 0;
        }

        /// <summary>
        /// Get number of phrases recorded today
        /// </summary>
        /// <returns>Number of phrases recorded today</returns>
        public int GetNumRecordedToday()
        {
            ulong todayEpochTime = DateTime.Today.GetEpochTime();
            return Data.Where(p => p.State == PhraseState.Recorded && (p.FNames.First().RecordTime > todayEpochTime)).Count();
        }

        /// <summary>
        /// Move token to the previous phrase
        /// </summary>
        /// <returns>Phrase object</returns>
        public Phrase GoPrevious()
        {
            if (CanGoPrevious)
            {
                return Data.ElementAt(--token);
            }
            return null;
        }

        /// <summary>
        /// Get a previous phrase
        /// </summary>
        /// <returns>Phrase object</returns>
        public Phrase GetPrevious()
        {
            if (CanGoPrevious)
                return Data.ElementAt(token - 1);
            else
                return null;
        }

        /// <summary>
        /// Move token to the following phrase
        /// </summary>
        /// <returns>Phrase object</returns>
        public Phrase GoFollowing()
        {
            if (CanGoFollowing)
            {
                return Data.ElementAt(++token);
            }
            return null;
        }

        /// <summary>
        /// Flag indicating whether it is possible to go to the previous phrase
        /// </summary>
        public bool CanGoPrevious
        {
            get { return token > 0; }
        }

        /// <summary>
        /// Flag indicating whether it is possible to go to the following phrase
        /// </summary>
        public bool CanGoFollowing
        {
            get { return token < FirstUnrecordedId; }
        }

        /// <summary>
        /// Flag indicating whether it is possible to go to the previous skipped phrase
        /// </summary>
        public bool CanGoPreviousSkipped
        {
            get
            {
                return Data.Where(p => p.State == PhraseState.Skipped && p.Id < token).Any();
            }
        }

        /// <summary>
        /// Flag indicating whether it is possible to go to the following skipped phrase
        /// </summary>
        public bool CanGoFollowingSkipped
        {
            get
            {
                return Data.Where(p => p.State == PhraseState.Skipped && p.Id > token).Any();
            }
        }

        /// <summary>
        /// Move token to the previous skipped phrase 
        /// </summary>
        /// <returns>Phrase object</returns>
        public Phrase GoPreviousSkipped()
        {
            if (CanGoPreviousSkipped)
            {
                var result = Data.Where(p => p.State == PhraseState.Skipped && p.Id < token).Last();
                token = result.Id;
                return result;
            }
            return null;
        }

        /// <summary>
        /// Move token to the following skipped phrase 
        /// </summary>
        /// <returns>Phrase object</returns>
        public Phrase GoFollowingSkipped()
        {
            if (CanGoFollowingSkipped)
            {
                var result = Data.Where(p => p.State == PhraseState.Skipped && p.Id > token).First();
                token = result.Id;
                return result;
            }
            return null;
        }

        /// <summary>
        /// Update phrase information stored in XML file, also add information about recorded wave file if argument is not null
        /// </summary>
        /// <param name="phrase">Phrase object (currently recorded phrase)</param>
        /// <param name="waveFile">Wave file storing information about recorded wave</param>
        public void Update(Phrase phrase, WaveFile waveFile=null)
        {
            var nodeToUpdate = XmlDocument.Root.Descendants("phrase").Where(n => n.Element("ortho").Value == phrase.Text).First();
            if (nodeToUpdate != null)
            {
                // Add State attribute
                if (phrase.State != PhraseState.Unrecorded)
                    nodeToUpdate.SetAttributeValue("State", phrase.State.ToString());

                // Update WantFName if it does not exist
                if (phrase.WantFName != null)
                    nodeToUpdate.SetAttributeValue("ID", phrase.WantFName);

                if (waveFile != null)
                {
                    var xname = waveFile.CheckResult == CheckModuleResult.Approved ? "RecordedFiles" : "RejectedFiles";
                    var recordedFileToChange = from n in nodeToUpdate.Descendants(xname).Descendants("File")
                                        where n.Attribute("Name").Value == waveFile.Name
                                        select n;

                    var updatedElement = new XElement("File",
                                new XAttribute("Name", waveFile.Name),
                                new XAttribute("RecordTime", waveFile.RecordTime),
                                new XAttribute("Duration", waveFile.Duration.ToString()),
                                new XAttribute("State", string.Join(",", waveFile.States)),
                                new XAttribute("Tag", waveFile.Tag),
                                new XAttribute("CaptureDeviceId", waveFile.CaptureDeviceId)
                            );

                    if (recordedFileToChange.Any())
                    {
                        // Update already existing File element
                        recordedFileToChange.First().ReplaceWith(updatedElement);
                    }
                    else
                    {
                        // Add new File element
                        var filesContainer = nodeToUpdate.Descendants(xname);
                        if (filesContainer == null || filesContainer.Count() == 0)
                            nodeToUpdate.Add(new XElement(xname));

                        nodeToUpdate.Descendants(xname).First().Add(updatedElement);
                    }
                          
                }
                Save();
            }
        }

        /// <summary>
        /// Merge two phrases XML files
        /// </summary>
        /// <param name="phrasesFileToMerge">Phrases file which should be merged into this loaded XML</param>
        public void Merge(PhrasesFile phrasesFileToMerge)
        {
            foreach (var phrase in phrasesFileToMerge.Data)
            {
                // If there is no Phrase with the same Text and WantFName, merge
                var duplicates = Data.Where(p => p.Text == phrase.Text && p.WantFName == phrase.WantFName);
                if (!duplicates.Any())
                {
                    if (!string.IsNullOrEmpty(phrase.WantFName))
                        AddToRoot(new XElement("phrase", 
                            new XAttribute("ID", phrase.WantFName), 
                            new XElement("ortho", phrase.Text)));
                    else
                        AddToRoot(new XElement("phrase", new XElement("ortho", phrase.Text)));
                }
            }
            Save();
            Data = LoadData();
        }

        /// <summary>
        /// Update qualification tag in a specific wave file information stored in XML
        /// </summary>
        /// <param name="waveFileInfo"></param>
        public void UpdateTag(WaveFile waveFileInfo)
        {
            var nodeToUpdate = XmlDocument.Root.Descendants("File").Where(n => n.Attribute("Name").Value == waveFileInfo.Name).First();
            if (nodeToUpdate != null)
            {
                nodeToUpdate.SetAttributeValue("Tag", waveFileInfo.Tag);
            }
            Save();
        }
    }
}
