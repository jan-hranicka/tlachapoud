﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

namespace Tlachapoud.Core.IO
{
    public abstract class DataFile<TObj, TEnum>
    {
        public FileInfo Info { get; set; }
        public string RootName { get; set; }
        public XDocument XmlDocument { get; set; }
        public abstract IEnumerable<TObj> Data { get; protected set; }

        public DataFile(string path, string rootName)
        {
            Info = new FileInfo(path);
            RootName = rootName;
        }

        // Abstract methods
        public abstract void WriteData(TObj obj);
        public abstract bool Exists(TObj obj, TEnum value);
        public abstract void UpdateData(TObj obj, TEnum elem);
        public abstract IEnumerable<TObj> LoadData();

        public void Load()
        {
            if (!Info.Exists)
            {
                Directory.CreateDirectory(Info.DirectoryName);

                XmlDocument = new XDocument(new XElement(RootName));
                XmlDocument.Save(Info.FullName);
            }
            else
            {
                XmlDocument = XDocument.Load(Info.FullName);
            }
        }

        public void Save() { XmlDocument.Save(Info.FullName); }

        public void AddToRoot(XElement element) { XmlDocument.Root.Add(element); }
    }
}
