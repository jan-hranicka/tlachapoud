﻿using System.Collections.Generic;
using System.Xml.Linq;

// Import tlachapoud Core library
using Tlachapoud.Core.Audio.CheckModules;

namespace Tlachapoud.Core.IO
{
    /// <summary>
    /// Class for loading data from and saving data to a CheckModule configuration file (XML-based)
    /// </summary>
    public class CheckModulesConfigFile
    {
        // Root element name
        private const string ROOT = "CheckModulesConfiguration";

        /// <summary>
        /// Load XML-based file containig configuration data of CheckModules
        /// </summary>
        /// <param name="path">Full path of a file</param>
        /// <returns>Enumerable collection of CheckModuleConfig objects</returns>
        public static IEnumerable<CheckModuleConfig> Load(string path)
        {
            var XmlDocument = XDocument.Load(path);
            var checkModulesConfig = XmlDocument.Root.Descendants("CheckModule");
            foreach (var module in checkModulesConfig)
            {
                CheckModuleConfig moduleConfig = new CheckModuleConfig();
                foreach (var attr in module.Attributes())
                {
                    moduleConfig.Add(attr.Name.ToString(), attr.Value);                 
                }
                yield return moduleConfig;
            }
        }
    }
}
