﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Xml.Linq;

using Tlachapoud.Core.Data;

namespace Tlachapoud.Core.IO
{
    public class UsersFile : DataFile<UserProfile, UserProfileData>
    {
        public override IEnumerable<UserProfile> Data { get; protected set; }

        /// <summary>
        /// Create or load data file with user profiles
        /// </summary>
        /// <param name="path">Path of a file with stored user profiles data</param>
        public UsersFile(string path) : base(path, "Users")
        {
            Load();
            Data = LoadData();
        }

        /// <summary>
        /// Write data to loaded file
        /// </summary>
        /// <param name="data"></param>
        public override void WriteData(UserProfile userProfile)
        {
            if (userProfile != null)
            {
                XElement userElement = new XElement("User",
                    new XElement(UserProfileData.FirstName.ToString(), userProfile.FirstName),
                    new XElement(UserProfileData.LastName.ToString(), userProfile.LastName),
                    new XElement(UserProfileData.Degree.ToString(), userProfile.Degree),
                    new XElement(UserProfileData.Email.ToString(), userProfile.Email),
                    new XElement(UserProfileData.Gender.ToString(), userProfile.Gender.ToString()),
                    new XElement(UserProfileData.YearOfBirth.ToString(), userProfile.YearOfBirth.ToString()),
                    new XElement(UserProfileData.PhoneNumber.ToString(), userProfile.PhoneNumber),
                    new XElement(UserProfileData.Password.ToString(), userProfile.Password),
                    new XElement(UserProfileData.Nickname.ToString(), userProfile.Nickname),
                    new XElement(UserProfileData.IsUsedToSpeakCoherently.ToString(), userProfile.IsUsedToSpeakCoherently),
                    new XElement(UserProfileData.HasRegionalAccent.ToString(), userProfile.HasRegionalAccent),
                    new XElement(UserProfileData.RegionalAccentKind.ToString(), userProfile.RegionalAccentKind),
                    new XElement(UserProfileData.RegistrationDate.ToString(), DateTime.Now.ToString()),
                    new XElement(UserProfileData.EnvironmentUserName.ToString(), userProfile.EnvironmentUserName),
                    new XElement(UserProfileData.Guid.ToString(), Guid.NewGuid().ToString())
                );
                AddToRoot(userElement);
            }
        }

        /// <summary>
        /// Check whether user profile already exists in file where are user profiles stored
        /// </summary>
        /// <param name="user">User profile for check</param>
        /// <param name="value">Value for comparison</param>
        /// <returns></returns>
        public override bool Exists(UserProfile user, UserProfileData value)
        {
            if (Data == null)
                throw new NullReferenceException("No data found! Please, use Load and LoadData method before calling Exists method.");

            return Data.Where(u => u[value] == user[value]).Count() > 0;
        }

        /// <summary>
        /// Select user profile by given key
        /// </summary>
        /// <param name="user"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public UserProfile Select(UserProfile user, UserProfileData value)
        {
            var profiles = Data.Where(u => u[value] == user[value]);
            if (profiles.Count() > 0)
                return profiles.First();
            else
                return null;
        }

        public UserProfile Select(string str, UserProfileData by)
        {
            var profiles = Data.Where(u => u[by] == str);
            if (profiles.Count() > 0)
                return profiles.First();
            else
                return null;
        }

        /// <summary>
        /// Load all user profiles stored in data file as an enumerable collection of user profiles
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<UserProfile> LoadData()
        {
            if (XmlDocument == null)
                throw new NullReferenceException("XML document not loaded. Please, use Load method before calling LoadData method.");

            // Load data
            var allUsers = from n in XmlDocument.Root.Descendants("User")
                           select new UserProfile
                           {
                               FirstName = n.ReadAndConvert(UserProfileData.FirstName, Convert.ToString),
                               LastName = n.ReadAndConvert(UserProfileData.LastName, Convert.ToString),
                               Email = n.ReadAndConvert(UserProfileData.Email, Convert.ToString),
                               Gender = (Gender)Enum.Parse(typeof(Gender), (string)n.Element(UserProfileData.Gender.ToString())),
                               Nickname = n.ReadAndConvert(UserProfileData.Nickname, Convert.ToString),
                               YearOfBirth = n.ReadAndConvert(UserProfileData.YearOfBirth, TypeConversions.ToNullableInt32),
                               Degree = n.ReadAndConvert(UserProfileData.Degree, Convert.ToString),
                               IsUsedToSpeakCoherently = n.ReadAndConvert(UserProfileData.IsUsedToSpeakCoherently, Convert.ToBoolean),
                               PhoneNumber = n.ReadAndConvert(UserProfileData.PhoneNumber, Convert.ToString),
                               HasRegionalAccent = n.ReadAndConvert(UserProfileData.HasRegionalAccent, Convert.ToBoolean),
                               RegionalAccentKind = n.ReadAndConvert(UserProfileData.RegionalAccentKind, Convert.ToString),
                               RegistrationDate = n.ReadAndConvert(UserProfileData.RegistrationDate, Convert.ToDateTime),
                               EnvironmentUserName = n.ReadAndConvert(UserProfileData.EnvironmentUserName, Convert.ToString),
                               Guid = n.Element(UserProfileData.Guid.ToString())?.Value ?? null
                           };

            // Set password
            foreach (var user in allUsers)
            {
                SecureString password = XmlDocument.Root.Descendants("User").Where(
                    n => n.Element(UserProfileData.Email.ToString()).Value == user.Email).Select(
                    n => n.Element(UserProfileData.Password.ToString()).Value).First().ToSecureString();
                user.SetPassword(password, false);
                user.SetGuid();               

                yield return user;
            }
        }

        /// <summary>
        /// Update user profile in data file
        /// </summary>
        /// <param name="userProfile"></param>
        public override void UpdateData(UserProfile userProfile, UserProfileData elem)
        {
            if (userProfile != null)
            {
                var users = from item in XmlDocument.Descendants("User")
                            where item.Element(UserProfileData.Email.ToString()).Value == userProfile.Email
                            select item;

                var user = users.FirstOrDefault();
                if (!user.Descendants(elem.ToString()).Any())
                {
                    user.Add(new XElement(elem.ToString(), userProfile[elem].ToString()));
                }
                else
                {
                    user.Element(elem.ToString()).SetValue(userProfile[elem].ToString());
                }
            }
        }
    }

    /// <summary>
    /// Enumeration containing personal records for user profile
    /// </summary>
    public enum UserProfileData
    {
        FirstName,
        LastName,
        Nickname,
        Degree,
        Gender,
        YearOfBirth,
        PhoneNumber,
        Password,
        Email,
        RegistrationDate,
        HasRegionalAccent,
        RegionalAccentKind,
        IsUsedToSpeakCoherently,
        EnvironmentUserName,
        Guid
    }
}
