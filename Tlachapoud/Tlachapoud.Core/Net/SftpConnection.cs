﻿using System;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Linq;

namespace Tlachapoud.Core.Net
{
    /// <summary>
    /// SFTP Secured Data
    /// </summary>
    public static class SftpSecuredData
    {
        static readonly RSACryptoServiceProvider rsa;

        /// <summary>
        /// Static constructor for SFTP Secured Data
        /// </summary>
        static SftpSecuredData()
        {
            rsa = new RSACryptoServiceProvider();

            using (var xmlStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Tlachapoud.Core.Resources.private.key"))
            {
                XDocument xmlKey = XDocument.Load(xmlStream);
                rsa.FromXmlString(xmlKey.ToString());
            }
        }

        /// <summary>
        /// Encrypt input data with 1024bit RSA
        /// </summary>
        /// <param name="strText">Text to encrypt</param>
        /// <returns>Encrypted data as a string</returns>
        public static string Encrypt(string strText)
        {
            var testData = Encoding.UTF8.GetBytes(strText);

            try
            {
                var encryptedData = rsa.Encrypt(testData, true);
                var base64Encrypted = Convert.ToBase64String(encryptedData);
                return base64Encrypted;
            }
            finally
            {
                //rsa.PersistKeyInCsp = false;
            }

        }

        /// <summary>
        /// Decrypt input data with 1024bit RSA
        /// </summary>
        /// <param name="strText">Data to decrypt</param>
        /// <returns>Decrypted string</returns>
        public static string Decrypt(string strText)
        {
            var testData = Encoding.UTF8.GetBytes(strText);

            try
            {
                var base64Encrypted = strText;

                var resultBytes = Convert.FromBase64String(base64Encrypted);
                var decryptedBytes = rsa.Decrypt(resultBytes, true);
                var decryptedData = Encoding.UTF8.GetString(decryptedBytes);
                return decryptedData.ToString();
            }
            finally
            {
                //rsa.PersistKeyInCsp = false;
            }

        }
    }
}
