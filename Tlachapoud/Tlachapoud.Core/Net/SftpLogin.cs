﻿namespace Tlachapoud.Core.Net
{ 
    /// <summary>
    /// Container for SFTP server login data
    /// </summary>
    public struct SftpLogin
    {
        public const string HostAddress = "turing.kky.zcu.cz";
        public const string UserName = "phaserec_app";
        public const string Password = "TuringRecorderApp";
        public const string MainDir = "/Projekty/PhraseRecorder";
    }

    /// <summary>
    /// Container for SFTP HCENAT server login data
    /// </summary>
    public struct SftpLogin_HCENAT
    {
        public const string HostAddress = "turing.kky.zcu.cz";
        public const string UserName_Old = "hcenat_app";
        public const string Password_Old = "gF8sLm60sWqLZsJPak";
        public const string MainDir_Old = "/Projekty/HCENAT/PhraseRecorder";
    }
}
