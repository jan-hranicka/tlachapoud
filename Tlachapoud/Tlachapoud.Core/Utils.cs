﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;

namespace Tlachapoud.Core
{
    /// <summary>
    /// Static class providing some usefull utilities
    /// </summary>
    public static class Utilities
    {
        /// <summary>
        /// Flag determining whether Adobe Reader is installed on the current machine or not
        /// </summary>
        public static bool IsAdobeReaderInstalled
        {
            get
            {
                RegistryKey adobe = Registry.LocalMachine.OpenSubKey("Software").OpenSubKey("Adobe");
                if (adobe == null)
                {
                    var policies = Registry.LocalMachine.OpenSubKey("Software").OpenSubKey("Policies");
                    if (policies == null)
                        return false;
                    adobe = policies.OpenSubKey("Adobe");
                }
                if (adobe != null)
                {
                    RegistryKey acroRead = adobe.OpenSubKey("Acrobat Reader");
                    if (acroRead != null)
                    {
                        var x = acroRead.GetSubKeyNames();
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Open PDF file specified in parameter as a path with Adobe Reader if installed
        /// </summary>
        /// <param name="path">PDF document full path</param>
        public static void OpenWithAdobeReader(string path)
        {
            var adobe = Registry.LocalMachine.OpenSubKey("Software").OpenSubKey("Microsoft").OpenSubKey("Windows").OpenSubKey("CurrentVersion").OpenSubKey("App Paths").OpenSubKey("AcroRd32.exe");
            var readerPath = adobe.GetValue("");

            Process myProcess = new Process();
            myProcess.StartInfo.FileName = readerPath.ToString();
            myProcess.StartInfo.Arguments = string.Format("\"{0}\"", path);
            myProcess.Start();
        }

        /// <summary>
        /// Flag indicating whether the Internet is connected or not
        /// </summary>
        /// <returns></returns>
        public static bool IsInternetConnected
        {
            get
            {
                try
                {
                    Ping myPing = new Ping();
                    String host = "google.com";
                    byte[] buffer = new byte[32];
                    int timeout = 1000;
                    PingOptions pingOptions = new PingOptions();
                    PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                    if (reply.Status == IPStatus.Success)
                        return true;
                    else return false;
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Flag indicating whether the input text is numeric or not
        /// </summary>
        /// <param name="text">Input text</param>
        /// <returns>True if input text is numeric, otherwise not</returns>
        public static bool IsNumericInput(string text)
        {
            Regex regex = new Regex("[^0-9.-]+");
            return !regex.IsMatch(text);
        }
    }
}
