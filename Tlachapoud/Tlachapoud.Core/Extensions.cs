﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Xml.Linq;

namespace Tlachapoud.Core
{
    public static class Extensions
    {
        /// <summary>
        /// Convert source string into the SecureString object
        /// </summary>
        /// <param name="source">Source string for conversion</param>
        /// <returns></returns>
        public static SecureString ToSecureString(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
                return null;
            else
            {
                SecureString result = new SecureString();
                foreach (char c in source.ToCharArray())
                    result.AppendChar(c);
                return result;
            }
        }

        /// <summary>
        /// Add extension to a string
        /// </summary>
        /// <param name="source">Source string</param>
        /// <param name="extension">Extension string</param>
        /// <returns></returns>
        public static string AddExtension(this string source, string extension)
        {
            return string.Join(".", source, extension);
        }

        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        /// <summary>
        /// Get epoch time of given time
        /// </summary>
        /// <param name="tNow">DateTime object</param>
        /// <returns></returns>
        public static ulong GetEpochTime(this DateTime tNow)
        {
            TimeSpan span = (tNow - epoch);
            return (ulong)span.TotalSeconds;
        }

        /// <summary>
        /// Convert epoch time to DateTime object
        /// </summary>
        /// <param name="epochTime">Int32 object as Unix time</param>
        /// <returns></returns>
        public static DateTime FromUnixTime(this ulong epochTime)
        {
            return epoch.AddSeconds(epochTime);
        }

        /// <summary>
        /// Read and convert XML element into the object of specific generic data type T
        /// </summary>
        /// <typeparam name="T">Generic data type</typeparam>
        /// <typeparam name="TEnum">Enumeration value representing the name of XElement.XName</typeparam>
        /// <param name="element">XElement object</param>
        /// <param name="tEnum">Enumeration value</param>
        /// <param name="func">Generic function delegate</param>
        /// <returns></returns>
        public static T ReadAndConvert<T, TEnum>(this XElement element, TEnum tEnum, Func<string, T> func)
        {
            return func(element.Element(tEnum.ToString()).Value);
        }

        /// <summary>
        /// Do given action for each element of enumerable object depending on given predicate
        /// </summary>
        /// <typeparam name="T">Generic data type</typeparam>
        /// <param name="iEnum">Enumerable object</param>
        /// <param name="action">Action to do for each item of enumerable object</param>
        /// <param name="predicate">Predicate when to do the action</param>
        /// <returns></returns>
        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> iEnum, Action<T> action, Func<T, bool> predicate)
        {
            foreach (T item in iEnum)
            {
                if (predicate(item))
                    action(item);
                yield return item;
            }
        }
    }
   
    /// <summary>
    /// Class defining type conversions
    /// </summary>
    public sealed class TypeConversions
    {
        /// <summary>
        /// Convert string to nullable Int32
        /// </summary>
        /// <param name="s">String to convert</param>
        /// <returns>Nullable Int32</returns>
        public static int? ToNullableInt32(string s)
        {
            if (int.TryParse(s, out int i)) return i;
            return null;
        }
    }
}
