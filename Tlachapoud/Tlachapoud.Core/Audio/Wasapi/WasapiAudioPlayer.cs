﻿using System;
using System.IO;
using System.Collections.Generic;

// Import NAudio libraries
using NAudio.CoreAudioApi;
using NAudio.Wave;

namespace Tlachapoud.Core.Audio.Wasapi
{
    /// <summary>
    /// Audio Player object used for rendering audio data in Wave format
    /// </summary>
    public class AudioPlayer : IDisposable
    {
        #region NAudio Wave objects
        private WasapiOut soundOut;
        private WaveFileReader waveReader;
        private WaveFormat waveFormat;
        #endregion NAudio Wave objects

        WaveBuffer waveBuffer;

        /// <summary>
        /// Get Audio Player current state
        /// </summary>
        public AudioPlayerState State { get; private set; }

        /// <summary>
        /// Create a new Audio Player that use render device given by an input argument
        /// </summary>
        /// <param name="renderDevice"></param>
        public AudioPlayer(MMDevice renderDevice)
        {
            soundOut = new WasapiOut(renderDevice, AudioClientShareMode.Shared, false, 150);
            soundOut.PlaybackStopped += OnPlaybackStopped;
            waveFormat = renderDevice.AudioClient.MixFormat;

            State = AudioPlayerState.Stopped;
        }

        /// <summary>
        /// Load Wave file from string and initialize render device to play this Wave file
        /// </summary>
        /// <param name="path">Full path to a Wave file</param>
        public void Load(string path)
        {
            waveReader = new WaveFileReader(path);
            var provider = new MultiplexingWaveProvider(new List<IWaveProvider>() { waveReader }, 1);
            //soundOut.Init(provider);
            soundOut.Init(provider);

            byte[] bytes = new byte[waveReader.Length];
            waveReader.Read(bytes, 0, (int)waveReader.Length);
            waveBuffer = new WaveBuffer(bytes);
        }
    

    /// <summary>
    /// Load Wave file as a stream and initialize render device to play the Wave file stored in stream
    /// </summary>
    /// <param name="stream">Stream containing Wave file data</param>
    public void Load(Stream stream)
        {
            throw new NotImplementedException();
            /*waveReader = new AudioFileReader(stream);
            waveProvider = new MultiplexingWaveProvider(new List<IWaveProvider>() { waveReader }, soundOut.OutputWaveFormat.Channels);
            soundOut.Init(waveProvider);*/
        }

        /// <summary>
        /// Event handler when playback has stopped
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPlaybackStopped(object sender, StoppedEventArgs e)
        {
            
        }

        /// <summary>
        /// Stop rendering audio data
        /// </summary>
        public void Stop()
        {
            soundOut.Stop();
            State = AudioPlayerState.Stopped;
            //waveReader.Position = 0;            // Reset reader position
        }

        /// <summary>
        /// Pause rendering audio data
        /// </summary>
        public void Pause()
        {
            if (State == AudioPlayerState.Playing)
                soundOut.Pause();
            State = AudioPlayerState.Paused;
        }

        /// <summary>
        /// Start/Continue rendering audio data
        /// </summary>
        public void Play()
        {
            if (State == AudioPlayerState.Playing)
                Stop();
            soundOut.Play();
            State = AudioPlayerState.Playing;
        }

        /// <summary>
        /// Dispose object
        /// </summary>
        public void Dispose()
        {
            if (waveReader != null)
            {
                waveReader.Close();
                waveReader.Dispose();
            }
        }
    }
}
