﻿using System;
using System.IO;

// Import NAudio library clases
using NAudio.CoreAudioApi;
using NAudio.Wave;

// Import CorpusRecording library
using CorpusRecording;
using Tlachapoud.Core.Data;
using System.Collections.Generic;
using System.Linq;
using NAudio.Utils;

namespace Tlachapoud.Core.Audio.Wasapi
{
    public class AudioRecorder : IDisposable
    {
        #region NAudio Wave objects
        private IWaveIn waveIn;
        private WaveFormat waveFormat;
        private WaveFileWriter waveWriter;
        private MMDevice captureDevice;
        #endregion NAudio Wave objects

        /// <summary>
        /// Memory stream containing data recorded using capture device set in the class constructor
        /// </summary>
        //public MemoryStream memoryStream;

        private int notificationCount;
        private int count;
        public AudioRecorderState State { get; private set; }
        public ulong RecordTime { get; private set; }
        public TimeSpan Duration { get; private set; }

        public event EventHandler MaximumCalculated;

        /// <summary>
        /// Constructor create a new instance of Audio Recorder
        /// </summary>
        /// <param name="device">Capture device</param>
        public AudioRecorder(MMDevice device)
        {
            State = AudioRecorderState.Stopped;
            captureDevice = device;

            waveIn = new WasapiCapture(captureDevice);
            waveIn.DataAvailable += OnDataAvailable;
            waveIn.RecordingStopped += OnRecordingStopped;
        }

        /// <summary>
        /// Begin monitoring of actually set capture device activities
        /// </summary>
        public void BeginMonitoring()
        {
            if (State != AudioRecorderState.Stopped)
                return;
            
            waveIn = new WasapiCapture(captureDevice);
            waveIn.DataAvailable += OnDataAvailable;
            waveIn.RecordingStopped += OnRecordingStopped;
            waveFormat = captureDevice.AudioClient.MixFormat;
            notificationCount = waveFormat.SampleRate / 10;
            waveIn.StartRecording();

            State = AudioRecorderState.Monitoring;
            
        }

        /// <summary>
        /// Begin recording audio using actually set capture device
        /// </summary>
        public void BeginRecording()
        {
            if (State != AudioRecorderState.Stopped)
                return;

            notificationCount = 0;        

            //memoryStream = new MemoryStream();
            waveWriter = new WaveFileWriter(@"d:\Repositories\tlachapoud\Tlachapoud\Tlachapoud\bin\Debug\UserData\jan.hranicka@gmail.com\Komora\tmp_wasapi.wav", captureDevice.AudioClient.MixFormat);  
            waveIn.StartRecording();

            State = AudioRecorderState.Recording;

        }

        /// <summary>
        /// Stop recording audio
        /// </summary>
        public void StopRecording()
        {
            if (State == AudioRecorderState.Recording)
            {
                State = AudioRecorderState.RequestedStop;
                waveIn.StopRecording();
                Dispose();
            }
        }

        /// <summary>
        /// Stop monitoring capture device input
        /// </summary>
        public void StopMonitoring()
        {
            if (State == AudioRecorderState.Monitoring)
            {
                State = AudioRecorderState.RequestedStop;
                waveIn.StopRecording();
                Dispose();
            }
        }

        /// <summary>
        /// Event handler when recording has stopped
        /// </summary>
        /// <param name="sender">AudioRecorder object</param>
        /// <param name="e">Stopped Event Arguments</param>
        private void OnRecordingStopped(object sender, StoppedEventArgs e)
        {
            // Set RecordTime as EpochTime from Now
            RecordTime = DateTime.Now.GetEpochTime();

            //Duration = TimeSpan.FromSeconds(memoryStream.Length / (waveIn.WaveFormat.SampleRate * waveIn.WaveFormat.Channels * waveIn.WaveFormat.BitsPerSample / 8));
        }

        /// <summary>
        /// Event handler when new input data from capture device are available
        /// Writes available byte[] data to a memory stream
        /// </summary>
        /// <param name="sender">AudioRecorder object</param>
        /// <param name="e">WaveIn event arguments</param>
        private void OnDataAvailable(object sender, WaveInEventArgs e)
        {
            if (State == AudioRecorderState.Recording)
            {
                /*memoryStream.Write(e.Buffer, 0, e.BytesRecorded);
                memoryStream.Flush();*/
                waveWriter.Write(e.Buffer, 0, e.BytesRecorded);
                return;
            }

            if (State == AudioRecorderState.Monitoring)
            {
                for (int index = 0; index < e.BytesRecorded; index += 2)
                {
                    count++;
                    if (count >= notificationCount && notificationCount > 0)
                    {
                        MaximumCalculated(this, null);
                        count = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Save buffer of a memory stream to a Wave file on a specific path
        /// Additionally converts stereo to mono
        /// </summary>
        /// <param name="path">Path of a wave file to write data into</param>
        /// <param name="stereoToMono">If true (default), converts stereo to mono, otherwise not</param>
        public void SaveFile(WaveFile waveFile, bool stereoToMono=true)
        {
           /* if (State == AudioRecorderState.Monitoring || State == AudioRecorderState.Recording)
                return;

            memoryStream.Position = 0;
            if (waveIn.WaveFormat.Channels != 1 && stereoToMono)
            {
                
                using (var waveFileReader = new RawSourceWaveStream(memoryStream, waveFormat))
                {
                    var outFormat = new WaveFormat(waveFormat.SampleRate, waveFormat.BitsPerSample, 1);
                    using (var resampler = new MediaFoundationResampler(waveFileReader, outFormat))
                    {
                        WaveFileWriter.CreateWaveFile(waveFile.Path, resampler);
                    }
                }
            }
            else
            {
                using (var waveWriter = new WaveFileWriter(waveFile.Path, waveIn.WaveFormat))
                {
                    waveWriter.Write(memoryStream.GetBuffer(), 0, memoryStream.GetBuffer().Length);        
                }
            }*/
        }

        // TODO: Not used? Remove
        private byte[] StereoToMono(byte[] input)
        {
            byte[] output = new byte[input.Length / 2];
            int outputIndex = 0;
            for (int n = 0; n < input.Length; n += 4)
            {
                // copy in the first 16 bit sample
                output[outputIndex++] = input[n];
                output[outputIndex++] = input[n + 1];
            }
            return output;
        }

        // TODO: Not used? Remove
        private byte[] MixStereoToMono(byte[] input)
        {
            byte[] output = new byte[input.Length / 2];
            int outputIndex = 0;
            for (int n = 0; n < input.Length; n += 4)
            {
                int leftChannel = BitConverter.ToInt16(input, n);
                int rightChannel = BitConverter.ToInt16(input, n + 2);
                int mixed = (leftChannel + rightChannel) / 2;
                byte[] outSample = BitConverter.GetBytes((short)mixed);

                // copy in the first 16 bit sample
                output[outputIndex++] = outSample[0];
                output[outputIndex++] = outSample[1];
            }
            return output;
        }

        // TODO: Implement Dispose method
        public void Dispose()
        {
            if (waveWriter != null)
            {
                waveWriter.Close();
                waveWriter.Dispose();
            }
        }
       
    }
}
