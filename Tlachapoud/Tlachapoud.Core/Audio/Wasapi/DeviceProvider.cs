﻿// Import NAudio libraries
using NAudio.CoreAudioApi;

using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Tlachapoud.Core.Audio.Wasapi
{
    /// <summary>
    /// Provider for WASAPI and DirectSound capture/render devices
    /// </summary>
    public static class DeviceProvider
    {
        /// <summary>
        /// Get enumeration of all active devices for rendering audio data
        /// </summary>
        /// <returns>WASAPI MMDevice object</returns>
        public static IEnumerable<MMDevice> GetRenderDevices()
        {
            var deviceEnum = new MMDeviceEnumerator();
            return deviceEnum.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.Active)?.ToList() ?? null;
        }

        /// <summary>
        /// Get enumeration of all active devices for capturing audio data
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<MMDevice> GetCaptureDevices()
        {
            var deviceEnum = new MMDeviceEnumerator();
            return deviceEnum.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active)?.ToList() ?? null;
        }

        /// <summary>
        /// Get WaveIn Capabilities from capture device
        /// </summary>
        /// <param name="captureDevice">WASAPI MMDevice representing a capture device</param>
        /// <returns></returns>
        public static int? GetCaptureCapabilities(MMDevice captureDevice)
        {
            var captureCapabilities = new List<WaveInCapabilities>();
            for (int i = 0; i < WaveIn.DeviceCount; i++)
                captureCapabilities.Add(WaveIn.GetCapabilities(i));

            try
            {
                var waveOutCapabilities = captureCapabilities.Where(c => captureDevice.FriendlyName.Contains(c.ProductName)).First();
                return captureCapabilities.IndexOf(waveOutCapabilities);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get WaveOut Capabilities from capture device
        /// </summary>
        /// <param name="renderDevice">WASAPI MMDevice representing a render device</param>
        /// <returns></returns>
        public static int? GetRenderCapabilities(MMDevice renderDevice)
        {
            var renderCapabilities = new List<WaveOutCapabilities>();
            for (int i = 0; i < WaveOut.DeviceCount; i++)
                renderCapabilities.Add(WaveOut.GetCapabilities(i));

            try
            {
                var waveOutCapabilities = renderCapabilities.Where(c => renderDevice.FriendlyName.Contains(c.ProductName)).First();
                return renderCapabilities.IndexOf(waveOutCapabilities);
            }
            catch
            {
                return null;
            }
        }       
    }
}
