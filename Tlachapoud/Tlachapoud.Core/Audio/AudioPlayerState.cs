﻿namespace Tlachapoud.Core.Audio
{
    /// <summary>
    /// Enumeration of possible Audio Player states
    /// </summary>
    public enum AudioPlayerState
    {
        Stopped,
        Paused,
        Playing,
        NoRenderDevice
    }
}
