﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// Import library for Corpus Recording
using CorpusRecording;

namespace Tlachapoud.Core.Audio.CheckModules
{
    /// <summary>
    /// Class encapsulating loaded and configurated CheckModules
    /// </summary>
    public class CheckModules
    {
        private List<CheckModule> checkModules;

        /// <summary>
        /// Event handler for CheckModules
        /// </summary>
        /// <param name="audioData">Audio data that have been checked</param>
        /// <param name="args"></param>
        public delegate void CheckModuleEventHandler(IAudioData audioData, CheckModuleEventArgs args);
        /// <summary>
        /// Event raised when audio data has been checked
        /// </summary>
        public event CheckModuleEventHandler AudioDataChecked;

        /// <summary>
        /// Constructor creating a new collection of CheckModules based on given configurations
        /// </summary>
        /// <param name="checkModuleConfig">Configurations of CheckModules for ModuleFactory</param>
        public CheckModules(IEnumerable<CheckModuleConfig> checkModuleConfig)
        {
            Init(checkModuleConfig);
        }

        public void Init(IEnumerable<CheckModuleConfig> checkModuleConfig)
        {
            checkModules = new List<CheckModule>(checkModuleConfig.Count());
            foreach (var config in checkModuleConfig.Where(c => c.IsActive))
            {
                checkModules.Add(CheckModule.ModuleFactory(config));
            }
        }

        /// <summary>
        /// Check audio data using configured CheckModules 
        /// </summary>
        /// <param name="iAudioData">Audio data to be checked</param>
        public void Check(IAudioData iAudioData)
        {
            List<string> messages = new List<string>();
            Parallel.ForEach(checkModules, module =>
                {
                    var checkMessages = module.Check(iAudioData, 1, false);
                    if (checkMessages.Any())
                        messages.Add(checkMessages[0]);
                }
            );

            List<CheckResultMessage> checkResultMessages = new List<CheckResultMessage>();
            foreach (var msg in messages)
            {
                checkResultMessages.Add(new CheckResultMessage(msg));
            }

            if (checkResultMessages.Any())
                AudioDataChecked(iAudioData, new CheckModuleEventArgs(CheckModuleResult.Rejected, checkResultMessages));
            else
                AudioDataChecked(iAudioData, new CheckModuleEventArgs(CheckModuleResult.Approved, null));
        }
    }

    /// <summary>
    /// Event arguments object for CheckModule event handler
    /// </summary>
    public class CheckModuleEventArgs : EventArgs
    {
        private IEnumerable<CheckResultMessage> messages;
        private CheckModuleResult result;

        /// <summary>
        /// Constructor that creates a new event arguments object storing information about 
        /// check result of CheckModules
        /// </summary>
        /// <param name="result">Check result (enumeration value)</param>
        /// <param name="messages">Enumerable collection of messages returned by check</param>
        public CheckModuleEventArgs(CheckModuleResult result, IEnumerable<CheckResultMessage> messages)
        {
            this.messages = messages;
            this.result = result;
        }

        /// <summary>
        /// Enumerable collection of Messages returned by the check
        /// </summary>
        public IEnumerable<CheckResultMessage> Messages
        {
            get { return messages; }
        }

        /// <summary>
        /// Check result (enumeration value)
        /// </summary>
        public CheckModuleResult Result
        {
            get { return result; }
        }
    }

    /// <summary>
    /// Enumeration of possible results of checking using CheckModules
    /// </summary>
    public enum CheckModuleResult
    {
        Approved,
        Rejected
    }

    /// <summary>
    /// CheckModules Check result message
    /// </summary>
    public struct CheckResultMessage
    {
        /// <summary>
        /// Enumeration code for resulted state
        /// </summary>
        public CheckModuleState State { get; private set; }
        /// <summary>
        /// Message text of the result
        /// </summary>
        public string Message { get; private set;}

        /// <summary>
        /// Create a object storing basic information from Check method of CheckModule
        /// </summary>
        /// <param name="msg"></param>
        public CheckResultMessage(string msg)
        {
            State = CheckModuleStateConvertor.GetState(msg);
            Message = CheckModuleStateConvertor.GetMessage(State);
        }
    }

    /// <summary>
    /// Static state-machine converting message provided by CheckModules Check method to more human-readable format
    /// </summary>
    public static class CheckModuleStateConvertor
    {
        /// <summary>
        /// Get State enumeration value from string message from CheckModule Check method
        /// </summary>
        /// <param name="message">CheckModule Check output message</param>
        /// <returns>CheckModuleState enumeration value</returns>
        public static CheckModuleState GetState(string message)
        {
            switch (message)
            {
                case "Nahravka je prilis ticha.":
                    return CheckModuleState.TooSilent;
                case "Nahravka je prilis hlasita.":
                    return CheckModuleState.TooLoud;
                case "Pauza na zacatku je prilis kratka.":
                    return CheckModuleState.BegPauseTooShort;
                case "Pauza na konci je prilis kratka.":
                    return CheckModuleState.EndPauseTooShort;
                case "Pauza na zacatku i na konci je prilis kratka.":
                    return CheckModuleState.BothPauseTooShort;
                case "The recording is too short or the time constant for omitting the signal at the beginning too long.":
                    return CheckModuleState.WaveTooShortPause;
                case "The recording is too short to do a check. Intensity.FindSpeechBeginning: range exceeded":
                    return CheckModuleState.WaveTooShortIntensity;
                default:
                    return CheckModuleState.Unknown;
            }
        }

        /// <summary>
        /// Get human-readable message from CheckModuleState enumeration value
        /// </summary>
        /// <param name="state">CheckModuleState enumeration value</param>
        /// <returns>String message</returns>
        public static string GetMessage(CheckModuleState? state)
        {
            switch (state)
            {
                case CheckModuleState.TooSilent:
                    return "Nahrávka je příliš tichá.";
                case CheckModuleState.TooLoud:
                    return "Nahrávka je příliš hlasitá.";
                case CheckModuleState.BegPauseTooShort:
                    return "Pauza na začátku je příliš krátká.";
                case CheckModuleState.EndPauseTooShort:
                    return "Pauza na konci je příliš krátká.";
                case CheckModuleState.BothPauseTooShort:
                    return "Pauza na začátku a na konci je příliš krátká.";
                case CheckModuleState.WaveTooShortPause:
                    return "Nahrávka je příliš krátká pro kontrolu dodržení úseků bez řečového singálu.";
                case CheckModuleState.WaveTooShortIntensity:
                    return "Nahrávka je příliš krátká pro kontrolu dodržení intenzity.";
                case CheckModuleState.Unknown:
                    return "Nahrávka neprošla kontrolními moduly.";
                case CheckModuleState.Fine:
                    return "Nahrávka je v pořádku";
                default:
                    return null;
            }
        }
    }

    /// <summary>
    /// CheckModule state enumeration
    /// </summary>
    public enum CheckModuleState
    {
        TooSilent,
        TooLoud,
        BegPauseTooShort,
        EndPauseTooShort,
        BothPauseTooShort,
        WaveTooShortPause,
        WaveTooShortIntensity,
        Fine,
        Unknown
    }

    /// <summary>
    /// Struct containing Names of available check modules
    /// </summary>
    public struct AvailableCheckModules
    {
        public static readonly string Intensity = "Intensita_1";
        public static readonly string PauseLen = "DelkaPauzy";
    }
}
