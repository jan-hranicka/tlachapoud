﻿using System.Collections.Generic;

// Import CorpusRecording library for CheckModules
using CorpusRecording;

namespace Tlachapoud.Core.Audio.CheckModules
{
    /// <summary>
    /// Class for storing configuration for CheckModule object
    /// </summary>
    public class CheckModuleConfig : ICheckModuleConfig
    {
        /// <summary>
        /// Dictionary of configuration values for CheckModule
        /// </summary>
        private Dictionary<string, string> values;
        /// <summary>
        /// Flag determining whether CheckModule set by the configuration is active or not
        /// </summary>
        public bool IsActive { get; set; } = true;

        /// <summary>
        /// Constructor creating a new CheckModule configuration object
        /// </summary>
        public CheckModuleConfig()
        {
            values = new Dictionary<string, string>();
        }

        /// <summary>
        /// Add a new key-value specific data to the CheckModule configuration
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add(string key, string value)
        {
            values.Add(key, value);
        }

        #region ICheckModuleConfig interface members implementation
        /// <summary>
        /// Indexer returning value of a specific item by its key in values dictionary
        /// </summary>
        /// <param name="tKey">Item key</param>
        /// <returns>Item value as a string</returns>
        public string this[string tKey]
        {
            get
            {
                return values[tKey];
            }
        }

        /// <summary>
        /// Get module class
        /// </summary>
        public string ModuleClass
        {
            get
            {
                return values["Class"];
            }
        }

        /// <summary>
        /// Get module library
        /// </summary>
        public string ModuleLib
        {
            get
            {
                return values["Lib"];
            }
        }

        /// <summary>
        /// Get module name
        /// </summary>
        public string ModuleName
        {
            get
            {
                return values["Name"];
            }
        }
        # endregion ICheckModuleConfig interface members implementation
    }
}
