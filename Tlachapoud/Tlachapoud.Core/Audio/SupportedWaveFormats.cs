﻿using NAudio.Wave;

namespace Tlachapoud.Core.Audio
{
    /// <summary>
    /// Class containing all supported wave formats
    /// </summary>
    public static class SupportedWaveFormats
    {
        /// <summary>
        /// PCM Wave format with 48 000 Hz Sample Rate, 16bit depth and mono
        /// </summary>
        public static readonly WaveFormat Mono48kHz16bit = new WaveFormat(48000, 16, 1);
        /// <summary>
        /// PCM Wave format with 48 000 Hz Sample Rate, 16bit depth and stereo
        /// </summary>
        public static readonly WaveFormat Stereo48kHz16bit = new WaveFormat(48000, 16, 2);
    }
}
