﻿using System;
using System.IO;

// Import NAudio libraries
using NAudio.CoreAudioApi;
using NAudio.Utils;
using NAudio.Wave;

namespace Tlachapoud.Core.Audio.DirectSound
{
    /// <summary>
    /// Class represeting an Audio Recorder implemented via DirectSound API
    /// </summary>
    public class DirectSoundAudioRecorder : IDisposable
    {
        #region NAudio objects
        private MMDevice WasapiCaptureDevice;
        private WaveIn waveIn;
        private WaveFileWriter waveWriter;
        #endregion NAudio objects

        private MemoryStream memoryStream;
        private IgnoreDisposeStream ignoreDisposeStream;
        private int notificationCount = 0;
        private int count = 0;

        /// <summary>
        /// Audio recorder state (enumeration value)
        /// </summary>
        public AudioRecorderState State { get; set; }
        /// <summary>
        /// Get the time of recording wave file in Epoch time format
        /// </summary>
        public ulong RecordTime { get; private set; }
        /// <summary>
        /// Get duration of recorded wave file
        /// </summary>
        public TimeSpan Duration { get; private set; }
        /// <summary>
        /// Path of a Wave file into which data are recorded
        /// </summary>
        public string FilePath { get; private set; }

        /// <summary>
        /// Event raised when master peak volume is calculated
        /// </summary>
        public event MasterPeakVolumeEventHandler PeakVolumeCalculated;
        /// <summary>
        /// Event handler for input device master peak volume meter
        /// </summary>
        /// <param name="sender">Always null</param>
        /// <param name="args">Arguments with calculated volume</param>
        public delegate void MasterPeakVolumeEventHandler(object sender, MasterPeakVolumeEventArgs args);
        
        /// <summary>
        /// Create a new Audio Recorder using DirectSound API
        /// </summary>
        /// <param name="captureDevice">WASAPI capture device</param>
        public DirectSoundAudioRecorder(MMDevice captureDevice)
        {
            WasapiCaptureDevice = captureDevice;
            Init();
        }

        /// <summary>
        /// Change capture device and re-initialize Audio Recorder
        /// </summary>
        /// <param name="captureDevice">WASAPI capture device</param>
        public void ChangeDevice(MMDevice captureDevice)
        {
            if (State == AudioRecorderState.Monitoring)
                StopMonitoring();
            if (State == AudioRecorderState.Recording)
                StopRecording();

            Dispose();
            WasapiCaptureDevice = captureDevice;
            Init();
        }

        /// <summary>
        /// Initialize Audio Recorder with set WASAPI capture device
        /// </summary>
        private void Init()
        {
            var deviceNumber = Wasapi.DeviceProvider.GetCaptureCapabilities(WasapiCaptureDevice);
            if (deviceNumber != null)
            {
                waveIn = new WaveIn();
                waveIn.DeviceNumber = (int)deviceNumber;
            }
            else
            {
                State = AudioRecorderState.NoCaptureDevice;
                return;
            }

            // Support recording stereo because of issue in recording room at NTIS
            waveIn.WaveFormat = WasapiCaptureDevice.AudioClient.MixFormat.Channels == 1 ? 
                SupportedWaveFormats.Mono48kHz16bit : SupportedWaveFormats.Stereo48kHz16bit;
            waveIn.DataAvailable += OnDataAvailable;
            waveIn.RecordingStopped += OnRecordingStopped;

            State = AudioRecorderState.Stopped;
        }

        /// <summary>
        /// Begin audio recording, save audio data to a Wave file
        /// </summary>
        public void BeginRecording(string filePath)
        {
            Init();
            FilePath = filePath;
            waveWriter = new WaveFileWriter(filePath, waveIn.WaveFormat);
            waveIn.StartRecording();

            State = AudioRecorderState.Recording;
        }

        public void BeginRecording()
        {
            Init();
            memoryStream = new MemoryStream();
            ignoreDisposeStream = new IgnoreDisposeStream(memoryStream);
            waveWriter = new WaveFileWriter(ignoreDisposeStream, waveIn.WaveFormat);
            waveIn.StartRecording();

            State = AudioRecorderState.Recording;
        }

        /// <summary>
        /// Begin monitoring capture device, no data are saved and raise MasterPeakVolume event when calculated
        /// </summary>
        public void BeginMonitoring()
        {
            if (waveIn == null)
                return;

            notificationCount = waveIn.WaveFormat.SampleRate / 10;
            waveIn.StartRecording();
            State = AudioRecorderState.Monitoring;
        }

        /// <summary>
        /// Stop audio recording
        /// </summary>
        public void StopRecording()
        {
            waveIn.StopRecording();
            State = AudioRecorderState.RequestedStop;
            RecordTime = DateTime.Now.GetEpochTime();
            Duration = TimeSpan.FromSeconds((double)waveWriter.Length / (waveIn.WaveFormat.SampleRate * waveIn.WaveFormat.Channels * (waveIn.WaveFormat.BitsPerSample / 8)));

            Dispose();
        }

        /// <summary>
        /// Stop monitoring capture device
        /// </summary>
        public void StopMonitoring()
        {
            waveIn.StopRecording();
            State = AudioRecorderState.RequestedStop;
        }

        /// <summary>
        /// Event when recording has stopped
        /// </summary>
        private void OnRecordingStopped(object sender, StoppedEventArgs e)
        {
            if (State == AudioRecorderState.Monitoring || waveWriter == null)
                return;

            // Set RecordTime as EpochTime from Now
            RecordTime = DateTime.Now.GetEpochTime();
            Duration = TimeSpan.FromSeconds((double)waveWriter.Length / (waveIn.WaveFormat.SampleRate * waveIn.WaveFormat.Channels * (waveIn.WaveFormat.BitsPerSample / 8)));
        }

        /// <summary>
        /// Event when audio data are available
        /// </summary>
        private void OnDataAvailable(object sender, WaveInEventArgs e)
        {
            if (State == AudioRecorderState.Recording)
            {
                //waveWriter.WriteAsync(e.Buffer, 0, e.BytesRecorded);
                waveWriter.Write(e.Buffer, 0, e.Buffer.Length);
                return;
            }

            if (State == AudioRecorderState.Monitoring)
            {
                for (int index = 0; index < e.BytesRecorded; index += 2)
                {
                    count++;
                    if (count >= notificationCount && notificationCount > 0)
                    {
                        PeakVolumeCalculated?.Invoke(null, new MasterPeakVolumeEventArgs(WasapiCaptureDevice.AudioMeterInformation.MasterPeakValue));
                        count = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Write data recorded into a memory stream as a new WAV file
        /// </summary>
        /// <param name="path">Path where a new WAV file should be created</param>
        public void WriteToFile(string path)
        {
            if (ignoreDisposeStream == null)
            {
                throw new NotImplementedException("The method WriteToFile is implemented only for recording into a memory stream!");
            }

            using (WaveFileWriter waveFileWriter = new WaveFileWriter(path, waveIn.WaveFormat))
            {
                ignoreDisposeStream.CopyTo(waveFileWriter);
            }
        }

        /// <summary>
        /// Get a memory stream where data were stored during recording
        /// </summary>
        /// <returns>IgnoreDispiseStream aka MemoryStream</returns>
        public IgnoreDisposeStream GetStream()
        {
            return ignoreDisposeStream;
        }

        /// <summary>
        /// Dispose this object and all its resources
        /// </summary>
        public void Dispose()
        {
            if (State == AudioRecorderState.Monitoring)
                StopMonitoring();
            if (State == AudioRecorderState.Recording)
                StopRecording();

            if (waveWriter != null)
            {
                waveWriter.Close();
                waveWriter.Dispose();
            }
        }
    }

    /// <summary>
    /// Master peak volume event arguments for event handler
    /// </summary>
    public class MasterPeakVolumeEventArgs : EventArgs
    {
        /// <summary>
        /// Get the calucated value of master peak
        /// </summary>
        public float PeakVolume { get; private set; }

        /// <summary>
        /// Create a new arguments object
        /// </summary>
        /// <param name="peakVolume">Calculated peak volume in range 0.0 and 1.0</param>
        public MasterPeakVolumeEventArgs(float peakVolume)
        {
            PeakVolume = peakVolume;
        }
    }
}
