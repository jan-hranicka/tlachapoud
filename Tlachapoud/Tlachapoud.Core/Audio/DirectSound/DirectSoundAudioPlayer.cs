﻿using System;
using System.IO;

// Import NAudio libraries
using NAudio.CoreAudioApi;
using NAudio.Wave;

// Import library for corpus recording
using CorpusRecording;

namespace Tlachapoud.Core.Audio.DirectSound
{
    /// <summary>
    /// Class represeting an Audio Player implemented via DirectSound API
    /// </summary>
    public class DirectSoundAudioPlayer : IAudioData, IDisposable
    {
        #region NAudio objects
        WaveOut soundOut;
        WaveFileReader waveReader;
        WaveChannel32 pcm;
        WaveBuffer waveBuffer;
        #endregion NAudio objects

        /// <summary>
        /// Audio player state
        /// </summary>
        public AudioPlayerState State { get; set; }

        public event EventHandler PlaybackStopped;

        /// <summary>
        /// Create a new Audio Player implemented via DirectSound API
        /// </summary>
        /// <param name="renderDevice"></param>
        public DirectSoundAudioPlayer(MMDevice renderDevice)
        {  
            var deviceNumber = Wasapi.DeviceProvider.GetRenderCapabilities(renderDevice);
            if (deviceNumber != null)
            {
                soundOut = new WaveOut();
                soundOut.DeviceNumber = (int)deviceNumber;
            }
            else
            {
                State = AudioPlayerState.NoRenderDevice;
                return;
            }
            State = AudioPlayerState.Stopped;
        }

        /// <summary>
        /// Load Wave file to play audio data stored in a file
        /// </summary>
        /// <param name="path">Full path to a Wave file</param>
        public void Load(string path)
        {
            waveReader = new WaveFileReader(path);
            InitializePlayer();
        }

        /// <summary>
        /// Load Stream containing audio data to play
        /// </summary>
        /// <param name="stream">Stream with audio data</param>
        public void Load(Stream stream)
        {
            stream.Position = 0;
            waveReader = new WaveFileReader(stream);
            waveReader.Position = 0;
            InitializePlayer();          
        }

        /// <summary>
        /// Initialize Audio Player
        /// </summary>
        private void InitializePlayer()
        {
            pcm = new WaveChannel32(waveReader);
            pcm.PadWithZeroes = false;
            soundOut.Init(pcm);
            soundOut.PlaybackStopped += OnPlaybackStopped;

            byte[] bytes = new byte[waveReader.Length];
            waveReader.Read(bytes, 0, (int)waveReader.Length);
            waveBuffer = new WaveBuffer(bytes);
            waveReader.Position = 0;
        }

        /// <summary>
        /// Event when playback has stopped
        /// </summary>
        private void OnPlaybackStopped(object sender, StoppedEventArgs e)
        {
            if (State == AudioPlayerState.Stopped)
                return;

            Stop();
            if (PlaybackStopped != null && State != AudioPlayerState.Playing)
                PlaybackStopped(null, null);
        }

        /// <summary>
        /// Play loaded audio data
        /// </summary>
        public void Play()
        {
            State = AudioPlayerState.Playing;
            soundOut.Play();
        }

        /// <summary>
        /// Pause loaded audio data
        /// </summary>
        public void Pause()
        {
            State = AudioPlayerState.Paused;
            soundOut.Pause();
        }

        /// <summary>
        /// Stop playing loaded audio data
        /// </summary>
        public void Stop()
        {
            State = AudioPlayerState.Stopped;
            soundOut.Stop();
        }

        /// <summary>
        /// Reset audio player to play wave from the beginning
        /// </summary>
        public void Reset()
        {
            if (State != AudioPlayerState.Playing)
            {
                waveReader.Position = 0;
            }
        }

        /// <summary>
        /// Dispose this object and release all resources
        /// </summary>
        public void Dispose()
        {
            if (State != AudioPlayerState.Stopped && State != AudioPlayerState.NoRenderDevice)
                soundOut.Stop();

            if (waveReader != null)
            {
                waveReader.Close();
                waveReader.Dispose();
            }
            if (pcm != null)
            {
                pcm.Close();
                pcm.Dispose();
            }
        }

        #region CorpusRecording IAudioData interface members implementation
        /// <summary>
        /// Get number of channels of loaded audio
        /// </summary>
        /// <returns>Number of channels</returns>
        public int GetNumChannels()
        {
            return waveReader.WaveFormat.Channels;
        }

        /// <summary>
        /// Get number of samples of loaded audio
        /// </summary>
        /// <returns>Number of samples</returns>
        public int GetNumSamples()
        {
            return waveBuffer.ShortBuffer.Length;
        }

        /// <summary>
        /// Get the sample rate of loaded audio
        /// </summary>
        /// <returns>Sample rate</returns>
        public int GetSampleRate()
        {
            return waveReader.WaveFormat.SampleRate;
        }

        /// <summary>
        /// Get samples of loaded audio as a short array
        /// </summary>
        /// <param name="iChannel">Not implemented, expects mono</param>
        /// <returns>Short buffer with audio samples</returns>
        public short[] GetSamples(int iChannel)
        {
            if (waveReader.WaveFormat.Channels == 1)
            {
                byte[] byteBuffer = waveBuffer.ByteBuffer;
                short[] shortBuffer = new short[(int)Math.Ceiling(byteBuffer.Length / 2d)];
                Buffer.BlockCopy(byteBuffer, 0, shortBuffer, 0, byteBuffer.Length);

                return shortBuffer;
            }
            else
            {
                byte[] buffer = waveBuffer.ByteBuffer;
                short[] left = new short[buffer.LongLength / 4];
                int index = 0;
                for (int sample = 0; sample < buffer.LongLength / 4; sample++)
                {
                    left[sample] = BitConverter.ToInt16(buffer, index);
                    index += 4;
                }

                return left; // when stereo then return only one (left) channel
            }
        }

        /// <summary>
        /// (Not supported) Set samples
        /// </summary>
        [Obsolete("This method is not supported yet")]
        public void SetSamples(short[] saSamples, int iChannel)
        {
            throw new NotSupportedException();
        }

        
        #endregion CorpusRecording IAudioData interface members implementation
    }
}
