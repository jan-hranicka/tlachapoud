﻿namespace Tlachapoud.Core.Audio
{
    /// <summary>
    /// Enumeration of possible states of Audio Recorder
    /// </summary>
    public enum AudioRecorderState
    {
        Monitoring,
        Stopped,
        RequestedStop,
        Recording,
        NoCaptureDevice
    }
}
