﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Tlachapoud.Controls
{
    /// <summary>
    /// Interaction logic for RegistrationBar.xaml
    /// </summary>
    public partial class RegistrationBar : UserControl
    {
        /// <summary>
        /// Default step when Registration bar is initialized
        /// </summary>
        const byte DEFAULT_STEP = 1;

        #region Static auxiliary objects
        static Grid _static_gMain;
        static Label _static_lStep;
        static List<Label> _static_labels = new List<Label>();
        static List<Rectangle> _static_rectangles = new List<Rectangle>();
        static byte _static_step = 1;
        #endregion

        /// <summary>
        /// Create a new instance of Registration bar
        /// </summary>
        public RegistrationBar()
        {
            InitializeComponent();

            _static_gMain = gMain;
            _static_lStep = lStep;
        }

        /// <summary>
        /// Register Items Dependency Property
        /// </summary>
        public static readonly DependencyProperty ItemsProperty = DependencyProperty.Register("Items",
            typeof(IEnumerable<string>), typeof(RegistrationBar), new FrameworkPropertyMetadata(ItemsProperty_PropertyChangedCallback));

        /// <summary>
        /// Items Dependency Property value changed callback
        /// </summary>
        /// <param name="obj">Dependency Propert object</param>
        /// <param name="args">DP Callback event arguments</param>
        private static void ItemsProperty_PropertyChangedCallback(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            // Reset static values
            _static_labels.Clear();
            _static_rectangles.Clear();
            _static_step = 1;

            var _itemsSource = (IEnumerable<string>)args.NewValue;

            // Prepare columns in Main Grid object
            foreach (var item in _itemsSource)
            {
                ColumnDefinition column = new ColumnDefinition();
                column.Width = new GridLength(1, GridUnitType.Star);
                _static_gMain.ColumnDefinitions.Add(column);
            }

            int i = 0;
            foreach (var item in _itemsSource)
            {
                Label label = new Label()
                {
                    Content = item,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    FontWeight = FontWeights.SemiBold,
                    Foreground = new SolidColorBrush(Colors.Gray)
                };
                Rectangle rect = new Rectangle()
                {
                    Height = 3,
                    Fill = new SolidColorBrush(Colors.LightGray),
                    Margin = new Thickness(2)
                };

                if (i == 0)
                {
                    label.Foreground = new SolidColorBrush(Colors.Black);
                    rect.Fill = new SolidColorBrush(Colors.DeepSkyBlue);
                }

                _static_labels.Add(label);
                _static_rectangles.Add(rect);

                _static_gMain.Children.Add(label);
                _static_gMain.Children.Add(rect);
                Grid.SetColumn(label, i);
                Grid.SetColumn(rect, i);
                Grid.SetRow(label, 2);
                Grid.SetRow(rect, 1);

                i++;
            }

            _static_lStep.Content = string.Format("Krok {0} ze {1}", _static_step, _static_labels.Count);
        }

        /// <summary>
        /// Register Items property (g/s) for ItemsProperty object
        /// </summary>
        public IEnumerable<string> Items
        {
            set { SetValue(ItemsProperty, value); }
            get { return (IEnumerable<string>)GetValue(ItemsProperty); }
        }

        /// <summary>
        /// Register Items Dependency Property
        /// </summary>
        public static readonly DependencyProperty StepProperty = DependencyProperty.Register("Step",
            typeof(byte), typeof(RegistrationBar), new FrameworkPropertyMetadata(DEFAULT_STEP, StepProperty_PropertyChangedCallback));

        /// <summary>
        /// Items Dependency Property value changed callback
        /// </summary>
        /// <param name="obj">Dependency Propert object</param>
        /// <param name="args">DP Callback event arguments</param>
        private static void StepProperty_PropertyChangedCallback(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var _prev_step = (byte)args.OldValue;
            _static_step = (byte)args.NewValue;

            // Change labels color for current step
            _static_labels[_static_step - 1].Foreground = new SolidColorBrush(Colors.Black);
            _static_labels[_prev_step - 1].Foreground = new SolidColorBrush(Colors.Gray);

            // Change rectangles color for current step
            _static_rectangles[_static_step - 1].Fill = new SolidColorBrush(Colors.DeepSkyBlue);
            _static_rectangles[_prev_step - 1].Fill = new SolidColorBrush(Colors.LightGray);

            _static_lStep.Content = string.Format("Krok {0} ze {1}", _static_step, _static_labels.Count);
        }

        /// <summary>
        /// Register Items property (g/s) for ItemsProperty object
        /// </summary>
        public byte Step
        {
            set { SetValue(StepProperty, value); }
            get { return (byte)GetValue(StepProperty); }
        }
    }
}
