﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Tlachapoud.Controls
{
    /// <summary>
    /// Interaction logic for RangeProgressBar.xaml
    /// </summary>
    public partial class RangeProgressBar : UserControl
    {
        #region Static auxiliary objects
        static ColumnDefinition doneColumn;
        static ColumnDefinition incompleteColumn;
        static Label lblCompleteNum;
        static Label lblIncompleteNum;
        static Label _lComplete;
        static Label _lIncomplete;
        #endregion

        /// <summary>
        /// Create a new instance of RangeProgress bar
        /// </summary>
        public RangeProgressBar()
        {
            InitializeComponent();

            doneColumn = doneProgressWidth;
            incompleteColumn = incompleteProgressWidth;
            lblCompleteNum = lblComplete;
            lblIncompleteNum = lblIncomplete;
            _lComplete = lComplete;
            _lIncomplete = lIncomplete;
        }

        #region Complete Dependency Property
        public static readonly DependencyProperty CompleteProperty = DependencyProperty.Register(
            "Complete", typeof(int), typeof(RangeProgressBar),
            new PropertyMetadata(Convert.ToInt32(-1), CompleteProperty_PropertyChangedCallback),
            CompleteProperty_ValidateValueCallback);

        private static void CompleteProperty_PropertyChangedCallback(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            doneColumn.Width = new GridLength(Convert.ToDouble(args.NewValue), GridUnitType.Star);
            lblCompleteNum.Content = args.NewValue.ToString();

        }

        private static bool CompleteProperty_ValidateValueCallback(object target)
        {
            return true;
        }

        public int Complete
        {
            get
            {
                return Convert.ToInt32(GetValue(CompleteProperty));
            }
            set
            {
                SetValue(CompleteProperty, value);
            }
        }
        #endregion Complete Dependency Property

        #region Incomplete Dependency Property
        public static readonly DependencyProperty IncompleteProperty = DependencyProperty.Register(
            "Incomplete", typeof(int), typeof(RangeProgressBar),
            new PropertyMetadata(Convert.ToInt32(0), IncompleteProperty_PropertyChangedCallback),
                IncompleteProperty_ValidateValueCallback);

        private static void IncompleteProperty_PropertyChangedCallback(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            incompleteColumn.Width = new GridLength(Convert.ToDouble(args.NewValue), GridUnitType.Star);
            lblIncompleteNum.Content = args.NewValue.ToString();
        }

        private static bool IncompleteProperty_ValidateValueCallback(object target)
        {
            return true;
        }

        public int Incomplete
        {
            get
            {
                return Convert.ToInt32(GetValue(IncompleteProperty));
            }
            set
            {
                SetValue(IncompleteProperty, value);
            }
        }
        #endregion

        #region CompleteLabel Dependency Property
        public static readonly DependencyProperty CompleteLabelProperty = DependencyProperty.Register(
            "CompleteLabel", typeof(string), typeof(RangeProgressBar), new PropertyMetadata("COMPLETED",
                CompleteLabelProperty_PropertyChangedCallback)
            );

        private static void CompleteLabelProperty_PropertyChangedCallback(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var newLabel = (string)args.NewValue;

            _lComplete.Content = newLabel.ToUpper();
        }

        public string CompleteLabel
        {
            get
            {
                return (string)GetValue(CompleteLabelProperty);
            }
            set
            {
                SetValue(CompleteLabelProperty, value);
            }
        }
        #endregion CompleteLabel Dependency Property

        #region IncompleteLabel Dependency Property
        public static readonly DependencyProperty IncompleteLabelProperty = DependencyProperty.Register(
            "IncompleteLabel", typeof(string), typeof(RangeProgressBar), new PropertyMetadata("REMAINS",
                IncompleteLabelProperty_PropertyChangedCallback)
            );

        private static void IncompleteLabelProperty_PropertyChangedCallback(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var newLabel = (string)args.NewValue;

            _lIncomplete.Content = newLabel.ToUpper();
        }

        public string IncompleteLabel
        {
            get
            {
                return (string)GetValue(IncompleteLabelProperty);
            }
            set
            {
                SetValue(IncompleteLabelProperty, value);
            }
        }
        #endregion CompleteLabel Dependency Property
    }
}
