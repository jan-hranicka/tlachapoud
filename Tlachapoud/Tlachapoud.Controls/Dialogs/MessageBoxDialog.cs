﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using MahApps.Metro.Controls;
using MahApps.Metro.IconPacks;

namespace Tlachapoud.Controls.Dialogs
{
    /// <summary>
    /// Dialog window with informative OK or Yes/No question
    /// </summary>
    public sealed class MessageBoxDialog : MetroWindow
    {
        private readonly SolidColorBrush DEFAUTL_BRUSH = new SolidColorBrush(Color.FromRgb(65, 177, 225));
        private SolidColorBrush windowBrush;

        /// <summary>
        /// Create a new MessageBox dialog window
        /// </summary>
        /// <param name="title">Title string</param>
        /// <param name="caption">Caption string</param>
        /// <param name="buttons">Buttons</param>
        /// <param name="icon">Icon showed in a dialog</param>
        public MessageBoxDialog(string title, string caption, MessageBoxDialogButtons buttons, PackIconFontAwesomeKind icon, Color? windowColor = null)
        {
            if (windowColor == null)
                windowBrush = DEFAUTL_BRUSH;
            else
                windowBrush = new SolidColorBrush((Color)windowColor);

            // Dialog Window settings
            SizeToContent = SizeToContent.Height;
            ResizeMode = ResizeMode.NoResize;
            WindowStyle = WindowStyle.None;
            WindowStartupLocation = WindowStartupLocation.CenterOwner;
            BorderThickness = new Thickness(2);
            BorderBrush = windowBrush;
            WindowTitleBrush = windowBrush;
            Width = 500;
            Title = title;

            Grid mainGrid = new Grid();
            mainGrid.Margin = new Thickness(5);
            Content = mainGrid;

            // Create grid for message
            for (int i = 0; i < 2; i++)
                mainGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Auto) });

            mainGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) });
            mainGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });

            // Icon
            PackIconFontAwesome msgIcon = new PackIconFontAwesome();
            msgIcon.Kind = icon;
            msgIcon.Width = 40;
            msgIcon.Height = 40;
            msgIcon.VerticalAlignment = VerticalAlignment.Center;
            msgIcon.HorizontalAlignment = HorizontalAlignment.Center;
            msgIcon.Margin = new Thickness(10);
            msgIcon.Foreground = windowBrush;
            mainGrid.Children.Add(msgIcon);

            // Text
            TextBlock tbkMessage = new TextBlock();
            tbkMessage.Text = caption;
            tbkMessage.TextWrapping = TextWrapping.Wrap;
            tbkMessage.TextAlignment = TextAlignment.Justify;
            tbkMessage.Margin = new Thickness(10, 15, 20, 30);
            Grid.SetColumn(tbkMessage, 1);
            mainGrid.Children.Add(tbkMessage);

            switch (buttons)
            {
                case MessageBoxDialogButtons.OK:
                    // Add OK button
                    Button btnOk = new Button();
                    btnOk.Content = "OK";
                    btnOk.Margin = new Thickness(13, 13, 20, 13);
                    btnOk.HorizontalAlignment = HorizontalAlignment.Right;
                    btnOk.VerticalAlignment = VerticalAlignment.Bottom;
                    btnOk.MinWidth = 70d;
                    btnOk.Click += delegate (object sender, RoutedEventArgs e)
                    {
                        Close();
                    };
                    Grid.SetRow(btnOk, 1);
                    Grid.SetColumn(btnOk, 1);

                    mainGrid.Children.Add(btnOk);

                    break;
                case MessageBoxDialogButtons.YesNo:
                    // Add StackPanel
                    StackPanel stackPanel = new StackPanel();
                    stackPanel.Orientation = Orientation.Horizontal;
                    stackPanel.HorizontalAlignment = HorizontalAlignment.Right;
                    stackPanel.VerticalAlignment = VerticalAlignment.Bottom;
                    stackPanel.Margin = new Thickness(10);
                    Grid.SetColumn(stackPanel, 1);
                    Grid.SetRow(stackPanel, 1);

                    mainGrid.Children.Add(stackPanel);
                    // Add Yes button
                    Button btnYes = new Button();
                    btnYes.Content = "ANO";
                    btnYes.Margin = new Thickness(3);
                    btnYes.MinWidth = 70d;
                    btnYes.Click += delegate (object sender, RoutedEventArgs e)
                    {
                        DialogResult = true;
                        Close();
                    };
                    stackPanel.Children.Add(btnYes);

                    // Add No button
                    Button btnNo = new Button();
                    btnNo.Content = "NE";
                    btnNo.Margin = new Thickness(3, 3, 10, 3);
                    btnNo.MinWidth = 70d;
                    btnNo.Click += delegate (object sender, RoutedEventArgs e)
                    {
                        Close();
                    };
                    stackPanel.Children.Add(btnNo);

                    break;
                default:
                    break;
            }
        }
    }

    public enum MessageBoxDialogButtons
    {
        OK,
        YesNo
    }
}
