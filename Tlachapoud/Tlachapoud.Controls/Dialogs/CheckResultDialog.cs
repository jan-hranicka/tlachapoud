﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

// Import MahApps Metro library
using MahApps.Metro.Controls;
using MahApps.Metro.IconPacks;

// Import Tlachapoud Core library for CheckModules
using Tlachapoud.Core.Audio.CheckModules;

namespace Tlachapoud.Controls.Dialogs
{
    /// <summary>
    /// Dialog window with information about Check Modules check results
    /// </summary>
    public sealed class CheckResultDialog : MetroWindow
    {
        /// <summary>
        /// Create a new dialog window for providing Check Module results to user
        /// </summary>
        /// <param name="title">Window title</param>
        /// <param name="headerMsg">Window header message</param>
        /// <param name="checkResultMsg">Results of CM's Check</param>
        public CheckResultDialog(string title, string headerMsg, IEnumerable<CheckResultMessage> checkResultMsg)
        {
            // Dialog Window settings
            SizeToContent = SizeToContent.Height;
            ResizeMode = ResizeMode.NoResize;
            WindowStyle = WindowStyle.None;
            WindowStartupLocation = WindowStartupLocation.CenterOwner;
            BorderThickness = new Thickness(2);
            BorderBrush = new SolidColorBrush(Colors.Crimson);
            WindowTitleBrush = new SolidColorBrush(Colors.Crimson);
            Width = 400;
            Title = title;
            // Close this dialog window when Enter or Esc is pressed
            KeyDown += delegate (object sender, KeyEventArgs e) 
            {
                if (e.Key == Key.Enter || e.Key == Key.Escape)
                    Close();
            };

            Grid mainGrid = new Grid();
            mainGrid.Margin = new Thickness(5);
            Content = mainGrid;

            // Create grid for messages
            for (int i = 0; i < 3; i++)
            {
                mainGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Auto) });
            }

            // Header message
            TextBlock tbkHeader = new TextBlock();
            tbkHeader.TextWrapping = TextWrapping.Wrap;
            tbkHeader.Text = headerMsg;
            tbkHeader.Margin = new Thickness(5);
            Grid.SetRow(tbkHeader, 0);
            mainGrid.Children.Add(tbkHeader);

            // Understand button
            Button btnConfirm = new Button();
            btnConfirm.Content = "ROZUMÍM";
            btnConfirm.HorizontalAlignment = HorizontalAlignment.Right;
            btnConfirm.VerticalAlignment = VerticalAlignment.Center;
            btnConfirm.Margin = new Thickness(5);
            btnConfirm.MinWidth = 100d;
            btnConfirm.Click += delegate (object sender, RoutedEventArgs e) { Close(); };
            Grid.SetRow(btnConfirm, 2);
            mainGrid.Children.Add(btnConfirm);

            // Messages
            Grid msgGrid = new Grid();
            msgGrid.Margin = new Thickness(15, 5, 5, 15);
            msgGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) });
            msgGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            for (int i = 0; i < checkResultMsg.Count(); i++)
            {
                msgGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Auto) });
            }

            for (int i = 0; i < checkResultMsg.Count(); i++)
            {
                var msgItem = checkResultMsg.ElementAt(i);

                // Icon
                PackIconFontAwesome icoMsg = new PackIconFontAwesome()
                {
                    Kind = msgItem.State != CheckModuleState.Unknown ? PackIconFontAwesomeKind.ExclamationCircle : PackIconFontAwesomeKind.QuestionCircle,
                    Foreground = msgItem.State != CheckModuleState.Unknown ? new SolidColorBrush(Colors.Crimson) : new SolidColorBrush(Colors.Gray),
                    VerticalAlignment = VerticalAlignment.Top,
                    Margin = new Thickness(3, 3, 15, 3)
                };
                Grid.SetRow(icoMsg, i);
                msgGrid.Children.Add(icoMsg);

                // Message
                TextBlock tbkMsg = new TextBlock()
                {
                    TextWrapping = TextWrapping.Wrap,
                    Text = msgItem.Message,
                    VerticalAlignment = VerticalAlignment.Top,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Margin = new Thickness(3)
                };
                Grid.SetColumn(tbkMsg, 1);
                Grid.SetRow(tbkMsg, i);
                msgGrid.Children.Add(tbkMsg);
            }
            Grid.SetRow(msgGrid, 1);
            mainGrid.Children.Add(msgGrid);
        }
    }
}
