﻿using MahApps.Metro.IconPacks;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Tlachapoud.Core.Audio.CheckModules;

namespace Tlachapoud.Controls
{
    /// <summary>
    /// Negate boolean value converter
    /// </summary>
    public sealed class NegateBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }
    }

    /// <summary>
    /// Converter used to adapt icons in buttons
    /// </summary>
    public sealed class HalfSize : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)value*.35;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)value*.35;
        }
    }

    /// <summary>
    /// Converter dynamically changing flyout width
    /// </summary>
    public sealed class DynamicFlyoutConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return 400 + (1080 - (double)value)*1.75;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Converter to convert null to Visibility enumeration
    /// </summary>
    public sealed class NullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Converter to convert boolean values to opacity (true is visible, false is almost transparent)
    /// </summary>
    public sealed class BooleanToOpacityConverter : BooleanConverter<double>
    {
        public BooleanToOpacityConverter() :
            base(1.0d, .25d)
        {

        }
    }

    /// <summary>
    /// CheckResult to IconStyle converter
    /// </summary>
    public sealed class CheckResultToIconStyleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Style style = new Style(typeof(PackIconFontAwesome));
            style.Setters.Add(new Setter(FrameworkElement.VerticalAlignmentProperty, VerticalAlignment.Center));
            style.Setters.Add(new Setter(PackIconFontAwesome.KindProperty,
                (CheckModuleResult)value == CheckModuleResult.Approved ? PackIconFontAwesomeKind.Check : PackIconFontAwesomeKind.Times));
            style.Setters.Add(new Setter(System.Windows.Controls.Control.ForegroundProperty,
                (CheckModuleResult)value == CheckModuleResult.Approved ? new SolidColorBrush(Colors.Green) : new SolidColorBrush(Colors.Red)));

            return style;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Generic class defining base for Boolean converters
    /// </summary>
    /// <typeparam name="T">Generic type</typeparam>
    public class BooleanConverter<T> : IValueConverter
    {
        public BooleanConverter(T trueValue, T falseValue)
        {
            True = trueValue;
            False = falseValue;
        }

        public T True { get; set; }
        public T False { get; set; }

        public virtual object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is bool && ((bool)value) ? True : False;
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is T && EqualityComparer<T>.Default.Equals((T)value, True);
        }
    }

    /// <summary>
    /// Boolean to visibility converter (tue = Visible, false = Collapsed)
    /// </summary>
    public sealed class BooleanToVisibilityConverter : BooleanConverter<Visibility>
    {
        public BooleanToVisibilityConverter() :
            base(Visibility.Visible, Visibility.Collapsed)
        { }
    }

    /// <summary>
    /// Boolean to color converter (true = SkyBlue, false = LightGray
    /// </summary>
    public sealed class BooleanToColorConverter : BooleanConverter<SolidColorBrush>
    {
        public BooleanToColorConverter() :
            base(new SolidColorBrush(Colors.SkyBlue), new SolidColorBrush(Colors.LightGray))
        { }
    }
}
