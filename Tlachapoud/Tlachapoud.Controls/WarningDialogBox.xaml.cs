﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tlachapoud.Controls
{
    /// <summary>
    /// Interaction logic for WarningDialogBox.xaml
    /// </summary>
    public partial class WarningDialogBox : UserControl
    {
        public WarningDialogBox()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty MessageProperty = DependencyProperty.Register("Message",
            typeof(string), typeof(WarningDialogBox), new FrameworkPropertyMetadata(MessageProperty_propertyChangedCallback));

        private static void MessageProperty_propertyChangedCallback(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {

        }

        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }
    }
}
