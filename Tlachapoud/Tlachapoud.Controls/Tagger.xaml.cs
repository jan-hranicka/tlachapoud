﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Tlachapoud.Core.Data;

namespace Tlachapoud.Controls
{
    /// <summary>
    /// Interakční logika pro Tagger.xaml
    /// </summary>
    public partial class Tagger : UserControl
    {
        // Define color constants for Tagger
        private readonly Color COLOR_DISABLED = Colors.LightGray;
        private readonly Color COLOR_ENABLED = Colors.Black;
        private readonly Color COLOR_GOOD = Colors.Gold;
        private readonly Color COLOR_BAD = Colors.OrangeRed;

        /// <summary>
        /// Event raised when wave file tag has been changed
        /// </summary>
        public event TagChangedEventHandler TagChanged;

        /// <summary>
        /// Handler for TagChanged event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public delegate void TagChangedEventHandler(object sender, TagChangedEventArgs args);

        /// <summary>
        /// Tagger constructor
        /// </summary>
        public Tagger()
        {
            InitializeComponent();
        }

        #region TagValue Dependency Property
        /// <summary>
        /// TagValue property storing WaveFileTag value
        /// </summary>
        public static readonly DependencyProperty TagValueProperty = DependencyProperty.Register("TagValue",
            typeof(WaveFileTag), typeof(Tagger), new FrameworkPropertyMetadata(WaveFileTag.NotSpecified,
                TagValueChangedCallback));

        /// <summary>
        /// Callback for TagValue dependency property
        /// </summary>
        private static void TagValueChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Tagger t = (Tagger)d;
            WaveFileTag tag = (WaveFileTag)e.NewValue;
            switch (tag)
            {
                case WaveFileTag.NotSpecified:
                    t.btnGood.IsEnabled = t.IsFine;
                    t.btnBad.IsEnabled = true;
                    break;
                case WaveFileTag.Good:
                    t.btnBad.IsEnabled = false;
                    t.icoGood.Foreground = new SolidColorBrush(t.COLOR_GOOD);
                    break;
                case WaveFileTag.Bad:
                    t.btnGood.IsEnabled = false;
                    t.icoBad.Foreground = new SolidColorBrush(t.COLOR_BAD);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Property storing information about WaveFile tag
        /// </summary>
        public WaveFileTag TagValue
        {
            get { return (WaveFileTag)GetValue(TagValueProperty); }
            set { SetValue(TagValueProperty, value); }
        }
        #endregion TagValue Dependency Property

        #region IsFine Dependency Property
        /// <summary>
        /// IsFine property storing information that linked wave file successfully went through the check modules
        /// </summary>
        public static readonly DependencyProperty IsFineProperty = DependencyProperty.Register("IsFine",
            typeof(bool), typeof(Tagger), new FrameworkPropertyMetadata(true,
                IsFineChangedCallback));

        /// <summary>
        /// IsFine dependency property value changed callback
        /// </summary>
        private static void IsFineChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Tagger t = (Tagger)d;
            bool checkResult = (bool)e.NewValue;
            if (!checkResult)
            {
                t.btnGood.IsEnabled = false;
            }
        }

        /// <summary>
        /// Property storing logical flag whether linked wave file is fine or not (went through the check modules)
        /// </summary>
        public bool IsFine
        {
            get { return (bool)GetValue(IsFineProperty); }
            set { SetValue(IsFineProperty, value); }
        }
        #endregion IsFine Dependency Property

        /// <summary>
        /// Event handler when user enter with cursor on any button of Tagger control.
        /// Change foreground color of icon depending on the type of button.
        /// </summary>
        /// <param name="sender">Button control</param>
        /// <param name="e">Mouse event arguments</param>
        private void OnButtonMouseEnter(object sender, MouseEventArgs e)
        {
            Button btn = sender as Button;

            if (btn.Name == "btnGood")
                icoGood.Foreground = new SolidColorBrush(COLOR_GOOD);
            else
                icoBad.Foreground = new SolidColorBrush(COLOR_BAD);
        }

        /// <summary>
        /// Event handler when user leave with cursor from any button of Tagger control.
        /// Change foreground color of icon depending on the type of button.
        /// </summary>
        /// <param name="sender">Button control</param>
        /// <param name="e">Mouse event arguments</param>
        private void OnButtonMouseLeave(object sender, MouseEventArgs e)
        {
            Button btn = sender as Button;

            if (btn.Name == "btnGood")
            {
                if (TagValue == WaveFileTag.Good)
                    return;
                icoGood.Foreground = new SolidColorBrush(COLOR_ENABLED);
            }
            else
            {
                if (TagValue == WaveFileTag.Bad)
                    return;
                icoBad.Foreground = new SolidColorBrush(COLOR_ENABLED);
            }
        }

        /// <summary>
        /// Event handler when any button of Tagger control change its IsEnabled flag
        /// Change foreground color of icon depending on the type of button.
        /// </summary>
        /// <param name="sender">Button control</param>
        /// <param name="e">Dependency property changed event arguments</param>
        private void OnButtonIsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Button btn = sender as Button;
            bool isEnabled = (bool)e.NewValue;
            if (btn.Name == "btnGood")
            {
                if (TagValue == WaveFileTag.Good)
                    return;

                if (isEnabled)
                    icoGood.Foreground = new SolidColorBrush(COLOR_ENABLED);
                else
                    icoGood.Foreground = new SolidColorBrush(COLOR_DISABLED);
            }
            else
            {
                if (TagValue == WaveFileTag.Bad)
                    return;

                if (isEnabled)
                    icoBad.Foreground = new SolidColorBrush(COLOR_ENABLED);
                else
                    icoBad.Foreground = new SolidColorBrush(COLOR_DISABLED);
            }
        }

        /// <summary>
        /// Event handler when user click on any button of Tagger control.
        /// Change foreground color of icon depending on the type of button.
        /// </summary>
        /// <param name="sender">Button control</param>
        /// <param name="e">Routed event arguments</param>
        private void OnButtonClick(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            if (btn.Name == "btnGood")
            {
                if (TagValue == WaveFileTag.NotSpecified)
                {
                    icoGood.Foreground = new SolidColorBrush(COLOR_GOOD);
                    TagValue = WaveFileTag.Good;
                }
                else
                {
                    icoGood.Foreground = new SolidColorBrush(COLOR_ENABLED);
                    TagValue = WaveFileTag.NotSpecified;
                }
            }
            else
            {
                if (TagValue == WaveFileTag.NotSpecified)
                {
                    icoBad.Foreground = new SolidColorBrush(COLOR_BAD);
                    TagValue = WaveFileTag.Bad;
                }
                else
                {
                    icoBad.Foreground = new SolidColorBrush(COLOR_ENABLED);
                    TagValue = WaveFileTag.NotSpecified;
                }
            }

            // Raise TagChanged event
            TagChanged?.Invoke(this, new TagChangedEventArgs(TagValue));
        }
    }

    /// <summary>
    /// Contains information about new set tag in Tagger control
    /// </summary>
    public class TagChangedEventArgs : EventArgs
    {
        /// <summary>
        /// New tag value
        /// </summary>
        public WaveFileTag NewTag { get; set; }

        /// <summary>
        /// Create new TagChanged event arguments encapsulation object
        /// </summary>
        /// <param name="newTag"></param>
        public TagChangedEventArgs(WaveFileTag newTag)
        {
            NewTag = newTag;
        }
    }
}
