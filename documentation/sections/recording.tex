\section{Obsluha nahrávací aplikace}
Hlavní formulář aplikace zobrazený na obrázku \ref{fig: win_main} poskytuje hlavní ovládací prvky pro proces nahrávání frází. V horní části formuláře je zobrazována fráze k nahrávání a pořadí této fráze. Pod textem fráze je indikátor počtu nahraných frází a frází zbývajících k nahrání. Pod ukazatelem, uprostřed formuláře, je panel ovládacích tlačítek pro nahrávání a navigaci mezi frázemi. V levé dolní části formuláře je informační panel o vstupním a výstupním zařízení a stavu kontrolních modulů. V pravé dolní části formuláře je seznam již nahraných verzí aktuální fráze s dodatečnými informacemi.

\begin{figure}
	\includegraphics[scale=.3]{figures/win_main_app.png}
	\caption{Hlavní formulář aplikace Phrase Recorder nabízí ucelený přehled o postupu nahrávání, navigační tlačítka pro přesun mezi frázemi, informační panel v levé dolní části formuláře a seznam nahraných verzí fráze v pravé dolní části aplikace.}
	\label{fig: win_main}
\end{figure}

\subsection{Proces nahrávání frází}
V horní části hlavního formuláře je zobrazen text aktuálně vybrané fráze. Před každým nahráváním doporučujeme si tuto frázi přečíst, případně i nahlas vyslovit. Tímto se můžete vyhnout zadrhávání či přeřekům. Nad textem fráze je zobrazeno číslo fráze v procesu nahrávání a její aktuální stav. Následující seznam poskytuje ucelený přehled všech možných stavů fráze včetně stručných popisků:

\begin{enumerate}[label={}]
	\item \includegraphics[scale=.8]{figures/recorded.png} Fráze byla úspěšně nahrána. Takto označená fráze má již alespoň jednu verzi, která úspěšně prošla kontrolou kontrolními moduly.
	
	\item \includegraphics[scale=.8]{figures/rejected.png} Fráze byla nahrána, ale odmítnuta kontrolními moduly. Takto označenou frázi je třeba pokusit se nahrát znovu a brát v úvahu upozornění napsané u odmítnutých frází.
	
	\item \includegraphics[scale=.8]{figures/skipped.png} Fráze byla odmítnuta. Takto označená fráze je vyřazena a není třeba ji nahrávat. K nahrávání takto označené fráze se můžete kdykoliv vrátit pomocí navigačních tlačítek.
	
	\item \includegraphics[scale=.8]{figures/question.png} Fráze nebyla dosud nahrávána. Takto označená fráze čeká na své první nahrávání.
\end{enumerate}

Samotné nahrávání spustíte pomocí tlačítka \uv{NAHRÁVAT}, uprostřed hlavního formuláře (tlačítko s~čer\hyp{}veným kolečkem). Při stisknutí se toto tlačítko změní na tlačítko \uv{ZASTAVIT} (tlačítko s oranžovým čtverečkem), pomocí kterého lze nahrávání zastavit. V případě, že je aktuálně vybraná fráze odmítnutá a není odemčená (viz sekce \ref{ssec: Skipping}), není možné tuto frázi nahrávat. Všechny možné stavy tlačítka pro nahrávání jsou vyobrazeny na obrázku \ref{fig: btn_recording} níže.

\begin{figure}
	\includegraphics[scale=.85]{figures/btn_start_recording.png}
	\quad
	\includegraphics[scale=.85]{figures/btn_stop_recording.png}
	\quad
	\includegraphics[scale=.85]{figures/btn_start_recording_disabled.png}
	\caption{Možné stavy tlačítka pro nahrávání: Spustit nahrávání zvuku (vlevo), zastavit nahrávání zvuku (uprostřed), nahrávání zvuku není možné (vpravo).}
	\label{fig: btn_recording}
\end{figure}

Mezi panelem s tlačítky a textem aktuálně vybrané fráze se nachází indikátor postupu nahrávání. Tento ukazatel vyobrazený na obrázku \ref{fig: progressbar} poskytuje informace o počtu již nahraných frází a o počtu frází zbývajících nahrát.

\begin{figure}
	\includegraphics[scale=.33]{figures/progress_bar.png}
	\caption{Indikátor stavu nahrávacího procesu.}
	\label{fig: progressbar}
\end{figure}

\subsection{Automatická kontrola kvality nahrávky}
Každá pořízená nahrávka je automaticky po zastavení nahrávání kontrolována kontrolními moduly. Funkce a důležitost kontrolních modulů byla již popsána v sekci \ref{ssec: AudioSettings}. Pokud je Vámi zaznamenaná fráze v pořádku, nezobrazí se žádné varování a můžete pokračovat v nahrávání další fráze. Pokud kontrola nahrávky zaznamená nějaké nedostatky, zobrazí se Vám dialogové okno s více informacemi, viz ukázka na obrázku \ref{fig: check_result} níže. Dialogové okno zavřete tlačítkem \uv{ROZUMÍM}, případně klávesou \uv{Enter}. 

\begin{figure}
	\includegraphics[scale=.8]{figures/check_result1.png}
	\caption{Dialogové okno poskytující informace, proč nahrávka neprošla kontrolními moduly. V tomto případě byla nahrávka příliš tichá.}
	\label{fig: check_result}
\end{figure}

\subsection{Odmítnutí fráze}
\label{ssec: Skipping}
V případě, že se Vám fráze špatně vyslovuje nebo z nějakého zásadního důvodu nechcete frázi nahrávat, lze její nahrávání odmítnout pomocí tlačítka \uv{ODMÍTNOUT}. V nastavení zvuku a kontrolních modulů (viz \ref{ssec: AudioSettings}) lze také nastavit, aby byla fráze odmítnuta, pokud 3x po sobě neprojde kontrolními moduly. Tato funkce je v základu povolena. Libovolnou odmítnutou frázi lze kdykoliv opětovně nahrát, je však nutné ji nejprve odemknout pomocí tlačítka \uv{ODEMKNOUT}, které zastupuje funkci tlačítka pro odmítnutí fráze. Teprve odemčenou odmítnutou frázi lze opětovně nahrát.

\begin{figure}
	\includegraphics[scale=.85]{figures/btn_skip.png}
	\quad
	\includegraphics[scale=.85]{figures/btn_unlock.png}
	\quad
	\includegraphics[scale=.85]{figures/btn_unlocked.png}
	\caption{Možné stavy tlačítka pro odmítnutí fráze: Odmítnout frázi (vlevo), odemknout odmítnutou frázi (uprostřed) a indikace, že je fráze odemknuta (vpravo).}
	\label{fig: btn_skip}
\end{figure}

\subsection{Navigace mezi frázemi}
Mezi již nahranými frázemi je možné se pohybovat pomocí navigačních tlačítek (tlačítka s šipkami vlevo/vpravo).

\begin{itemize}
	\item \textbf{Posun na následující frázi} -- Pomocí navigačních tlačítek uprostřed formuláře vpravo se lze navigovat k následující, následující odmítnuté nebo poslední (první nenahrané) frázi.
	
	\begin{figure}
		\includegraphics[scale=.75]{figures/btn_navigation_following.png}
	\end{figure}

	\item \textbf{Posun na předchozí frázi} -- Pomocí navigačních tlačítek uprostřed formuláře vlevo se lze navigovat k předchozí, předchozí odmítnuté nebo zcela první frázi.
	
	\begin{figure}
		\includegraphics[scale=.75]{figures/btn_navigation_previous.png}
	\end{figure}
\end{itemize}
	
\subsection{Klávesové zkratky}
Některé funkce aplikace mají přednastavenou klávesovou zkratku. Pomocí klávesových zkratek lze ovládat dané prvky aplikace stejně, jako klikáním myšítka. To může urychlit, usnadnit a zpříjemnit proces nahrávání. Seznam všech dostupných klávesových zkratek je uveden níže.

\begin{itemize}[leftmargin=10em]
	\item[\textbf{N}] Nahrávat/Zastavit nahrávání.
	\item[\textbf{Backspace}] Odmítnout/Odemknout nahrávku.
	\item[\textbf{Enter}] Přejít na další frázi.
	\item[\textbf{šipka vlevo}] Přejít na předchozí frázi.
	\item[\textbf{šipka vpravo}] Přejít na následující frázi.
	\item[\textbf{Ctrl + šipka vlevo}] Přejít na první frázi.
	\item[\textbf{Ctrl + šipka vpravo}] Přejít na poslední frázi.
	\item[\textbf{Shift + šipka vlevo}] Přejít na předchozí odmítnutou frázi.
	\item[\textbf{Shift + šipka vpravo}] Přejít na následující odmítnutou frázi.	
	\item[\textbf{šipka nahoru}] Výběr položky ze seznamu výše.
	\item[\textbf{šipka dolů}] Výběr položky ze seznamu níže.
	\item[\textbf{P}] Přehrát vybranou verzi fráze.
	\item[\textbf{S}] Zastavit přehrávání.
	\item[\textbf{L}] Pozastavit přehrávání.
	\item[\textbf{Q}] Zobrazit okno s přehledem klávesových zkratek.
	\item[\textbf{Esc}] Zavřít dialogové okno/panel.
	\item[\textbf{F1}] Zobrazit uživatelskou dokumentaci.
\end{itemize}

Při stisknutí klávesy \textbf{Q} v hlavním formuláři aplikace lze otevřít rychlý předhled klávesových zkratek.
